/// 项目计划详情状态
/// 项目计划状态
///
class PlanStatusEnum {
  /// 所有
  static int all = 0;

  /// 未排期
  static const unschedule = 1;

  /// 未开始
  static const unstart = 2;

  /// 进行中
  static const doing = 3;

  /// 项目延期
  static const delay = 4;

  /// 已停工
  static const suspend = 5;

  /// 已交付
  static const done = 6;
}

/// 异常状态
Map exceptionStatus = {"dissolve": 1, "solve": 2};

/// tasktype
class Tasktype {
  static const All = 0;

  ///进度反馈
  static const FeedBack = 1;

  /// 分配工站负责人
  static const AssignWorkStation = 2;

  /// 分配工序负责人
  static const AssignProcess = 3;

  /// 异常处理
  static const Exception = 4;
}

/// TaskStatus
class TaskStatus {
  static const None = 0;
  static const UnStart = 1;
  static const Doing = 2;
  static const Finished = 99;
}

/// 工人类型
class UserType {
  /// 所有
  static const all = 0;

  ///项目经理
  static const projectManager = 1;

  /// 质检员
  static const qualityControl = 2;

  /// 供应商
  static const supplier = 3;

  /// 员工
  static const staff = 4;

  /// 组长
  static const groupLeader = 5;

  /// 调度主管
  static const dispatchingManager = 6;

  /// 计划专员
  static const projectPlanner = 7;

  /// 测量专员
  static const measureSpecialist = 8;

  /// 调度专员（人力）
  static const humanDispatcher = 9;

  /// 调度专员（物料）
  static const materialDispatcher = 10;

  /// 深化设计师
  static const deepDesigner = 11;

  /// 异常处理专员
  static const exceptionHandler = 12;

  /// 区域经理
  static const areaManager = 13;

  /// 班长
  static const monitor = 14;

  /// 队长
  static const teamLeader = 15;

  /// XDS管理员
  static const administrator = 100;
}

/// 项目计划详情状态  施工状态
class PlanDetailsStatus {
  /// 所有
  static const All = 0;

  /// 未开始
  static const Unstart = 1;

  /// 进行中
  static const Doing = 2;

  /// 已停工
  static const Suspend = 3;

  /// 已交付
  static const Done = 4;

  static List planDetailsStatusList = [
    {"value": 1, "text": "未开始"},
    {"value": 2, "text": "进行中"},
    {"value": 4, "text": "已完成"},
  ];
  static Map planDetailsStatusText = {
    1: "未开始",
    2: "进行中",
    4: "已完成",
  };
}

class SuspendStatus {
  /// 全部
  static const all = 0;

  /// 停工未开始
  static const unSuspend = 1;

  /// 已停工
  static const suspended = 2;

  /// 已复工
  static const resumed = 3;
}

class ProcessTypeEnum {
  /// 基装
  static const Basic = 1;

  /// 安装
  static const Installation = 2;

  /// 送料
  static const Send = 3;
}

class MessageType {
  /// 风险
  static const planDelayRisk = "8";
}

class MessageCategoryEnum {
  /// 施工
  static const Construction = 1;

  /// 风险
  static const Risk = 2;
}
