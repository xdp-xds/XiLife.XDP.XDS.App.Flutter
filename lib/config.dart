class Config {
  /// 平台id
  static final String clientId = 'xilife.xdp.xds.app.login';

  /// 路由守卫开关
  static final bool routeDefend = true;

  /// 设置代理
  static final bool proxy = false;

  /// app环境
  static final String appEnv = 'uat';

  /// baseUrl
  static final String baseUrl = 'https://uat-api.xdp.xi-life.cn';

  /// 请求链接超时
  static final int connectTimeout = 500000;

  /// 请求接收超时
  static final int receiveTimeout = 500000;
}
