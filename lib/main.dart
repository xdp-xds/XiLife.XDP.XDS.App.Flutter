import 'dart:io';

import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:xdsapp/provider/notice/index.dart';
import 'package:xdsapp/router/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/initialize_jpush.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'common_ui/xds_loading/index.dart';
import 'package:flutter_downloader/flutter_downloader.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (!G.isIOS) {
    await FlutterDownloader.initialize();
  }

  /// 初始化user
  await G.user.init();
  debugPaintSizeEnabled = false;
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: NoticeModel()),
      ],
      child: MyApp(),
    ),
  );

  /// 设置android状态栏为透明的沉浸。
  /// 写在组件渲染之后，是为了在渲染后进行set赋值，覆盖状态栏，
  /// 写在渲染之前MaterialApp组件会覆盖掉这个值。
  if (Platform.isAndroid) {
    SystemUiOverlayStyle systemUiOverlayStyle = SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,

        ///这是设置状态栏的图标和字体的颜色
        ///Brightness.light  一般都是显示为白色
        ///Brightness.dark 一般都是显示为黑色
        statusBarIconBrightness: Brightness.dark);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    /// 初始化jpush
    initializeJPush();
  }

  @override
  Widget build(BuildContext context) {
    XdsLoading.ctx = context;
    /// TODO: input点击其他位置键盘收起，不知道后期会不会有问题，带观察
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
      },
      child: MaterialApp(
        navigatorKey: G.navigatorKey,
        // title: 'Flutter Demo',
        theme: ThemeData(
          platform: TargetPlatform.fuchsia,
          primaryColor: hex("#d8eadb"),
          backgroundColor: Color(0xFFffffff),
          primaryColorBrightness: Brightness.light,
          accentColor: hex("#d8eadb"),

          /// body 背景颜色
          canvasColor: Color(0xFFffffff),
          textTheme: TextTheme(
              body1: TextStyle(fontSize: 15, color: hex('#111')),
              button: TextStyle(
                color: hex("#13BB87"),
              )),
          inputDecorationTheme: InputDecorationTheme(
            contentPadding: const EdgeInsets.symmetric(vertical: 8),
            hintStyle: TextStyle(color: Color(0xffbbbbbb), fontSize: 15),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xff111111),
              ),
            ),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                color: Color(0xffbbbbbb),
              ),
            ),
          ),
          appBarTheme: AppBarTheme(
            elevation: 0,
            color: Color(0xFFffffff),
            iconTheme: IconThemeData(color: Color(0xFF111111)),
            textTheme: TextTheme(
              title: TextStyle(
                  color: Color(0xFF111111),
                  fontSize: 17,
                  fontWeight: FontWeight.bold),
            ),
          ),

          /// button去除点击后的背景，注意：对appbar中的的button无效
          buttonTheme: ButtonThemeData(
            splashColor: Color(0xFFffffff),
            highlightColor: Color(0xFFffffff),
          ),
        ),
        initialRoute: '/',
        onGenerateRoute: Router.onGenerateRoute,
        //增加国际化
        localizationsDelegates: [
          //此处
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          //此处
          const Locale('zh', 'CH'),
        ],
      ),
    );
  }
}
