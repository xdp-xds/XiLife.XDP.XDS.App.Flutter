import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/example.dart';
import 'package:xdsapp/common_ui/xds_collapse/example.dart';
import 'package:xdsapp/common_ui/xds_dialog/example.dart';
import 'package:xdsapp/common_ui/xds_leader_card/example.dart';
import 'package:xdsapp/common_ui/xds_list_row/example.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/example.dart';
import 'package:xdsapp/common_ui/xds_project_base_detail/example.dart';
import 'package:xdsapp/config.dart';
import 'package:xdsapp/pages/choose_worker/index.dart';
import 'package:xdsapp/pages/home/index.dart';
// import 'package:xdsapp/pages/home/index1.dart';
import 'package:xdsapp/pages/home/index3.dart';
import 'package:xdsapp/pages/login/index.dart';
import 'package:xdsapp/pages/notice/detail.dart';
import 'package:xdsapp/pages/notice/index.dart';
import 'package:xdsapp/pages/project_detail/delay_detail/index.dart';
import 'package:xdsapp/pages/project_detail/index.dart';
import 'package:xdsapp/pages/project_detail/stop_page/stop_confirm.dart';
import 'package:xdsapp/pages/project_detail/continue_confirm/index.dart';
import 'package:xdsapp/pages/project_list/index.dart';
import 'package:xdsapp/pages/project_pause_detail/index.dart';
import 'package:xdsapp/pages/task/assignment.dart';
import 'package:xdsapp/pages/task/task_assignment_process/index.dart';
import 'package:xdsapp/pages/task/progress_update/index.dart';
import 'package:xdsapp/pages/task/progress_update_material/index.dart';
import 'package:xdsapp/pages/task/progress_detail/index.dart';
import 'package:xdsapp/pages/task_list/index.dart';
import 'package:xdsapp/utils/global.dart';

class Router {
  static final routers = {
    '/login': (BuildContext context) => Login(),
    // '/': (BuildContext context) => HomePage(),
    '/': (BuildContext context) => Home(),
    '/home_test': (BuildContext context, {Object arguments}) => HomeTest(),
    '/notice': (BuildContext context) => NoticeList(),
    '/notice_detail': (BuildContext context, {Object arguments}) =>
        NoticeDetail(args: arguments),
    '/project_list': (BuildContext context, {Object arguments}) =>
        ProjectList(),
    '/project_detail': (BuildContext context, {Object arguments}) =>
        ProjectDetail(args: arguments),
    '/task_list': (BuildContext context, {Object arguments}) => TaskList(),
    '/task_assignment': (BuildContext context, {Object arguments}) =>
        Assignment(args: arguments),
    '/choose_worker': (BuildContext context, {Object arguments}) =>
        ChooseWorker(args: arguments),
    '/task_assignment_process': (BuildContext context, {Object arguments}) =>
        AssignmentProcess(args: arguments),
    '/task_progress_update': (BuildContext context, {Object arguments}) =>
        ProgressUpdate(args: arguments),
    '/task_progress_update_material': (BuildContext context,
            {Object arguments}) =>
        ProgressUpdateMaterial(args: arguments),
    '/delay_detail': (BuildContext context, {Object arguments}) =>
        DelayDetail(args: arguments),
    '/project_pause_detail': (BuildContext context, {Object arguments}) =>
        ProjectPauseDetail(args: arguments),
    '/stop_confirm': (BuildContext context, {Object arguments}) => StopConfirm(
          args: arguments,
        ),
    '/continue_confirm': (BuildContext context, {Object arguments}) =>
        ContinueConfirm(
          args: arguments,
        ),
    '/progress_detail': (BuildContext context, {Object arguments}) =>
        ProgressDetail(
          args: arguments,
        ),

    // 组件 example
    '/example_xds_pull_to_refresh': (BuildContext context) =>
        ExampleXdsPullToRefresh(),
    '/example_xds_dialog': (BuildContext context) => ExampleXdsDialog(),
    '/example_xds_list_row': (BuildContext context) => ExampleXdsListRow(),
    '/example_xds_collapse': (BuildContext context) => ExampleXdsCollapse(),
    '/example_xds_leader_card': (BuildContext context) =>
        ExampleXdsLeaderCard(),
    '/example_xds_bottom_button': (BuildContext context) =>
        ExampleXdsBottomButton(),
    '/example_xds_project_base_detail': (BuildContext context) =>
        ExampleXdsProjectBaseDetail(),
  };

  static Route onGenerateRoute(RouteSettings settings) {
    // 统一处理
    final String name = settings.name;
    final Function pageContentBuilder = routers[name];
    Route route;
    var token = G.user.workInfo?.id;
    if (token == null && Config.routeDefend) {
      route = MaterialPageRoute(
        builder: (context) => Login(),
      );
    } else {
      if (pageContentBuilder != null) {
        if (settings.arguments != null) {
          route = MaterialPageRoute(
              builder: (context) =>
                  pageContentBuilder(context, arguments: settings.arguments));
        } else {
          route = MaterialPageRoute(
              builder: (context) => pageContentBuilder(context));
        }
      }
    }
    // }

    return route;
  }
}
