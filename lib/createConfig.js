/**
 * 生成配置
 * 1.api地址
 * 2。自动存储万能token
 */
const fs = require('fs');
let APP_ENV = process.env.APP_ENV;

let apiRoot = 'http://192.168.16.175:5000';

let getConfig = (env) => {
  console.log(env)
  switch (env) {
    case 'development':
      apiRoot = 'http://192.168.16.190:5000';
      break;
    case 'development_manualtest':
      apiRoot = 'http://192.168.16.175:5000';
      break;
    case 'manualtest':
      apiRoot = 'http://192.168.16.170:5000';
      break;
    case 'uat':
      apiRoot = 'https://uat-api.xdp.xi-life.cn';
      break;
    default:
      apiRoot = 'http://192.168.16.175:5000';
  }
}
getConfig(APP_ENV)
let template = `
class ApiConfig {
    static String  appEnv = "${APP_ENV}";
    static  String  baseUrl = "${apiRoot}";
    static int connectTimeout = 500000;
    static  int receiveTimeout = 500000;
    static bool proxy = false;
  }
  

`

fs.writeFile('./lib/api/api_config.dart', template, function (err) {
  if (err) {
    return console.log(err)
  }

  console.log('Configuration file has generated')
})
var  argv= process.argv;

fs.readFile("./pubspec.yaml", function (err, data) {
  if (err) {
    console.log(err);
    return false;
  }
  var dataString = data.toString()
  var startString = "";
  var endsString = "";
  var first =0;
  for (var i = 0; i < dataString.length; i++) {
    if (dataString[i] == "\n" && first ==1) {
      endsString = dataString.substring(i);
      first=2;
    }
    if (dataString[i] == "\n" && i == 50) {
      startString = dataString.substr(0, i);
      first =1;
    }
  }
  var versionName ="1.0.0";
  var versionCode =1;
  if(argv.length>2){
    versionName=argv[2];
    versionCode= argv[3];
  }
  var newString = startString + `\nversion: ${versionName}+${versionCode}` + endsString;
  console.log(`版本号：\nversion: ${versionName}+${versionCode}`);
  fs.writeFile('./pubspec.yaml', newString, function (err) {
    if (err) {
      return console.log(err)
    }
    console.log('重新生成版本号成功')
  })
});

