/*
 * @Author: meetqy
 * @since: 2019-10-15 21:35:54
 * @lastTime: 2019-10-16 10:13:48
 * @LastEditors: meetqy
 */
import 'package:flutter/material.dart';

class NoticeModel with ChangeNotifier {
  int _leftMsgCount = 0;
  int _rightMsgCount = 0;

  /// 施工消息总数
  int get constructionMsgCount => _leftMsgCount;
  /// 风险消息总数
  int get riskMsgCount => _rightMsgCount;

  /// 设置施工数
  void setConstruction(int number) {
    _leftMsgCount = number;
    notifyListeners();
  }

  /// 设置风险数
  void setRisk(int number) {
    _rightMsgCount = number;
    notifyListeners();
  }
}
