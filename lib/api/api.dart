import 'package:xdsapp/api/api_urls.dart';
import 'package:xdsapp/api/index.dart';

class Request {
  static final instance = Http.getInstance();
  static getSmsCode(data) {
    return instance.get('${ApiUrls.sendSMS}', data: data);
  }

  static login(data) {
    return instance.post('${ApiUrls.sendSMS}', data: data);
  }

  static getTaskCount(data) {
    return instance.get('${ApiUrls.taskCount}', data: data);
  }

  static getUserId(data) {
    return instance.get('${ApiUrls.getUserId}/$data');
  }

  static getMesgCount(data) {
    return instance.get('${ApiUrls.mesgCount}', data: data);
  }

  static getProjectList(pageIndex, pageSize, data) {
    String pageIndexString = pageIndex.toString();
    String pageSizeString = pageSize.toString();
    return instance.post(
        '${ApiUrls.projectList}?pageIndex=$pageIndexString&pageSize=$pageSizeString',
        data: data);
  }

  static getProjectDetail(data) {
    return instance.get('${ApiUrls.projectDetail}/$data');
  }

  static getNoticeDetail(String id) {
    return instance.get('${ApiUrls.noticeDetail}/$id');
  }

  static getMessageList(data) {
    return instance.get('${ApiUrls.messageList}', data: data);
  }

  static getPlanList(pageIndex, pageSize, order, data) {
    return instance.post(
        '${ApiUrls.planList}?pageIndex=$pageIndex&pageSize=$pageSize&order=$order',
        data: data);
  }

  static getMetaData() {
    return instance.get(ApiUrls.metaData);
  }

  /// 获取负责人
  static getChargeperson({String planId, String deliveryStageId = ''}) {
    return instance.get(
        '${ApiUrls.chargeperson}?planId=$planId&deliveryStageId=$deliveryStageId');
  }

  /// 修改工站负责人
  static putChargeperson({
    data,
    String planId,
    String deliveryStageId,
    String taskId,
  }) {
    return instance.put(
        '${ApiUrls.chargeperson}?planId=$planId&deliveryStageId=$deliveryStageId&taskId=$taskId',
        data: data);
  }

  /// 修改工序负责人
  static chargepersonProcess({
    data,
    String planId,
    String taskId,
  }) {
    return instance.put(
        '${ApiUrls.chargepersonProcess}?planId=$planId&taskId=$taskId',
        data: data);
  }

  static getTaskList(pageIndex, pageSize, userId) {
    return instance.post(
      '${ApiUrls.taskList}?pageIndex=$pageIndex&pageSize=$pageSize&userId=$userId',
    );
  }

  /// 首页获取消息数
  static getTodoList(String userId) {
    return instance.get('${ApiUrls.todoList}$userId/todolist');
  }

  /// 获取工站信息
  static getWorkStation(
      {String planId,
      String deliveryStageId,
      bool isAll = false,
      String taskId}) {
    return instance.get(
        '${ApiUrls.getWorkStation}?planId=$planId&deliveryStageId=$deliveryStageId&isAll=$isAll&taskId=$taskId');
  }

  /// 获取工序信息
  static getWorkProcedure(
      {String planId,
      String workStationId,
      bool isAll = false,
      String taskId}) {
    return instance.get(
        '${ApiUrls.getWorkProcedure}?planId=$planId&workStationId=$workStationId&isAll=$isAll&taskId=$taskId');
  }

  ///获取项目经理下成员列表
  static getChargepersonList(String id, String filter) {
    return instance.get('${ApiUrls.chargepersonList}/$id?filter=$filter');
  }

  /// 获取任务详情
  static getTaskDetail(id) {
    return instance.get('${ApiUrls.taskDetail}/$id');
  }

  /// 获取进度更新列表
  static getFeedbackList(pageIndex, pageSize, userId) {
    return instance.post(
      '${ApiUrls.getFeedbackList}?pageIndex=$pageIndex&pageSize=$pageSize&userId=$userId',
    );
  }

  /// 进度反馈详情
  static getFeedbackDetail(id) {
    return instance.get('${ApiUrls.updateFeedback}/$id');
  }

  /// 更新进度反馈
  static updateFeedback(taskId, status, remark) {
    return instance.post(
      '${ApiUrls.updateFeedback}?taskId=$taskId&status=$status&remark=$remark',
    );
  }

  /// 更新进度反馈 送料
  static updateFeedbackMaterail(taskId, completionTime, remark) {
    return instance.post(
      '${ApiUrls.updateFeedbackMaterail}?taskId=$taskId&completionTime=$completionTime&remark=$remark',
    );
  }

  /// 获取计划延期信息
  static getDelay(String planId) {
    return instance.get('${ApiUrls.getDelay}/$planId');
  }

  ///项目信息施工进度
  getSchedule(String planId) {
    return instance.get('${ApiUrls.schedule}/$planId');
  }

  /// 项目获取负责人
  static getPrinciapl(String planId) {
    return instance.get('${ApiUrls.getPrincipal}/$planId');
  }

  /// 获取停工信息
  static getPlanPauseDetail(String planId) {
    return instance.get('${ApiUrls.getPlanPauseDetail}/$planId');
  }

  /// 计划停工
  static getSuspend(Map data) {
    return instance.post('${ApiUrls.getSuSpend}', data: data);
  }

  ///获取停工工序
  static getSuspendedProcess(planId) {
    return instance.get('${ApiUrls.getSuspendedProcess}/$planId');
  }

  ///复工
  static reActiveProcess(planId, data) {
    return instance.post('${ApiUrls.reactivate}/$planId', data: data);
  }

  ///工序详情
  static getProcessDetail(processId, data) {
    return instance.post('${ApiUrls.processDetail}/$processId', data: data);
  }

  ///工序详情
  static getVersion(data) {
    return instance.get('${ApiUrls.version}', data: data);
  }
}
