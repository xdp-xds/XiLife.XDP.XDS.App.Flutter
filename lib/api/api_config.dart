import 'package:xdsapp/config.dart';

class ApiConfig {
  static String appEnv = Config.appEnv;
  static String baseUrl = Config.baseUrl;
  static int connectTimeout = Config.connectTimeout;
  static int receiveTimeout = Config.receiveTimeout;
  static bool proxy = Config.proxy;
}
