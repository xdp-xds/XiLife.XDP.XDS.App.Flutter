class ApiUrls {
  /// 刷新token
  static String refreshUrl = 'identity/v1/Login/refresh';

  /// 发送验证码
  static String sendSMS = '/identity/v1/Login/smscode';

  /// 获取未完成任务书
  static String taskCount = '/xds/v1/Task/auth/taskcount';

  /// 获取通知列表
  static String noticeList = '/xds/v1/Message/auth/pagination';

  /// 获取消息详情
  static String noticeDetail = '/xds/v1/Message/auth/';

  /// 获取工人id
  static String getUserId = '/xds/v1/User/auth/userid';

  /// 工人消息未读数
  static String mesgCount = '/xds/v1/Message/auth/taskcount';

  /// 项目列表
  static String projectList = '/xds/v1/Project/auth/plan/list';

  /// 项目详情
  static String projectDetail = '/xds/v1/app/Plan/auth';

  /// 消息列表
  static String messageList = '/xds/v1/Message/auth/pagination';

  /// 计划列表分
  static String planList = '/xds/v1/app/Plan/auth/pagination';

  /// 获取元数据
  static String metaData = '/xds/v1/Metadata/auth';

  /// 工站负责人信息 编辑put
  static String chargeperson = '/xds/v1/app/Task/auth/chargeperson';

  /// 获取工站信息
  static String getWorkStation =
      '/xds/v1/app/Task/auth/chargeperson/deliverystage';

  /// 获取工序信息
  static String getWorkProcedure =
      '/xds/v1/app/Task/auth/chargeperson/workstation';

  /// 获取项目列表
  static String taskList = '/xds/v1/app/Task/auth/list';

  /// 获取待完成事项
  static String todoList = '/xds/v1/User/auth/';

  /// 获取项目经理下成员列表
  static String chargepersonList = '/xds/v1/app/Task/auth/chargeperson';

  /// 任务详情
  static String taskDetail = "/xds/v1/app/Task/auth";

  ///进度反馈的列表
  static String getFeedbackList = "/xds/v1/app/Task/auth/feedback/list";

  /// 编辑工序负责人
  static String chargepersonProcess =
      "/xds/v1/app/Task/auth/chargeperson/process";

  /// 提交进度反馈
  static String updateFeedback = "/xds/v1/app/Task/auth/feedback";

  ///进度反馈送料
  static String updateFeedbackMaterail =
      "/xds/v1/app/Task/auth/feedback/materail";

  /// 获取计划延期信息
  static String getDelay = "/xds/v1/app/Plan/auth/delay";

  ///项目详情施工进度
  static String schedule = "/xds/v1/app/Plan/auth/schedule";

  /// 项目获取负责人
  static String getPrincipal = "/xds/v1/app/Plan/auth/principal";

  /// 计划停工信息
  static String getPlanPauseDetail = '/xds/v1/app/Plan/auth/suspend';

  /// 计划停工
  static String getSuSpend = '/xds/v1/app/Plan/auth/suspend';

  //获取复工的工序
  static String getSuspendedProcess = "/xds/v1/app/Plan/auth/suspended/";

  //复工
  static String reactivate = "/xds/v1/app/Plan/auth/reactivate";

  /// 工序详情
  static String processDetail = "/xds/v1/app/Plan/auth/process";

  ///获取版本信息
  static String version = "/common/v1/App/version";
}
