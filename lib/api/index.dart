import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xdsapp/api/api_config.dart';

import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api_urls.dart';
import 'package:xdsapp/config.dart';
import 'package:xdsapp/utils/global.dart';

/// 是否刷新token
bool dioLock = false;

class Http {
  static String token;
  static Dio _dio;
  static BaseOptions options;
  static Http _instance;
  bool isRefresh = false;
  var list = [];

  static getInstance() {
    if (_instance == null) {
      _instance = new Http();
    }
    return _instance;
  }

  Http() {
    // 初始化http请求options
    options = new BaseOptions(
        connectTimeout: ApiConfig.connectTimeout,
        receiveTimeout: ApiConfig.receiveTimeout,
        headers: {},
        baseUrl: ApiConfig.baseUrl);

    _dio = new Dio(options);
    // 设置http代理
    if (ApiConfig.proxy) {
      (_dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
          (HttpClient client) {
        client.findProxy = (uri) {
          //proxy all request to localhost:8888
          return "PROXY  localhost:8888";
        };
        //忽略证书
        client.badCertificateCallback =
            (X509Certificate cert, String host, int port) => true;
      };
    }
    // 请求前拦截添加token等
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      // GlobalLoading.before(options.uri);
      Map<String, dynamic> headers = options.headers;
      String accessToken = G.user.accessToken;
      if (accessToken != null)
        headers['Authorization'] = 'Bearer ' + accessToken;
      return options;
    }));

    // 请求成功拦截，简化代码中调用难度
    _dio.interceptors.add(InterceptorsWrapper(onResponse: (Response response) {
      // GlobalLoading.complete(response.request.uri);
      return response.data;
    }));

    // 请求失败拦截
    _dio.interceptors.add(InterceptorsWrapper(onError: (DioError e) async {
      // GlobalLoading.complete(e.request.uri);
      if(e.error.message == "Connection failed") {
        Fluttertoast.showToast(
                msg: '网络开小差了，请检查网络后再试！',
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 2,
                backgroundColor: Colors.grey,
                textColor: Colors.white,
                fontSize: 10.0);
        throw e;
      }
      switch (e.response.statusCode) {
        case 400:
          {
            String msg = e.response.data[0];
            Fluttertoast.showToast(
                msg: msg.isEmpty ? '400服务器' : msg,
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 2,
                backgroundColor: Colors.grey,
                textColor: Colors.white,
                fontSize: 10.0);
            break;
          }
        case 404:
          {
            Fluttertoast.showToast(
                msg: "404资源未找到",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 2,
                fontSize: 10.0);
            break;
          }
        case 500:
          {
            Fluttertoast.showToast(
                msg: "500服务器",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.CENTER,
                timeInSecForIos: 2,
                fontSize: 10.0);
            break;
          }
        case 401:
          {
            dioLock = true;
            if (dioLock) {
              _dio.lock();

              // 获取新的token
              SharedPreferences prefs = await SharedPreferences.getInstance();
              String refreshToken = prefs.getString('refresh_token');
              Map<String, dynamic> body = {
                'refreshToken': refreshToken,
                'clientId': Config.clientId
              };
              Dio dio = Dio();
              dio.options.baseUrl = Config.baseUrl;

              try {
                Response response =
                    await dio.post('/${ApiUrls.refreshUrl}', data: body);
                await G.user.setToken(
                  accessToken: response.data['access_token'],
                  refreshToken: response.data['refresh_token'],
                );
                var request = e.response.request;
                await G.user.parsingToken(response.data['access_token']);
                dioLock = false;
                _dio.unlock();
                var newRequest = await httpMethod(request.path, request.method,
                    data: request.queryParameters,
                    cancelToken: request.cancelToken);

                return newRequest;
              } on DioError catch (_) {
                await Fluttertoast.showToast(
                    msg: "您当前的登录已失效，请重新登录",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    timeInSecForIos: 2,
                    fontSize: 10.0);
                G.pushNamed('/login');
                _dio.unlock();
              }
            }
            return false;
          }
      }
      throw e;
    }));
  }

  httpMethod(url, String type, {Options options, cancelToken, data}) async {
    Response response;
    try {
      if (type == 'GET') {
        response = await _dio.request(url,
            queryParameters: data,
            cancelToken: cancelToken,
            options: _checkOptions(type, options));
      } else {
        response = await _dio.request(url,
            data: data,
            cancelToken: cancelToken,
            options: _checkOptions(type, options));
      }
    } on DioError catch (e) {
      throw e;
      //   if (CancelToken.isCancel(e)) {
      //   } else {}
    }

    return response?.data;
  }

  // 封装get
  get(url, {options, cancelToken, data}) async {
    return httpMethod(url, "GET",
        options: options, cancelToken: cancelToken, data: data);
  }

  // post请求封装
  post(url, {options, cancelToken, data}) async {
    return httpMethod(url, "POST",
        options: options, cancelToken: cancelToken, data: data);
  }

  // put请求封装
  put(url, {options, cancelToken, data}) async {
    return httpMethod(url, "PUT",
        options: options, cancelToken: cancelToken, data: data);
  }

  Options _checkOptions(method, options) {
    if (options == null) {
      options = new Options();
    }
    options.method = method;
    return options;
  }
}
