import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:color_dart/HexColor.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/pages/task_list/components/base_list.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/global.dart';

class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList>
//TickerProviderStateMixin
//SingleTickerProviderStateMixin
    with
        TickerProviderStateMixin {
  static const processFeedback = '进度反馈';
  static const assignPrincipal = '分配负责人';
  static const exception = '异常处理';
  TabController _tabController;
  List tabs = [processFeedback, assignPrincipal];
  int processFeedbackPageIndex = 1;
  int assignPrincipalPageIndex = 1;
  int pageSize = 10;
  final String userId = G.user.workInfo.id;
  //G.user.workInfo.id;
  int type;
  List<Widget> tabList = [];
  TabBarView tabBarView;
  Map assignPrincipalData = {
    "data": [],
    "count": 1,
    "unFinishedCount": 0,
    "isLoadFinished": null,
  };
  Map processFeedbackData = {
    "data": [],
    "count": 1,
    "isLoadFinished": null,
    "unFinishedCount": 0,
    "task": Tasktype.FeedBack
  };

  @override
  void initState() {
    super.initState();
    this.setState(() {
      if (G.user.workInfo.type == UserType.projectManager) {
        tabs = [
          processFeedback,
          // exception,
          assignPrincipal,
        ];
      } else {
        tabs = [
          processFeedback,
          assignPrincipal,
        ];
      }
    });

    _tabController = TabController(length: tabs.length, vsync: this);
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    /// 接口请求
    getAssignPrincipalTaskList(false);
    getProcessFeedbackTaskList(false);
    // });

    // XdsLoading.show();
    // try {
    //   XdsLoading.show();
    // } catch (e) {}
  }

  initData() {
    setState(() {
      processFeedbackData['isLoadFinished'] = null;
      processFeedbackData['count'] = 1;
      assignPrincipalData['isLoadFinished'] = null;
      assignPrincipalData['count'] = 1;
    });
    processFeedbackPageIndex = 1;
    assignPrincipalPageIndex = 1;
    getAssignPrincipalTaskList(true);
  }

  getProcessFeedbackTaskList(isRefrensh) {
    Request.getFeedbackList(processFeedbackPageIndex, pageSize, userId)
        .then((res) {
      setState(() {
        Map taskList = res["data"]["taskList"];
        processFeedbackData['count'] = taskList['count'];
        if (isRefrensh == true) {
          processFeedbackData['data'] = [];
        }
        taskList['data'].forEach((item) {
          Map taskRemark = item['taskRemark'];
          item["projectName"] = taskRemark["projectName"];
          item['endProcessDate'] = taskRemark['endDate'];
          item['startPrcessDate'] = taskRemark['startDate'];
          item["processName"] = taskRemark['processName'];
          Map remark = json.decode(item["remark"]);
          item["processType"] = remark["ProcessType"];

          processFeedbackData['data'].add(item);
          if (processFeedbackData['isLoadFinished'] == null &&
              item['status'] == TaskStatus.Finished) {
            processFeedbackData['isLoadFinished'] =
                taskList['data'].indexOf(item) +
                    pageSize * (processFeedbackPageIndex - 1);
          }
        });
        processFeedbackData["unFinishedCount"] = res["data"]["unFinishedCount"];
      });
    });
  }

  getAssignPrincipalTaskList(isRefrensh) {
    Request.getTaskList(assignPrincipalPageIndex, pageSize, userId).then((res) {
      this.setState(() {
        assignPrincipalData['count'] = res['count'];
        if (isRefrensh == true) {
          assignPrincipalData['data'] = [];
        }
        res['data'][0]['tasks'].forEach((item) {
          if (assignPrincipalData['isLoadFinished'] == null &&
              item['status'] == TaskStatus.Finished) {
            assignPrincipalData['isLoadFinished'] =
                res['data'][0]['tasks'].indexOf(item) +
                    pageSize * (assignPrincipalPageIndex - 1);
          }
        });
        assignPrincipalData["unFinishedCount"] =
            res['data'][0]["unFinishedCount"];
        assignPrincipalData['data'].addAll(res['data'][0]['tasks']);
      });
    });
  }

  onLoadAssignPrincipalData() {
    this.setState(() {
      assignPrincipalPageIndex++;
      getAssignPrincipalTaskList(false);
    });
  }

  onRefreshAssignPrincipalData() {
    this.setState(() {
      assignPrincipalPageIndex = 1;
      assignPrincipalData['isLoadFinished'] = null;
      getAssignPrincipalTaskList(true);
    });
  }

  onLoadProcessFeedbackData() {
    this.setState(() {
      processFeedbackPageIndex++;
    });
    getProcessFeedbackTaskList(false);
  }

  onRefreshProcessFeedbackData() {
    this.setState(() {
      processFeedbackPageIndex = 1;
      processFeedbackData['isLoadFinished'] = null;
    });
    getProcessFeedbackTaskList(true);
  }

  getTab(value) {
    if (value == processFeedback) {
      if (processFeedbackData["unFinishedCount"] > 0) {
        return "$value（${processFeedbackData["unFinishedCount"].toString()}）";
      }
      return "$value";
    }
    if (value == assignPrincipal) {
      if (assignPrincipalData["unFinishedCount"] > 0) {
        return "$value（${assignPrincipalData["unFinishedCount"].toString()}）";
      }
      return "$value";
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('任务列表'),
        // leading: G.customAppbar.leading(),
        actions: <Widget>[Container()],
        bottom: TabBar(
            controller: _tabController,
            onTap: (i) {},
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: hex('#13bb87'),
            labelStyle: TextStyle(fontWeight: FontWeight.bold),
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
            tabs: tabs.map((e) => Tab(text: getTab(e))).toList()),
      ),
      body: SafeArea(
          child: Container(
              child: TabBarView(
        controller: _tabController,
        children: [
          new BaseList(
              data: processFeedbackData,
              onLoad: this.onLoadProcessFeedbackData,
              onRefresh: this.onRefreshProcessFeedbackData,
              backRefrensh: this.initData),
          new BaseList(
              data: assignPrincipalData,
              onLoad: this.onLoadAssignPrincipalData,
              onRefresh: this.onRefreshAssignPrincipalData,
              backRefrensh: this.initData)
        ],
      ))),
    );
  }
}
