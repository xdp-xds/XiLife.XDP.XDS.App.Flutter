import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:xdsapp/common_ui/xds_list_row/index.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/index.dart';
import 'package:xdsapp/utils/parse_date.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/global.dart';

class BaseList extends StatefulWidget {
  final data;
  final Function onRefresh;
  final Function onLoad;
  final Function backRefrensh;
  BaseList({this.data, this.onRefresh, this.onLoad, this.backRefrensh});
  @override
  _BaseListState createState() => _BaseListState();
}

class _BaseListState extends State<BaseList> {
  int isFinished;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  ScrollController _contoller = new ScrollController();
  @override
  void initState() {
    super.initState();
    _contoller.addListener(() {});
  }

  Widget returnListRow(item, i) {
    if (item['type'] == Tasktype.FeedBack) {
      return returnFeedBackRow(item, i);
    }
    if (item['type'] == Tasktype.AssignProcess ||
        item['type'] == Tasktype.AssignWorkStation) {
      return listRow(item, i);
    }
    return listRow(item, i);
  }

  Widget returnFeedBackRow(item, i) {
    final processName = item['processName'] != null ? item['processName'] : '';
    final projectName = item['projectName'] != null ? item['projectName'] : '';
    String bottomLeft;
    String bottomRight;
    var status = Status.normal;
    bottomRight =
        "${dealMonthAndDay(item['startPrcessDate'])}-${dealMonthAndDay(item['endProcessDate'])}";
    if (item['status'] == TaskStatus.Doing) {
      status = Status.normal;
    }
    if (item['status'] == TaskStatus.Finished) {
      if (isFinished == null) {
        status = Status.complate;
      }
    }
    bottomLeft = processName;

    return Container(
      child: Column(
        children: <Widget>[
          //显示未完成
          i == 0 && item['status'] == TaskStatus.Doing
              ? Container(
                  color: hex("#F4F4F4"),
                  height: 50,
                  padding: EdgeInsets.only(left: 15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '未完成',
                    style: TextStyle(
                      color: hex("#999"),
                      fontSize: 14,
                    ),
                  ),
                )
              : Container(),
          //显示已完成
          widget.data['isLoadFinished'] == i
              ? Container(
                  color: hex("#F4F4F4"),
                  height: 50,
                  padding: EdgeInsets.only(left: 15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '已完成',
                    style: TextStyle(
                      color: hex("#999"),
                      fontSize: 14,
                    ),
                  ),
                )
              : Container(),
          GestureDetector(
            onTap: () {
              this.routerDetail(item);
            },
            child: XdsListRow(
              status: status,
              title: '$projectName ',
              bottomLeft: bottomLeft,
              bottomRight: bottomRight,
            ),
          ),
        ],
      ),
    );
  }

  Widget listRow(item, i) {
    final deliveryStageName =
        item['deliveryStageName'] != null ? item['deliveryStageName'] : '';
    final projectName = item['projectName'] != null ? item['projectName'] : '';
    final workstationName =
        item['workstationName'] != null ? item['workstationName'] : '';
    // final createdTime = item['status'] == TaskStatus.Doing
    //     ? dealtimeSlash(item['createdTime'])
    //     : "";
    final type = item['type'];
    String bottomLeft;
    String bottomRight;
    var status;
    bottomRight =
        "${dealMonthAndDay(item['planStartDate'])}-${dealMonthAndDay(item['planEndDate'])}";
    if (item['status'] == TaskStatus.Doing) {
      status = Status.normal;
    }
    if (item['status'] == TaskStatus.Finished) {
      if (isFinished == null) {
        status = Status.complate;
      }
    }
    switch (type) {
      case Tasktype.AssignWorkStation:
        {
          bottomLeft = deliveryStageName;
          break;
        }
      case Tasktype.AssignProcess:
        {
          bottomLeft = workstationName;
          break;
        }
      default:
        {
          bottomLeft = "";
        }
    }

    return Container(
      child: Column(
        children: <Widget>[
          //显示未完成
          i == 0 && item['status'] == TaskStatus.Doing
              ? Container(
                  color: hex("#F4F4F4"),
                  height: 50,
                  padding: EdgeInsets.only(left: 15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '未完成',
                    style: TextStyle(
                      color: hex("#999"),
                      fontSize: 14,
                    ),
                  ),
                )
              : Container(),
          //显示已完成
          widget.data['isLoadFinished'] == i
              ? Container(
                  color: hex("#F4F4F4"),
                  height: 50,
                  padding: EdgeInsets.only(left: 15),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    '已完成',
                    style: TextStyle(
                      color: hex("#999"),
                      fontSize: 14,
                    ),
                  ),
                )
              : Container(),
          GestureDetector(
            onTap: () {
              this.routerDetail(item);
            },
            child: XdsListRow(
              status: status,
              title: '$projectName ',
              bottomLeft: bottomLeft,
              bottomRight: bottomRight,
            ),
          ),
        ],
      ),
    );
  }

  routerDetail(item) {
    String route = '/task_assignment';

    if (item['type'] == Tasktype.AssignWorkStation) {
      route = '/task_assignment';
    }
    if (item['type'] == Tasktype.AssignProcess) {
      route = '/task_assignment_process';
    }
    if (item['type'] == Tasktype.FeedBack) {
      route = "/task_progress_update";
    }
    if (item["processType"] == ProcessTypeEnum.Send) {
      route = "/task_progress_update_material";
    }
    // route = "/task_progress_update_material";
    G.pushNamed(route, arguments: {
      'planId': item['relatedId'],
      'taskId': item['id'],
      "deliveryStageId": item['deliveryStageId'],
      "workstationId": item['workstationId'],
    }).then((value) async {
      await widget.onRefresh();
      _contoller.jumpTo(0);
      _refreshController.resetNoData();
    });
  }

  // 下拉刷新
  void _onRefresh() async {
    await widget.onRefresh();
    if (mounted) {
      _refreshController.loadComplete();
    }
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  // 下拉刷新
  void _onLoading() async {
    // monitor network fetch
    await widget.onLoad();

    if (mounted) {
      // if failed,use loadFailed(),if no data return,use LoadNodata()
      if (widget.data['data'].length == widget.data['count']) {
        return _refreshController.loadNoData();
      }
    }

    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hex("#F4F4F4"),
      child: XdsPullToRefresh(
        controller: _refreshController,
        onLoading: this._onLoading,
        onRefresh: this._onRefresh,
        data: widget.data['data'],
        child: ListView.separated(
          itemCount: widget.data['data'].length,
          controller: _contoller,
          separatorBuilder: (c, i) {
            //显示已完成就不需要分割线
            if (widget.data['isLoadFinished'] != null &&
                i == widget.data['isLoadFinished'] - 1) {
              return Container();
            }
            return Container(
              height: 20,
              color: hex('#f4f4f4'),
            );
          },
          itemBuilder: (c, i) {
            return returnListRow(widget.data['data'][i], i);
          },
        ),
      ),
    );
  }
}
