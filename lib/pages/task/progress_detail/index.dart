import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/utils/global.dart';
import '../progress_update/process.dart';
import '../progress_update_material/material_process.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/parse_date.dart';

class ProgressDetail extends StatefulWidget {
  final Map args;
  ProgressDetail({
    Key key,
    @required this.args,
  }) : super(key: key);
  @override
  _ProgressDetailState createState() => _ProgressDetailState();
}

class _ProgressDetailState extends State<ProgressDetail> {
  String processId = 'c86d3845-9711-11e9-8f54-00155dca5a82';
  int processType;
  Map param = {
    "planId": "9cbc08f7-cd2e-4c13-a81b-edb24fe58b17",
    "planDetailId": "81e0319c-d850-409d-85ec-966c3276ad9f",
    "processName": "新建图纸修改",
    "userId": ""
  };
  Map data = {};
  @override
  void initState() {
    super.initState();
    this.setState(() {
      if (widget.args != null) {
        processId = widget.args['processId'];
        param = widget.args['param'];
      }
    });
    getProcessDetail();
  }

  getProcessDetail() {
    Request.getProcessDetail(processId, param).then((res) {
      this.setState(() {
        Map resData = res["data"];
        if (resData.isNotEmpty) {
          processType = resData["processType"];
          resData["startDateStirng"] =
              resData["startDateStirng"] = dealtimeSlash(resData["startDate"]);
          resData["endDateStirng"] = dealtimeSlash(resData["endDate"]);
          resData["actualStartDateStirng"] =
              dealtimeSlash(resData["actualStartDate"]);
          resData["actualEndDateStirng"] =
              dealtimeSlash(resData["actualEndDate"]);
          data = resData;
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: XdsAppBar(
          title: Text('工序详情'),
          leadingOnPressed: () {
            G.pop();
          },
        ),
        body: Container(
          color: hex("#f4f4f4"),
          padding: EdgeInsets.only(top: 25),
          child: Container(
            color: hex("#fff"),
            padding: EdgeInsets.fromLTRB(15, 10, 15, 0),
            child: Column(
              children: <Widget>[
                processType != ProcessTypeEnum.Send
                    ? Process(
                        false,
                        data,
                      )
                    : MaterialProcess(
                        false,
                        data,
                      )
              ],
            ),
          ),
        ));
  }
}
