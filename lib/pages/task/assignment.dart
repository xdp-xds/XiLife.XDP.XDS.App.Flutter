// import 'dart:convert';

import 'dart:convert';

import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/not_task_page.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/common_ui/xds_leader_card/card_type.dart';
import 'package:xdsapp/common_ui/xds_leader_card/index.dart';
import 'package:xdsapp/common_ui/xds_project_base_detail/index.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/constant/index.dart';

class Assignment extends StatefulWidget {
  Assignment({
    Key key,
    @required this.args,
  }) : super(key: key);

  final Map args;

  @override
  _AssignmentState createState() => _AssignmentState();
}

class _AssignmentState extends State<Assignment> {
  /// ```
  /// [
  ///   // 交互阶段
  ///   {
  ///     /// 展开收起
  ///     collapse: true
  ///     /// 所有工站
  ///     workStations: [XdsLeaderCardData]
  ///     /// 所有工序
  ///     workProcess: {
  ///       workStationId: XdsLeaderCardData,
  ///       workStationId: XdsLeaderCardData,
  ///     }
  ///   }
  /// ]
  /// ```
  List data = [];

  /// 编辑状态  控制所有的负责人编辑状态
  bool isEdit = false;

  String planId = '0a99a9b9-01fe-40dc-8ccd-1c5952fc137f';

  String taskId = '31ccaccd-7d19-467b-b20d-0d3e064a11ff';

  String deliveryStageId = 'a8dfd63e-b987-11e9-a05f-00155dca5a82';

  /// 项目详情
  Map projectDetailData = {};

  /// 当前操作的工站索引
  int currentWorkStationIndex;

  /// 当前操作的交互阶段索引
  int currentDeliveryStageIndex = 0;

  /// 当前操作的交互阶段索引
  int currentProcessIndex;

  /// 确认弹窗
  Confirm confirm;

  /// loading
  Loading loading;

  /// alert
  Alert alert;

  /// 返回页面是否弹窗
  bool isShowConfirm = false;

  Map taskDetail = {};

  @override
  void initState() {
    super.initState();

    /// 初始化弹窗 & loading & alert
    confirm = XdsDialog.confirm(context);
    loading = XdsDialog.loading(context);
    alert = XdsDialog.alert(context);

    planId = widget.args['planId'];
    deliveryStageId = widget.args['deliveryStageId'];
    taskId = widget.args['taskId'];
    // isEdit = widget.args['isEdit'];

    Future.delayed(Duration.zero, () async {
      loading.show();
      try {
        ///获取任务状态，判断是编辑还是详情
        taskDetail = await Request.getTaskDetail(taskId);
      } catch(e) {
        return setState(() {
          taskDetail = null;
          loading.hide();
        });
      }

      await getProjectDetail();

      isEdit = taskDetail['status'] == TaskStatus.Finished ? false : true;

      List res = await getWorkStation();
      // String str = json.encode(res);
      // print(str);
      setState(() {
        data = res;
        taskDetail = taskDetail;
        loading.hide();
      });
    });
  }

  /// 获取工站信息，并映射数据
  getWorkStation() async {
    try {
      List res = await Request.getWorkStation(
          planId: planId,
          deliveryStageId: deliveryStageId,
          isAll: !isEdit,
          taskId: taskId);

      /// 映射交互阶段
      res.forEach((val) {
        val['collapse'] = false;
        val['workProcess'] = {};

        /// 映射工站数据 =》 [XdsLeaderCardData]
        List workStations = val['workStations'];
        val['workStations'] = workStations.map((data) {
          Map<String, dynamic> json = {
            "workStation": data['workStation'],
            "workProcedure": data['processNames'],
            "leader": {
              "defaultValue": isEdit ? null : data['planChargePerson'],
              "list": []
            },
            "remark": isEdit ? null : data['remark'],
            "isEdit": isEdit,
            "verifyLeader": false,
            "validation": true,
            "verifyRemark": false,
            "collapse": isEdit ? true : false
          };

          return XdsLeaderCardData.fromJson(json);
        }).toList();
      });

      return res;
    } catch (e) {
    }
  }

  /// 对比userId请求工序
  getWorkProcedure(Map<String, dynamic> personData) async {
    String id = G.user.workInfo.id;

    /// 当前交互阶段
    var currentDeliveryStage = data[currentDeliveryStageIndex];

    /// 当前操作的工站信息
    XdsLeaderCardData currentWorkStation =
        currentDeliveryStage['workStations'][currentWorkStationIndex];

    /// 工站id
    String workStationId = currentWorkStation.workStation.id;

    /// 开启验证
    currentWorkStation.verifyLeader = true;

    /// 没有选择负责人
    if (personData == null)
      return setState(() {
        data[currentDeliveryStageIndex] = currentDeliveryStage;
      });

    currentWorkStation.leader.defaultValue = DefaultValue.fromJson(personData);

    /// 当期选择的负责人不是项目经理
    // if (id != personData['id']) {
    //   currentDeliveryStage['workProcess'].remove('$workStationId');
    /// 修改需求：如果是项目经理不在请求工序
      setState(() {
        data[currentDeliveryStageIndex] = currentDeliveryStage;
      });

      return;
    // }

    List<XdsLeaderCardData> processDataList =
        currentDeliveryStage['workProcess']['$workStationId'];

    /// 当前工序已经存在，不在发起请求
    if (processDataList != null && processDataList.isNotEmpty) return;

    try {
      List res = await Request.getWorkProcedure(
          planId: planId,
          workStationId: workStationId,
          isAll: !isEdit,
          taskId: taskId);

      List processes = res[0]['processes'];

      List<XdsLeaderCardData> _processesData = processes.map((val) {
        Map<String, dynamic> json = {
          "workStation": {
            "id": val['process']['id'],
            "name": val['process']['name']
          },
          "workProcedure": '',
          "leader": {
            "defaultValue": isEdit ? null : val['planChargePerson'],
            "list": null
          },
          "remark": isEdit ? null : val['remark'],
          "isEdit": isEdit,
          "verifyLeader": false,
          "validation": true,
          "verifyRemark": false,
        };

        return XdsLeaderCardData.fromJson(json);
      }).toList();

      currentDeliveryStage['workProcess']['$workStationId'] = _processesData;
      setState(() {
        data[currentDeliveryStageIndex] = currentDeliveryStage;
      });
    } catch (e) {
      print(e);
    }
  }

  /// 保存之前验证
  String saveVerify() {
    Map<String, dynamic> command = {
      /// 工站
      "deliveryCommand": [],

      /// 工序
      "workstationCommand": []
    };

    List deliveryCommand = command['deliveryCommand'];
    List workstationCommand = command['workstationCommand'];

    Map currentDeliveryStage = data[currentDeliveryStageIndex];

    List<XdsLeaderCardData> workStations = currentDeliveryStage['workStations'];

    bool validation = true;

    /// 整理传给后端的数据
    for (int a = 0, length = workStations.length; a < length; a++) {
      XdsLeaderCardData val = workStations[a];
      String workStationId = val.workStation.id;

      /// 当前工站的工序
      List<XdsLeaderCardData> workProcess =
          currentDeliveryStage['workProcess'][workStationId];

      val.verifyLeader = true;
      if (val.leader.defaultValue == null ||
          val.leader.defaultValue.id == '0') {
        val.validation = false;
        validation = false;
        continue;
      }

      /// 工站
      Map workStationsJson = {
        "planId": planId,
        "deliveryStageId": deliveryStageId,
        "workStationId": workStationId,
        "chargePersonId": val.leader.defaultValue.id,
        "chargeType": 1, // 1：工站  2：工序
        "processId": null,
        "remark": val.remark
      };

      deliveryCommand.add(workStationsJson);

      /// 工序
      if (workProcess != null && workProcess.isNotEmpty) {
        List temp = [];
        for (int i = 0, len = workProcess.length; i < len; i++) {
          XdsLeaderCardData process = workProcess[i];
          process.verifyLeader = true;
          if (process.leader.defaultValue == null ||
              process.leader.defaultValue.id == '0') {
            process.validation = false;
            validation = false;
            continue;
          }

          Map procressJson = {
            "planId": planId,
            "deliveryStageId": deliveryStageId,
            "workStationId": workStationId,
            "chargePersonId": process.leader.defaultValue.id,
            "chargeType": 2,
            "processId": process.workStation.id,
            "remark": process.remark
          };
          temp.add(procressJson);
        }

        workstationCommand.add({
          "workStationId": workStationId,
          "addPlanChargePersonCommands": temp
        });
      }
    }

    if (!validation) {
      setState(() {
        data[currentDeliveryStageIndex] = currentDeliveryStage;
      });
      return null;
    }

    String jsonString = json.encode(command);

    return jsonString;
  }

  /// 保存
  save(String command) async {
    loading.show();

    try {
      await Request.putChargeperson(
        data: command,
        taskId: taskId,
        planId: planId,
        deliveryStageId: deliveryStageId,
      );
      loading.hide();
      alert.success(onClose: () {
        setState(() {
          isEdit = false;
        });
      });
    } catch (e) {
      loading.hide();
      String mes;
      try {
        mes = e.response.data[0];
        alert.error(desc: mes);
      } catch (err) {
        alert.error(desc: '重新保存');
      }
    }
  }

  /// 获取项目详情
  getProjectDetail() async {
    try {
      var res = await Request.getProjectDetail(planId);
      if (res['isSuccessStatusCode'] == true) {
        setState(() {
          projectDetailData = res['data'];
        });
      }
    } catch(e) {
      print(e);
    }
  }

  List<Widget> buildContent() {
    List<Widget> collapses = [];
    data.asMap().forEach((index, val) {
      /// 交互阶段
      // Map<String, dynamic> deliveryStage = val['deliveryStage'];

      /// 工站
      List<XdsLeaderCardData> workStations = val['workStations'];

      collapses.add(Container(
          // color: hex('#fff'),
          child: Column(children: buildLeaders(workStations, index))));
    });

    return collapses;
  }

  /// 负责人
  List<Widget> buildLeaders(
      List<XdsLeaderCardData> workStationsData,

      /// 当前交付阶段索引
      int index) {
    List<Widget> collapse = [];

    /// 项目经理id
    String projectManagerId =
        projectDetailData['planBasicInfo']['projectManagerId'];

    workStationsData.asMap().forEach((workStationIndex, workStationVal) {
      /// 工站id
      String workStationId = workStationVal.workStation.id;

      /// 工序List
      List process = data[index]['workProcess'][workStationId];

      List<Widget> collapseBody = [];

      XdsLeaderCardData currenWorkStation = data[index]['workStations'][workStationIndex];
      collapseBody.add(XdsLeaderCard(
        disabledAddRemark: currenWorkStation.leader.defaultValue == null || currenWorkStation.leader.defaultValue.id == null ? true : false,
        key: Key(workStationId),
        isEdit: isEdit,
        data: workStationVal,
        bottomBorder: process == null ? false : true,
        onLeaderPress: (XdsLeaderCardData xdsLeaderCardData, json) {
          currentWorkStationIndex = workStationIndex;
          currentDeliveryStageIndex = index;
          isShowConfirm = true;

          /// 当前选中的负责人
          // xdsLeaderCardData.leader.defaultValue.id;
          G.pushNamed('/choose_worker', arguments: {
            "id": projectManagerId,
          }).then((data) {
            Map<String, dynamic> person = (data == null)
                ? null
                : {
                    "id": data['id'],
                    "name": data['name'],
                    "phone": data['phone'],
                  };

            getWorkProcedure(person);
          });
        },
        onChange: (XdsLeaderCardData xdsLeaderCardData, json) {
          isShowConfirm = true;
          setState(() {
            data[index]['workStations'][workStationIndex] = xdsLeaderCardData;
          });
        },
      ));

      /// 工序负责人build
      if (process != null) {
        int processLen = process.length;
        process.asMap().forEach((processIndex, processVal) {
          XdsLeaderCardData currenWorkProcess = data[index]['workProcess'][workStationId][processIndex];
          collapseBody.add(Container(
            padding: EdgeInsets.only(top: 20),
            child: Column(
              children: <Widget>[
                buildTitle(data: processVal, type: CardType.workProcedure),
                XdsLeaderCard(
                  disabledAddRemark: currenWorkProcess.leader.defaultValue == null || currenWorkProcess.leader.defaultValue.id == null ? true : false,
                  key: Key(processVal.workStation.id),
                  isEdit: isEdit,
                  data: processVal,
                  bottomBorder: processLen - 1 == processIndex ? false : true,
                  type: CardType.workProcedure,
                  onLeaderPress: (XdsLeaderCardData processLeaderData, json) {
                    currentWorkStationIndex = workStationIndex;
                    currentDeliveryStageIndex = index;
                    currentProcessIndex = processIndex;

                    /// 当前选中的负责人
                    // xdsLeaderCardData.leader.defaultValue.id;
                    G.pushNamed('/choose_worker', arguments: {
                      "id": projectManagerId,
                    }).then((data) {
                      Map<String, dynamic> person = (data == null)
                          ? null
                          : {
                              "id": data['id'],
                              "name": data['name'],
                              "phone": data['phone'],
                            };

                      /// 开启负责人验证
                      processLeaderData.verifyLeader = true;
                      if (person != null) {
                        processLeaderData.leader.defaultValue =
                            DefaultValue.fromJson(person);
                      }
                    });
                  },
                  onChange: (XdsLeaderCardData xdsLeaderCardData, json) {
                    setState(() {
                      data[index]['workProcess'][workStationId][processIndex] =
                          xdsLeaderCardData;
                    });
                  },
                ),
              ],
            ),
          ));
        });
      }

      /// 展开收起模块
      collapse.add(Container(
        color: hex('#fff'),
        margin: EdgeInsets.only(bottom: 20),
        padding: EdgeInsets.only(top: 10),
        child: XdsCollapse(
          key: Key(workStationId),
          value: workStationVal.collapse,
          // icon: isEdit ? Container() : null,
          title: Container(
            alignment: Alignment.center,
            height: 50,
            child: buildTitle(data: workStationVal, type: CardType.workStation),
          ),
          body: Column(
            children: collapseBody,
          ),
          onChange: (bool collapseVal) {
            currentWorkStationIndex = workStationIndex;
            currentDeliveryStageIndex = index;
            workStationVal.collapse = collapseVal;
            if (collapseVal) {
              DefaultValue defaultValue = workStationVal.leader.defaultValue;

              Map<String, dynamic> person =
                  defaultValue == null ? null : defaultValue.toJson();
              getWorkProcedure(person);
            }
            setState(() {
              data[index]['workStations'][workStationIndex] = workStationVal;
            });
          },
        ),
      ));
    });

    return collapse;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('分配负责人'),
        leadingOnPressed: () {
          if (!isEdit) return G.pop();
          isShowConfirm
              ? confirm.show(
                  title: '取消将不保存当前信息',
                  subtitle: '取消将不保存当前信息',
                  onOk: () {
                    G.pop();
                  },
                )
              : G.pop();
        },
      ),
      body: taskDetail == null
          ? NotTaskPage()
          : SafeArea(
              top: false,
              bottom: isEdit,
              child: Stack(
                children: [
                  /// 项目信息
                  SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(bottom: isEdit ? 84 : 0),
                      color: hex('#f4f4f4'),
                      child: projectDetailData == null ||
                              projectDetailData['planBasicInfo'] == null
                          ? Container()
                          : Column(
                              children: <Widget>[
                                Container(
                                  color: hex('#fff'),
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: BaseDetail(
                                      onTapProjectName: () => G.pushNamed(
                                          '/project_detail',
                                          arguments: {'id': planId}),
                                      data: projectDetailData['planBasicInfo'],
                                      currentPlanDetails: projectDetailData[
                                          'currentPlanDetails']),
                                ),
                                Container(
                                    constraints: BoxConstraints(
                                        minHeight: G.screenHeight - 88),
                                    child: Column(
                                      children: buildContent(),
                                    ))
                              ],
                            ),
                    ),
                  ),
                  isEdit
                      ? XdsBottomButton(
                          /// 底部取消
                          onCancel: () {
                            isShowConfirm
                                ? confirm.show(
                                    title: '取消将不保存当前信息',
                                    subtitle: '取消将不保存当前信息',
                                    onOk: () {
                                      G.pop();
                                    },
                                  )
                                : G.pop();
                          },
                          onConfirm: () {
                            String command = saveVerify();
                            if (command == null)
                              return alert.error(title: '负责人不能为空', desc: " ");
                            confirm.show(
                              title: '确认之后立即发起派工',
                              subtitle: '确认之后不可撤回',
                              onOk: () {
                                save(command);
                              },
                            );
                          },
                        )
                      : Container()
                ],
              ),
            ),
    );
  }
}
