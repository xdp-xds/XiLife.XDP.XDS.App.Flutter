import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/parse_date.dart';
import 'package:xdsapp/pages/task/components/process_remark.dart';
import 'package:xdsapp/pages/task/components/build_title.dart';
import 'package:xdsapp/pages/task/components/build_row.dart';
import 'package:xdsapp/pages/task/components/build_remark.dart';

class Process extends StatefulWidget {
  final bool isEdit;
  final Function(String val) onRemarkChange;
  final Function(List val) onPickerChange;
  final int selectIndex;
  final Map data;
  final Map selectItem;
  final TextEditingController controller;
  final List planDetailsStatus;
  Process(this.isEdit, this.data,
      {this.onRemarkChange,
      this.onPickerChange,
      this.selectIndex,
      this.selectItem,
      this.controller,
      this.planDetailsStatus});
  @override
  _ProcessState createState() => _ProcessState();
}

class _ProcessState extends State<Process> {
  // final Function() onPickerChange;
  Map data = {};

  Widget remarkText() {
    List logs = widget.data["logs"];
    List<Widget> logList = [];
    if (logs != null) {
      int logLen = logs.length;
      if (logLen > 0) {
        logList.add(row("进度备注"));
      }
      logs.forEach((item) {
        String time = dealtimeSlashHourAndMinute(item["logTime"]);
        String text = item["content"];
        logList.addAll(renarkItem(time, text));
      });
    }
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start, children: logList);
  }

  List<Widget> renarkItem(time, text) {
    return [
      Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Text(
          "$time",
          style: TextStyle(color: hex('#666'), fontSize: 14),
        ),
      ),
      Container(
        margin: EdgeInsets.only(bottom: 15),
        child: Text(
          "$text",
          style: TextStyle(color: hex('#111'), fontSize: 14),
        ),
      ),
      Container(
        margin: EdgeInsets.only(bottom: 15),
        decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: hex('#e4e4e4'), width: 1)),
        ),
      )
    ];
  }

  showPickerModal(BuildContext context) {
    if (widget.planDetailsStatus.isEmpty) return;

    Picker(
        selecteds: [widget.selectIndex],
        height: 264,
        itemExtent: 44,
        cancelText: '取消',
        confirmText: '确认',
        cancelTextStyle: TextStyle(color: hex('#1989fa')),
        confirmTextStyle: TextStyle(color: hex('#1989fa')),
        adapter: PickerDataAdapter<String>(
            pickerdata: widget.planDetailsStatus.map((val) {
          return val["text"];
        }).toList()),
        changeToFirst: false,
        hideHeader: false,
        onConfirm: (Picker picker, List value) {
          widget.onPickerChange(value);
        }).showModal(context);
  }

  @override
  Widget build(BuildContext context) {
    Map data = widget.data;
    String status = PlanDetailsStatus.planDetailsStatusText[data['status']];

    String planDate =
        data["startDateStirng"] == "" && data["endDateStirng"] == ""
            ? ""
            : "${data["startDateStirng"]} - ${data["endDateStirng"]}";
    String actualDate = data["actualStartDateStirng"] == "" &&
            data["actualEndDateStirng"] == ""
        ? ""
        : "${data["actualStartDateStirng"]} - ${data["actualEndDateStirng"]}";
    return Container(
      child: Column(
        children: <Widget>[
          title(widget.data['name']),
          processRemark(widget.data['remark'],
              userName: data["userName"], phone: data["phone"]),
          Container(
            child: Column(
              children: <Widget>[
                //编辑状态 施工状态
                widget.isEdit
                    ? Container(
                        margin: EdgeInsets.only(bottom: 15),
                        child: Row(
                          children: <Widget>[
                            Text(
                              "施工状态：",
                              style:
                                  TextStyle(color: hex('#333'), fontSize: 15),
                            ),
                            Expanded(
                              child: GestureDetector(
                                onTap: () => showPickerModal(context),
                                child: Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  height: 44,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: hex("#979797")),
                                      borderRadius: BorderRadius.circular(4)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text("${widget.selectItem["text"]}"),
                                      Container(
                                        margin: EdgeInsets.only(right: 10),
                                        child: iconicontest(
                                            size: 10, color: hex("#393939")),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    : Container(),
                widget.isEdit == false
                    ? row("施工状态", content: status)
                    : Container(),
                row("计划时间", content: "$planDate"),
                row("实际时间", content: "$actualDate"),
                //备注信息
                remarkText(),
                // 备注输入框
                widget.isEdit
                    ? buildRemark(
                        "进度备注", widget.controller, widget.onRemarkChange)
                    : Container()
              ],
            ),
          )
        ],
      ),
    );
  }
}
