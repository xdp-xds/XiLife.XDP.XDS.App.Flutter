// import 'dart:convert';

import 'dart:convert';

import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/not_task_page.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/common_ui/xds_leader_card/card_type.dart';
import 'package:xdsapp/common_ui/xds_leader_card/index.dart';
import 'package:xdsapp/common_ui/xds_project_base_detail/index.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';
import 'package:xdsapp/pages/project_detail/components/collapse_title.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/constant/index.dart';

class AssignmentProcess extends StatefulWidget {
  AssignmentProcess({
    Key key,
    @required this.args,
  }) : super(key: key);

  final Map args;

  @override
  _AssignmentProcessState createState() => _AssignmentProcessState();
}

class _AssignmentProcessState extends State<AssignmentProcess> {
  /// ```
  /// [
  ///   {
  ///     collapse: true
  ///     workStations: [XdsLeaderCardData]
  ///   }
  /// ]
  /// ```
  List data = [];

  /// 编辑状态  控制所有的负责人编辑状态
  bool isEdit = false;

  String planId = "3fa7687a-bb9e-4f6b-91d9-368148a985c4";

  String workstationId = "278427sd-bf04-11x9-8e67-00155dca5a80";

  String taskId = "";

  /// 项目详情
  Map projectDetailData = {};

  /// loading
  Loading loading;

  /// config
  Confirm confirm;

  /// alert
  Alert alert;

  /// 返回页面是否弹窗
  bool isShowConfirm = false;

  Map taskDetail = {};

  @override
  void initState() {
    super.initState();

    /// 初始化
    loading = XdsDialog.loading(context);
    confirm = XdsDialog.confirm(context);
    alert = XdsDialog.alert(context);

    planId = widget.args['planId'];
    workstationId = widget.args['workstationId'];
    // isEdit = widget.args['isEdit'];
    taskId = widget.args['taskId'];

    Future.delayed(Duration.zero, () async {
      loading.show();
      try {
        ///获取任务状态，判断是编辑还是详情
        taskDetail = await Request.getTaskDetail(taskId);
      } catch(e) {
        return setState(() {
          taskDetail = null;
          loading.hide();
        });
      }

      getProjectDetail();

      isEdit = taskDetail['status'] == TaskStatus.Finished ? false : true;

      var res = await getWorkProcedure();

      setState(() {
        data = res;
        taskDetail = taskDetail;
        loading.hide();
      });
    });
  }

  onTapProjectName() {
    G.pushNamed('/project_detail', arguments: {'id': planId});
  }

  /// 获取工序
  getWorkProcedure() async {
    try {
      var res = await Request.getWorkProcedure(
          planId: planId,
          workStationId: workstationId,
          isAll: !isEdit,
          taskId: taskId);
      res.forEach((val) {
        val['collapse'] = true;

        /// 映射工序数据 =》 [XdsLeaderCardData]
        /// 当前页面的 [XdsLeaderCardData]代表工序数据
        /// workStation => process
        List processes = val['processes'];

        List<XdsLeaderCardData> processList = [];

        for(int i = 0,len = processes.length;i<len;i++) {
          Map data = processes[i];
          Map process = data['process'];
          
          /// 负责人已经选择不在加入
          // if(data['planChargePerson'] != null) continue;

          Map<String, dynamic> json = {
            "workStation": {"id": process['id'], "name": process['name']},
            "workProcedure": null,
            "leader": {
              "defaultValue": isEdit ? null : data['planChargePerson'],
              // "list": data['availablePlanChargePersons']
            },
            "remark": isEdit ? null : data['remark'],
            "isEdit": isEdit,
            "verifyLeader": false,
            "validation": true,
            "verifyRemark": false,
          };

          processList.add(XdsLeaderCardData.fromJson(json));
        }

        val['processes'] = processList;

        // val['processes'] = processes.map((data) {
        //   Map process = data['process'];

        //   Map<String, dynamic> json = {
        //     "workStation": {"id": process['id'], "name": process['name']},
        //     "workProcedure": null,
        //     "leader": {
        //       "defaultValue": isEdit ? null : data['planChargePerson'],
        //       // "list": data['availablePlanChargePersons']
        //     },
        //     "remark": isEdit ? null : data['remark'],
        //     "isEdit": isEdit,
        //     "verifyLeader": false,
        //     "validation": true,
        //     "verifyRemark": false,
        //   };
        //   return XdsLeaderCardData.fromJson(json);
        // }).toList();
      });

      return res;
    } catch (e) {
    }
  }

  /// 获取项目详情
  getProjectDetail() async {
    var res = await Request.getProjectDetail(planId);

    if (res['isSuccessStatusCode'] == true) {
      setState(() {
        projectDetailData = res['data'];
      });
    }
  }

  /// 负责人
  /// @index 当前交付阶段索引
  List<Widget> buildLeaders(
      List<XdsLeaderCardData> workStationsData, int index) {

    List<Widget> widgets = [];

    if(workStationsData.isEmpty) return widgets;


    /// 项目经理id
    String projectManagerId =
        projectDetailData['planBasicInfo']['projectManagerId'];

    XdsLeaderCardData lastData = workStationsData.last;

    workStationsData.asMap().forEach((leaderIndex, val) {
      XdsLeaderCardData currentworkStations = data[index]['processes'][leaderIndex];
      widgets.add(Column(
        children: <Widget>[
          Container(
            child: buildTitle(data: val, type: CardType.workProcedure),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 20),
            child: XdsLeaderCard(
              disabledAddRemark: currentworkStations.leader.defaultValue == null || currentworkStations.leader.defaultValue.id == null ? true : false,
              onLeaderPress: (XdsLeaderCardData xdsLeaderCardData, json) {
                isShowConfirm = true;

                /// 选中的负责人
                G.pushNamed('/choose_worker',
                    arguments: {"id": projectManagerId}).then((args) {
                  Map<String, dynamic> person = args == null
                      ? null
                      : {
                          "id": args['id'],
                          "name": args['name'],
                          "phone": args['phone'],
                        };

                  if (person != null) {
                    xdsLeaderCardData.leader.defaultValue =
                        DefaultValue.fromJson(person);
                  }

                  /// 开启验证
                  xdsLeaderCardData.verifyLeader = false;

                  setState(() {
                    data[index]['processes'][leaderIndex] = xdsLeaderCardData;
                  });
                });
              },
              type: CardType.workProcedure,
              isEdit: isEdit,
              data: val,
              bottomBorder:
                  lastData.workStation.id == val.workStation.id ? false : true,
              onChange: (XdsLeaderCardData xdsLeaderCardData, json) {
                isShowConfirm = true;
                setState(() {
                  data[index]['processes'][leaderIndex] = xdsLeaderCardData;
                });
              },
            ),
          ),
        ],
      ));
    });

    return widgets;
  }

  /// 保存之前检测
  String saveVerify() {
    bool validation = false;

    Map<String, dynamic> command = {
      "workStationId": workstationId,
      "addPlanChargePersonCommands": []
    };

    data.forEach((val) {
      List<XdsLeaderCardData> processes = val['processes'];

      val['processes'] = processes.map((process) {
        /// 开启负责人验证
        process.verifyLeader = false;
        if (process.leader.defaultValue == null ||
            process.leader.defaultValue.id == '0') {
          process.validation = false;
          // validation = false;
        } else {
          validation = true;
          Map<String, dynamic> json = {
            "planId": planId,
            "deliveryStageId": null,
            "workStationId": workstationId,
            "chargePersonId": process.leader.defaultValue.id,
            "chargeType": 2,
            "processId": process.workStation.id,
            "remark": process.remark
          };

          command['addPlanChargePersonCommands'].add(json);
        }

        return process;
      }).toList();
    });

    if (!validation) {
      setState(() {
        data = data;
      });
      return null;
    }

    return json.encode(command);
  }

  save(String command) async {
    loading.show();

    try {
      await Request.chargepersonProcess(
        data: command,
        taskId: taskId,
        planId: planId,
      );
      loading.hide();
      alert.success(onClose: () {
        setState(() {
          isEdit = false;
        });
      });
    } catch (e) {
      loading.hide();
      String mes;
      try {
        mes = e.response.data[0];
        alert.error(desc: mes);
      } catch (err) {
        alert.error(desc: '重新保存');
      }
    }
  }

  /// 展开收起组件
  List<Widget> buildCollapses() {
    List<Widget> collapses = [];
    data.asMap().forEach((index, val) {
      /// 工站
      Map<String, dynamic> workStation = val['workStation'];

      /// 工序
      List<XdsLeaderCardData> processes = val['processes'];

      collapses.add(Container(
        margin: EdgeInsets.only(bottom: 20),
        color: hex('#fff'),
        child: XdsCollapse(
          icon: Container(),
          key: Key(workStation['id']),
          title: CollapseTitle(
              title: workStation['name'],
              // iconIndex: index,
              icon: iconjizhuangshigong(color: hex('#13BB87'), size: 18)),
          value: val['collapse'],
          padding: EdgeInsets.only(top: 15, left: 20, right: 20, bottom: 5),
          body: Container(
              child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Expanded(
                    child: val['remark'] != null
                        ? Container(
                            color: hex('#f4f4f4'),
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.fromLTRB(0, 12, 0, 10),
                            child: Text(
                              "备注：${val['remark'] != null ? val['remark'] : ""}",
                              style: new TextStyle(
                                  color: hex('#111'), fontSize: 15),
                            ),
                          )
                        : Container(),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 12, bottom: 12),
                decoration: BoxDecoration(
                    // color: hex('#f4f4f4'),
                    border: Border(bottom: BorderSide(color: hex('#d8d8d8')))),
              ),
              Column(children: buildLeaders(processes, index))
            ],
          )),
          onChange: (bool val) {
            // setState(() {
            //   data[index]['collapse'] = val;
            // });
          },
        ),
      ));
    });

    return collapses;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('分配负责人'),
        leadingOnPressed: () {
          if (!isEdit) return G.pop();
          isShowConfirm
              ? confirm.show(
                  title: '取消将不保存当前信息',
                  subtitle: '取消将不保存当前信息',
                  onOk: () {
                    G.pop();
                  },
                )
              : G.pop();
        },
      ),
      body: taskDetail == null
          ? NotTaskPage()
          : SafeArea(
              top: false,
              bottom: isEdit,
              child: Stack(
                children: [
                  SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(bottom: isEdit ? 84 : 0),
                      color: hex('#f4f4f4'),
                      child: projectDetailData == null ||
                              projectDetailData['planBasicInfo'] == null
                          ? Container()
                          : Column(
                              children: <Widget>[
                                Container(
                                  color: hex('#fff'),
                                  margin: EdgeInsets.only(bottom: 20),
                                  child: BaseDetail(
                                    data: projectDetailData['planBasicInfo'],
                                    currentPlanDetails:
                                        projectDetailData['currentPlanDetails'],
                                    onTapProjectName: this.onTapProjectName,
                                  ),
                                ),
                                Container(
                                  constraints: BoxConstraints(
                                      minHeight: G.screenHeight - 88),
                                  child: Column(
                                    children: buildCollapses(),
                                  ),
                                )
                              ],
                            ),
                    ),
                  ),
                  isEdit
                      ? XdsBottomButton(
                          /// 底部取消
                          onCancel: () {
                            isShowConfirm
                                ? confirm.show(
                                    title: '取消将不保存当前信息',
                                    subtitle: '取消将不保存当前信息',
                                    onOk: () {
                                      G.pop();
                                    },
                                  )
                                : G.pop();
                          },

                          /// 底部确认
                          onConfirm: () {
                            String command = saveVerify();
                            if (command == null) {
                              return alert.error(title: '未做任何修改', desc: " ");
                            }

                            confirm.show(
                              title: '确认之后立即发起派工',
                              subtitle: '确认之后不可撤回',
                              onOk: () {
                                save(command);
                              },
                            );
                          },
                        )
                      : Container()
                ],
              ),
            ),
    );
  }
}
