import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';

Widget title(name) {
  //widget.data['name']
  return Container(
    padding: EdgeInsets.fromLTRB(0, 10, 0, 25),
    child: Row(
      children: <Widget>[
        Container(
            margin: EdgeInsets.only(right: 5),
            child: Icon(
              Icons.star,
              size: 16,
            )),
        Text('${name != null ? name : ''}',
            style: TextStyle(
                color: hex('#111'), fontSize: 15, fontWeight: FontWeight.bold))
      ],
    ),
  );
}
