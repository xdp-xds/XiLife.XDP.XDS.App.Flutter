import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/utils/global.dart';

Widget contactInfor(userName, phone) {
  return Row(
    // crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Text(
        "负责人：${userName == null ? "" : userName} / ${phone == null ? "" : phone}",
        style: TextStyle(
          color: hex('#111'),
          fontSize: 15,
        ),
      ),
      Container(
        margin: EdgeInsets.only(left: 10),
        child: InkWell(
          onTap: () {
            G.callPhone(int.parse(phone));
          },
          child: Container(
            padding: EdgeInsets.only(bottom: 2),
            width: 30,
            height: 30,
            child: icondianhua(color: hex('#13BB87'), size: 15),
          ),
        ),
      )
    ],
  );
}
