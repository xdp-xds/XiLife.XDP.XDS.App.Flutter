import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';
import 'package:xdsapp/pages/task/components/build_contact_row.dart';

Widget processRemark(remark, {String userName, String phone}) {
  return Container(
    child:
        (remark != "" && remark != null) || (userName != null && phone != null)
            ? Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    color: hex("#f4f4f4"),
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        userName != null && phone != null
                            ? contactInfor(userName, phone)
                            : Container(),
                        remark != "" && remark != null
                            ? Text(
                                "任务备注：${remark != null ? remark : ''}",
                                style: TextStyle(
                                  color: hex('#111'),
                                  fontSize: 15,
                                ),
                              )
                            : Container()
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 25),
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: hex("#d8d8d8"), width: 1)),
                    ),
                  )
                ],
              )
            : Container(),
  );
}
