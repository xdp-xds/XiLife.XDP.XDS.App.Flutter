import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';
import './build_row.dart';

Widget buildRemark(label, controller, onRemarkChange) {
  //widget.controller
  return Column(
    children: <Widget>[
      row("$label"),
      Container(
        decoration: BoxDecoration(
            border: Border.all(color: hex("#979797")),
            borderRadius: BorderRadius.circular(4)),
        child: TextField(
          controller: controller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              contentPadding: EdgeInsets.all(10),
              hintText: '请填写备注',
              counterText: ''),
          maxLength: 100,
          maxLines: 3,
          onChanged: (String val) {
            onRemarkChange(val);
          },
        ),
      )
    ],
  );
}
