import 'package:flutter/material.dart';
import 'package:color_dart/color_dart.dart';

Widget row(label, {content}) {
  return Container(
    margin: EdgeInsets.only(bottom: 15),
    child: Row(
      // crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "$label：",
          style: TextStyle(color: hex('#333'), fontSize: 15),
        ),
        Expanded(
          child: Container(
              child: Wrap(
            children: <Widget>[
              Text("${content != null ? content : ''}",
                  style: TextStyle(
                    color: hex('#111'),
                    fontSize: 15,
                  )),
            ],
          )),
        )
      ],
    ),
  );
}
