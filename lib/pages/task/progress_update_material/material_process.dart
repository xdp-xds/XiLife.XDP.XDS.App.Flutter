import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/pages/task/components/process_remark.dart';
import 'package:xdsapp/pages/task/components/build_title.dart';
import 'package:xdsapp/pages/task/components/build_row.dart';
import 'package:xdsapp/pages/task/components/build_remark.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:xdsapp/utils/iconfont.dart';

class MaterialProcess extends StatelessWidget {
  final Map data;
  final bool isEdit;
  final Function(String val) onRemarkChange;
  final TextEditingController controller;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final String selectDate;
  final bool showError;
  final Function(String) onChangeDate;
  MaterialProcess(
    this.isEdit,
    this.data, {
    this.controller,
    this.onRemarkChange,
    this.scaffoldKey,
    this.selectDate,
    this.showError,
    this.onChangeDate,
    Key key,
  }) : super(key: key);

  showPickerDateTime(BuildContext context) {
    new Picker(
            adapter: new DateTimePickerAdapter(
                type: PickerDateTimeType.kYMD,
                isNumberMonth: true,
                yearSuffix: "年",
                monthSuffix: "月",
                daySuffix: "日"),
            title: new Text("选择日期"),
            height: 264,
            itemExtent: 44,
            cancelText: '取消',
            confirmText: '确认',
            cancelTextStyle: TextStyle(color: hex('#1989fa')),
            confirmTextStyle: TextStyle(color: hex('#1989fa')),
            changeToFirst: false,
            hideHeader: false,
            onConfirm: (Picker picker, List value) {
              onChangeDate(picker.adapter.text);
            },
            onSelect: (Picker picker, int index, List<int> selecteds) {})
        .show(scaffoldKey.currentState);
  }

  @override
  Widget build(BuildContext context) {
    String status = PlanDetailsStatus.planDetailsStatusText[data['status']];
    String remark = "";
    if (data["logs"] != null &&
        data["logs"].length > 0 &&
        data["logs"][0] != null) {
      remark = data["logs"][0]["content"];
    }
    return Container(
      child: Column(
        children: <Widget>[
          title(data['name']),
          processRemark(data['remark'],
              userName: data["userName"], phone: data["phone"]),
          Container(
              child: Column(
            children: <Widget>[
              row("任务状态", content: status),
              row("计划送料时间",
                  content:
                      "${data["startDateStirng"]} - ${data["endDateStirng"]}"),
              isEdit == false
                  ? row("实际送料时间",
                      content:
                          "${data["actualStartDateStirng"]} - ${data["actualEndDateStirng"]}")
                  : Container(
                      margin: EdgeInsets.only(bottom: 25),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "实际送料时间：",
                            style: TextStyle(color: hex('#333'), fontSize: 15),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () => this.showPickerDateTime(context),
                              child: Container(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                                height: 44,
                                decoration: BoxDecoration(
                                    border: Border.all(color: hex("#979797")),
                                    borderRadius: BorderRadius.circular(4)),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("$selectDate"),
                                    Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: iconrili(
                                          size: 18, color: hex("#13bb87")),
                                    )
                                  ],
                                ),
                              ),
                              // showError
                              //     ? Container(
                              //         child: Text(
                              //           "* 请选择实际送料材料",
                              //           style: TextStyle(
                              //               color: hex("#f61e1e")),
                              //         ),
                              //         margin:
                              //             EdgeInsets.fromLTRB(10, 8, 0, 0),
                              //       )
                              //     : Container()
                            ),
                          ),
                        ],
                      )),
              isEdit == false && remark != ""
                  ? row("送料备注", content: "$remark")
                  : Container(),
              isEdit == true
                  ? buildRemark("送料备注", controller, onRemarkChange)
                  : Container(),
            ],
          ))
        ],
      ),
    );
  }
}
