import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/common_ui/xds_project_base_detail/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/pages/task/progress_update_material/material_process.dart';
import 'package:xdsapp/utils/parse_date.dart';
import 'package:xdsapp/common_ui/not_task_page.dart';

class ProgressUpdateMaterial extends StatefulWidget {
  final Map args;
  ProgressUpdateMaterial({
    Key key,
    @required this.args,
  }) : super(key: key);
  @override
  _PProgressUpdateMaterialState createState() =>
      _PProgressUpdateMaterialState();
}

class _PProgressUpdateMaterialState extends State<ProgressUpdateMaterial> {
  /// 编辑状态  控制所有的负责人编辑状态
  bool isEdit = true;

  String planId = "aacc2f18-ec88-44f9-bf04-1498c9137f14";

  String taskId = "c47f1eae-0402-41fd-9761-c79430463c0a";

  /// 项目详情
  Map projectDetailData = {};

  /// config
  Confirm confirm;

  /// alert
  Alert alert;

  /// loading
  Loading loading;

  /// 进度更新数据
  Map data = {};

  String remark = "";

  /// ''显示填写备注输入框
  TextEditingController _controller;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ///选择的时间
  String selectDate = nowDate();

  ///校验
  bool showError = false;

  /// 返回页面是否弹窗
  bool isShowConfirm = false;

  /// 任务是否删除
  bool isDelete = false;

  @override
  void initState() {
    super.initState();

    /// 初始化
    loading = XdsDialog.loading(context);
    confirm = XdsDialog.confirm(context);
    alert = XdsDialog.alert(context);

    planId = widget.args['planId'];
    taskId = widget.args['taskId'];
    _controller = TextEditingController();
    intPage();
    if (selectDate.contains(" ")) {
      selectDate = selectDate.split(" ")[0];
    }
  }

  intPage() {
    getProjectDetail();
    getTaskDetail();
    getFeedbackDetail();
  }

  ///获取任务状态，判断是编辑还是详情
  getTaskDetail() async {
    try {
      Map response = await Request.getTaskDetail(taskId);
      bool isedit = false;
      if (response.length > 0) {
        response["status"] == TaskStatus.Finished
            ? isedit = false
            : isedit = true;
        setState(() {
          isEdit = isedit;
        });
      }
    } catch (e) {
      if (e.response.statusCode == 400) {
        setState(() {
          isDelete = true;
        });
      }
    }
  }

  /// 获取项目详情
  getProjectDetail() async {
    var res = await Request.getProjectDetail(planId);

    if (res['isSuccessStatusCode'] == true) {
      setState(() {
        projectDetailData = res['data'];
      });
    }
  }

  onTapProjectName() {
    G.pushNamed('/project_detail', arguments: {'id': planId});
  }

  getFeedbackDetail() {
    Request.getFeedbackDetail(taskId).then((res) {
      setState(() {
        if (res.length > 0 && res["planDetail"].length > 0) {
          Map resData = res["planDetail"];
          resData["startDateStirng"] = dealtimeSlash(resData["startDate"]);
          resData["endDateStirng"] = dealtimeSlash(resData["endDate"]);
          resData["actualStartDateStirng"] =
              dealtimeSlash(resData["actualStartDate"]);
          resData["actualEndDateStirng"] =
              dealtimeSlash(resData["actualEndDate"]);
          data = resData;
        }
      });
    });
  }

//清空输入框
  resetRemark() {
    _controller.clear();
  }

  onRemarkChange(remark) {
    this.setState(() {
      this.remark = remark;
      isShowConfirm = true;
    });
  }

  ///选中日期
  onChangeDate(date) {
    this.setState(() {
      selectDate = date.split(" ")[0];
      isShowConfirm = true;
    });
  }

  /// 保存之前检测
  String saveVerify() {
    if (selectDate == null && remark == "") {
      return null;
    }
    return "1";
  }

  save(String command) async {
    if (command == "1") {
      loading.show();
      try {
        Request.updateFeedbackMaterail(taskId, selectDate, remark).then((res) {
          loading.hide();
          alert.success(onClose: () {
            G.pop();
            // intPage();
            // resetRemark();
            // setState(() {
            //   remark = "";
            // });
          });
        });
      } catch (e) {
        loading.hide();
        String mes;
        try {
          mes = e.response.data[0];
          alert.error(desc: mes);
        } catch (err) {
          alert.error(desc: '重新保存');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: XdsAppBar(
          title: Text('进度反馈'),
          leadingOnPressed: () {
            if (!isEdit) return G.pop();
            isShowConfirm
                ? confirm.show(
                    title: '取消将不保存当前信息',
                    subtitle: '取消将不保存当前信息',
                    onOk: () {
                      G.pop();
                    },
                  )
                : G.pop();
          },
        ),
        body: isDelete
            ? NotTaskPage()
            : SafeArea(
                top: false,
                bottom: isEdit,
                child: Stack(children: [
                  Container(
                    padding: EdgeInsets.only(bottom: isEdit ? 84 : 0),
                    color: hex('#f4f4f4'),
                    child: projectDetailData == null ||
                            projectDetailData['planBasicInfo'] == null
                        ? Container()
                        : ListView(
                            padding: EdgeInsets.all(0),
                            children: <Widget>[
                              Container(
                                color: hex('#fff'),
                                margin: EdgeInsets.only(bottom: 20),
                                child: BaseDetail(
                                  data: projectDetailData['planBasicInfo'],
                                  currentPlanDetails:
                                      projectDetailData['currentPlanDetails'],
                                  onTapProjectName: this.onTapProjectName,
                                ),
                              ),
                              Container(
                                color: hex('#FFF'),
                                padding: EdgeInsets.all(15),
                                child: MaterialProcess(isEdit, data,
                                    controller: _controller,
                                    onRemarkChange: onRemarkChange,
                                    scaffoldKey: _scaffoldKey,
                                    selectDate: selectDate,
                                    showError: showError,
                                    onChangeDate: onChangeDate),
                              )
                            ],
                          ),
                  ),
                  isEdit
                      ? XdsBottomButton(
                          /// 底部取消
                          onCancel: () {
                            isShowConfirm
                                ? confirm.show(
                                    title: '取消将不保存当���信息',
                                    subtitle: '取消将不保存����前信息',
                                    onOk: () {
                                      G.pop();
                                    },
                                  )
                                : G.pop();
                          },

                          /// 底部确认
                          onConfirm: () {
                            String command = saveVerify();
                            if (command == null) {
                              return confirm.show(
                                title: '你没有修改任何信息',
                                subtitle: '',
                                onOk: () {},
                              );
                            }
                            return confirm.show(
                              title: '确认之后立即更新工序进度',
                              subtitle: '确认之后不可撤回',
                              onOk: () {
                                save(command);
                              },
                            );
                          },
                        )
                      : Container()
                ]),
              ));
  }
}
