import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class ProjectPauseDetail extends StatefulWidget {
  final Map args;
  ProjectPauseDetail({Key key, this.args}) : super(key: key);
  @override
  _ProjectPauseDetailState createState() => _ProjectPauseDetailState();
}

class _ProjectPauseDetailState extends State<ProjectPauseDetail> {
  Map pauseWork;
  List returnWorkList;
  List preparePauseWork = [];
  List pauseList = [];
  final planId = '107c2c6b-9922-4435-b201-8baffd8c803c';
  final statusColor = {
    1: {
      'type': '即将停工',
      'textColor': hex('#e80202'),
      'colorList': [hex('#F06464'), hex('#EB1A1A')],
    },
    2: {
      'type': '停工中',
      'textColor': hex('#e80202'),
      'colorList': [hex('#F06464'), hex('#EB1A1A')],
    },
    3: {
      'type': '已复工',
      'textColor': hex('#111111'),
      'colorList': [hex('#ADACBA'), hex('#808080')],
    }
  };

  @override
  void initState() {
    super.initState();
    // Request.getPlanPauseDetail(widget.args['planId']).then((res) {
    //   setState(() {
    //     pauseList = res['suspends'];
    //   });
    // });
    setState(() {
      pauseWork = widget.args['exceptions']['exceptionsOfFrozen']
          ['suspendingException'];
      returnWorkList =
          widget.args['exceptions']['exceptionsOfFrozen']['resumedExptions'];
    });
  }

  Widget dotLine() {
    return Image.asset('lib/assets/images/dashed.png');
  }

  Widget statusType(int status) {
    return Row(
      children: <Widget>[
        Container(
          width: 15,
          height: 15,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              gradient: LinearGradient(
                  colors: statusColor[status]['colorList'],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight)),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 7),
          child: Text(
            statusColor[status]['type'],
            style: TextStyle(
              color: statusColor[status]['textColor'],
            ),
          ),
        ),
      ],
    );
  }

  Widget baseTitle(int status, value) {
    return Container(
      height: 55,
      padding: const EdgeInsets.symmetric(horizontal: 15),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: hex('#e4e4e4'),
            width: 0.5,
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          statusType(status),
          status != 1
              ? Text(
                  '停工天数：${value['frozenDays']}天',
                  style: TextStyle(
                    color: statusColor[status]['textColor'],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget baseText(String type, String text) {
    return Container(
      margin: const EdgeInsets.only(bottom: 20),
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: Row(
        children: <Widget>[
          Text(
            type,
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                text,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget baseContent(String prefix, item) {
    var process =
        prefix == '停工' ? item['processName'] : item['resumeProcessName'];
    var date = prefix == '停工'
        ? G.handleDate(item['date'])
        : G.handleDate(item['resumeDate']);
    var reason = prefix == '停工' ? item['remark'] : item['resumeRemark'];
    var string = prefix == '停工' ? '原因' : '说明';
    return Container(
      padding: const EdgeInsets.only(top: 21.5),
      child: Column(
        children: <Widget>[
          item['status'] != 1
              ? baseText('$prefix工序:', '$process')
              : Container(),
          baseText(
            '$prefix时间:',
            '$date',
          ),
          reason != null && reason.length != 0
              ? baseText('$prefix$string:', '$reason')
              : Container(),
        ],
      ),
    );
  }

  Widget basePart(value) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      decoration: BoxDecoration(color: hex('#fff')),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          baseTitle(value['status'], value),
          value['status'] == 3 ? baseContent('复工', value) : Container(),
          value['status'] == 3
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: dotLine(),
                )
              : Container(),
          baseContent('停工', value)
        ],
      ),
    );
  }

  Widget returnWork() {
    return GestureDetector(
      onTap: () {},
      child: Padding(
        padding: const EdgeInsets.only(right: 15),
        child: Center(
          child: Row(children: <Widget>[
            iconfugong(color: hex('#e80202')),
            Padding(
              padding: const EdgeInsets.only(left: 5, top: 2),
              child: Text(
                '复工',
                style: TextStyle(color: hex('#e80202')),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('停工详情'),
        // actions: <Widget>[returnWork()],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            pauseWork['status'] != 0 ? basePart(pauseWork) : Container(),
            Column(
              children: returnWorkList.map((value) {
                return basePart(value);
              }).toList(),
            ),
          ],
        ),
      ),
      backgroundColor: hex('#f4f4f4'),
    );
  }
}
