import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class ChooseWorker extends StatefulWidget {
  ChooseWorker({
    Key key,
    @required this.args,
  }) : super(key: key);

  final Map args;
  @override
  _ChooseWorkerState createState() => _ChooseWorkerState();
}

class _ChooseWorkerState extends State<ChooseWorker> {
  String name = '';
  List chooseNames = [];
  bool isShowBotton = false;
  Map person;

  @override
  void initState() {
    super.initState();
    getWorkers(name);
  }

  getWorkers(String filter) {
    // if (filter.length > 0) {
    Request.getChargepersonList(widget.args['id'], filter).then((res) {
      setState(() {
        res.forEach((item) {
          item['isChoose'] = false;
        });
        chooseNames = res;
        isShowBotton = res.length > 0 ? true : false;
      });
    });
    // } else {
    //   setState(() {
    //     chooseNames.clear();
    //     isShowBotton = false;
    //   });
    // }
  }

  Widget workerItem(Map item, int index) {
    return Padding(
      padding: EdgeInsets.only(bottom: 35),
      child: Row(
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () {
                chooseWorker(index, item);
              },
              child: Row(
                children: <Widget>[
                  chooseIcon(isChoose: item['isChoose']),
                  Padding(
                    padding: EdgeInsets.only(left: 9),
                    child: Text(
                      '${item["name"]}/${item["phone"]}',
                      style: TextStyle(
                        fontSize: 16,
                        color: item['isChoose'] ? hex("##13BB87") : hex('#111'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              G.callPhone(int.parse(item['phone']));
            },
            child: icondianhua(
              size: 16,
              color: item['isChoose'] ? hex("##13BB87") : hex("#666"),
            ),
          ),
        ],
      ),
    );
  }

  // 选择工人
  chooseWorker(int index, Map item) {
    setState(() {
      chooseNames.forEach((f) {
        f['isChoose'] = false;
      });
      chooseNames[index]['isChoose'] = true;
      person = {
        'id': item['id'],
        'name': item['name'],
        'phone': item['phone'],
      };
    });
  }

  Widget chooseIcon({bool isChoose}) {
    return Container(
      width: 16,
      height: 16,
      child: isChoose
          ? Icon(
              Icons.check,
              size: 12,
              color: hex("#fff"),
            )
          : Container(),
      decoration: BoxDecoration(
        color: isChoose ? hex("#13BB87") : hex("#fff"),
        borderRadius: BorderRadius.circular(16),
        border: Border.all(
          color: !isChoose ? hex("#666") : hex("#fff"),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: XdsAppBar(
          title: Text('选择负责人'),
        ),
        body: Stack(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    height: 39.0,
                    padding: EdgeInsets.symmetric(horizontal: 12),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(right: 8),
                          child: iconsousuo(),
                        ),
                        Expanded(
                          child: TextField(
                            onChanged: (name) {
                              getWorkers(name);
                            },
                            decoration: InputDecoration(
                              hintText: '负责人名字',
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                            ),
                          ),
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: hex("#999"),
                          width: 0.5,
                        ),
                        borderRadius: BorderRadius.circular(4)),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 35,
                        bottom: isShowBotton ? 84 : 0,
                      ),
                      child: ListView.builder(
                        itemCount: chooseNames.length,
                        itemBuilder: (c, i) {
                          return workerItem(chooseNames[i], i);
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
            isShowBotton
                ? XdsBottomButton(
                    onCancel: () {
                      G.pop();
                    },
                    onConfirm: () {
                      G.pop(person);
                    },
                  )
                : Container(),
          ],
        ));
  }
}
