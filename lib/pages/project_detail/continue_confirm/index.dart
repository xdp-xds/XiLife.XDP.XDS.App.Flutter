import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';
import './date_range_picker.dart' as DateRagePicker;
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/utils/parse_date.dart';
//import 'package:date_range_picker/date_range_picker.dart' as DateRagePicker;

class ContinueConfirm extends StatefulWidget {
  final Map args;
  ContinueConfirm({Key key, this.args}) : super(key: key);
  @override
  _ContinueConfirmState createState() => _ContinueConfirmState();
}

class _ContinueConfirmState extends State<ContinueConfirm> {
  /// 确认弹窗
  Confirm confirm;

  /// loading
  Loading loading;

  /// alert
  Alert alert;

  TextEditingController _remarkController;

  String remark;

  String planId;
  List processList = [];
  int startYear = DateTime.now().year - 1;
  int endYear = DateTime.now().year + 2;

  /// 返回页面是否弹窗
  bool isShowConfirm = false;

  @override
  void initState() {
    super.initState();

    /// 初始化弹窗 & loading & alert
    confirm = XdsDialog.confirm(context);
    loading = XdsDialog.loading(context);
    alert = XdsDialog.alert(context);
    _remarkController = TextEditingController(text: remark);
    planId = widget.args['planId'];
    getSuspendedProcess();
  }

  getSuspendedProcess() {
    Request.getSuspendedProcess(planId).then((res) {
      setState(() {
        String now = nowDate().split(" ")[0];
        String date = "$now";
        List<dynamic> data = res["data"];
        data.map((item) {
          item["startDate"] = date;
          processList.add(item);
        }).toList();
        print(processList);
      });
    });
  }

  Widget title(name) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.star,
          size: 16,
          color: hex("#111"),
        ),
        Text(
          "$name",
          style: TextStyle(color: hex("#111"), fontSize: 15),
        )
      ],
    );
  }

  validate() {
    bool isValidate = false;
    List newProcessList = processList.map((process) {
      if (process["startDate"] != "" &&
          process["endDate"] != "" &&
          process["startDate"] != null &&
          process["endDate"] != null) {
        if (process["validate"] != null) {
          isValidate = true;
        }
      } else {
        process["validate"] = true;
        isValidate = true;
      }
      return process;
    }).toList();
    setState(() {
      processList = newProcessList;
    });
    // for (int i = 0; i < processList.length; i++) {
    //   if (processList[i]["validate"] == true) {
    //     isValidate = true;
    //     break;
    //   }
    // }
    if (isValidate) {
      return alert.error(title: '', desc: "请选择今天及以后的复工时间段");
    }
    return true;
  }

  List<Widget> getProcessList() {
    List<Widget> processListWidget = [];
    processList.forEach((item) {
      processListWidget.add(process(item));
    });
    return processListWidget;
  }

  Widget process(item) {
    String start = item["startDate"];
    String end = "";
    String endString = nowDate().split(" ")[0];
    if (item["endDate"] != null && item["endDate"] != "") {
      end = item["endDate"];
      endString = item["endDate"];
    }
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: Column(
        children: <Widget>[
          title(item["processName"]),
          Container(
            margin: EdgeInsets.only(top: 25),
            child: Row(
              children: <Widget>[
                Text(
                  "选择时间:",
                  style: TextStyle(color: hex("#333")),
                ),
                Expanded(
                  child: InkWell(
                    onTap: () async {
                      final List<DateTime> picked =
                          await DateRagePicker.showDatePicker(
                              context: context,
                              initialFirstDate:
                                  DateTime.parse(start.replaceAll("/", "-")),
                              initialLastDate: DateTime.parse(
                                  endString.replaceAll("/", "-")),
                              firstDate: new DateTime(startYear),
                              lastDate: new DateTime(endYear),
                              locale: const Locale('zh', 'CH'));
                      if (picked != null && picked.length == 2) {
                        isShowConfirm = true;
                        setState(() {
                          processList = processList.map((process) {
                            if (item["processId"] == process["processId"]) {
                              String start = picked[0].toString();
                              String startDate = start.split(" ")[0];
                              String end = picked[1].toString();
                              String endDate = end.split(" ")[0];
                              var difference =
                                  picked[0].difference(new DateTime.now());
                              if (difference.inDays < 0) {
                                print("34234");
                                process["validate"] = true;
                              } else {
                                process["validate"] = null;
                              }
                              process["startDate"] =
                                  startDate.replaceAll("-", "/");
                              process["endDate"] = endDate.replaceAll("-", "/");
                            }
                            return process;
                          }).toList();
                        });
                      }
                    },
                    child: Container(
                        height: 44,
                        margin: EdgeInsets.only(left: 10),
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        decoration: BoxDecoration(
                            border: Border.all(color: hex('#979797')),
                            borderRadius: BorderRadius.circular(3)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('$start - $end'),
                            iconrili(color: hex('#13bb87'))
                          ],
                        )),
                  ),
                )
              ],
            ),
          ),
          item["validate"] != null
              ? Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Row(
                    children: <Widget>[
                      Opacity(opacity: 0, child: Text('选择时间：')),
                      Text(
                        '* 请选择今天及以后的复工时间段',
                        style: TextStyle(color: hex('#F61E1E'), fontSize: 13),
                      ),
                    ],
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: XdsAppBar(
          title: Text("复工确认"),
          leadingOnPressed: () {
            isShowConfirm
                ? confirm.show(
                    title: '取消将不保存当前信息',
                    subtitle: '取消将不保存当前信息',
                    onOk: () {
                      G.pop();
                    },
                  )
                : G.pop();
          },
        ),
        body: SafeArea(
            child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.fromLTRB(15, 0, 15, 100),
                constraints: BoxConstraints(minHeight: G.screenHeight - 88),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 30,
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      color: hex("#E7F8F3"),
                      child: Row(
                        children: <Widget>[
                          Text(
                            "* 请为复工工序重新选择计划时间",
                            style:
                                TextStyle(color: hex("#0fa073"), fontSize: 13),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: getProcessList(),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 25),
                      child: Row(
                        children: <Widget>[
                          Text('复工说明：'),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                  border: Border.all(color: hex('#979797')),
                                  borderRadius: BorderRadius.circular(3)),
                              child: TextField(
                                controller: _remarkController,
                                keyboardType: TextInputType.multiline,
                                decoration: InputDecoration(
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    contentPadding: EdgeInsets.all(10),
                                    hintText: '请填写备注',
                                    counterText: ''),
                                maxLength: 100,
                                maxLines: 3,
                                onChanged: (String val) {
                                  isShowConfirm = true;
                                  remark = val;
                                },
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            XdsBottomButton(
              showBorder: true,
              onCancel: () {
                isShowConfirm
                    ? confirm.show(
                        title: '取消将不保存当前信息',
                        subtitle: '取消将不保存当前信息',
                        onOk: () {
                          G.pop();
                        },
                      )
                    : G.pop();
              },
              onConfirm: () {
                if (validate() != true) {
                  return;
                }

                Map data = {"remark": remark, "processes": processList};
                confirm.show(
                    title: '是否确认要复工该项目？',
                    subtitle: '确认之后不可撤回',
                    onOk: () async {
                      try {
                        Request.reActiveProcess(planId, data).then((res) {
                          alert.success(
                              title: "复工成功",
                              desc: '',
                              onClose: () {
                                G.pop();
                                // G.pushNamed('/project_pause_detail',
                                //     arguments: {"planId": planId});
                              });
                        });
                      } catch (e) {
                        alert.error(
                          title: '',
                          desc: '停工操作失败，请稍后再试',
                          // onClose: () {
                          //   G.pushNamed('/project_pause_detail', arguments: {
                          //     "planId": planId
                          //   });
                          // }
                        );
                      }
                    });
              },
            )
          ],
        )));
  }
}
