import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/utils/global.dart';

class DelayDetail extends StatefulWidget {
  DelayDetail({Key key, this.args}) : super(key: key);

  final Map args;

  @override
  _DelayDetailState createState() => _DelayDetailState();
}

class _DelayDetailState extends State<DelayDetail> {
  String planId;

  Map data;

  @override
  void initState() {
    super.initState();

    planId = widget.args['planId'];
    // planId = '312e0067-dbe3-4956-a0c8-c1ebcba7b2e2';

    Future.delayed(Duration.zero, () {
      getDelay();
    });
  }

  /// 1：未开始
  /// 2：进行中
  /// 4：已完成
  status(int status) {
    switch (status) {
      case 1:
        {
          return {
            'colors': [hex('#F69C69'), hex('#FA6510')],
            'text': '未开始'
          };
        }
      case 2:
        {
          return {
            'colors': [hex('#F69C69'), hex('#FA6510')],
            'text': '进行中'
          };
        }
      case 4:
        {
          return {
            'colors': [hex('#ADACBA'), hex('#808080')],
            'text': '已完成'
          };
        }
    }
  }

  getDelay() async {
    try {
      var res = await Request.getDelay(planId);
      setState(() {
        data = res;
      });
    } catch (e) {
      print(e);
    }
  }

  List<Widget> handle() {
    if (data == null) return [Container()];
    List delays = data['delays'];

    List<Widget> widgets = [];

    delays.forEach((val) {
      num status = val['status'];
      Map statusData = this.status(status);
      widgets.add(buildCard(
          colors: statusData['colors'], data: val, text: statusData['text']));
    });

    return widgets;
  }

  goToProcessDetail(data) {
    String processId = data["processId"];
    String processName = data["process"]["name"];
    String planDetailId = data["id"];

    G.pushNamed('/progress_detail', arguments: {
      "processId": processId,
      "param": {
        "planId": planId,
        "planDetailId": planDetailId,
        "processName": processName
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('延期详情'),
      ),
      body: Container(
        color: hex('#f4f4f4'),
        child: ListView(children: handle()),
      ),
    );
  }

  Container buildCard(
      {

      /// 背景渐变
      List<Color> colors,
      Map data,
      String text}) {
    String actualStartDate = G.handleDate(data["actualStartDate"]);
    String actualEndDate = G.handleDate(data["actualEndDate"]);
    String endDate = G.handleDate(data["endDate"]);
    String startDate = G.handleDate(data["startDate"]);
    return Container(
      margin: EdgeInsets.only(top: 20),
      padding: EdgeInsets.symmetric(vertical: 20),
      color: hex('#fff'),
      child: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(bottom: 20, left: 15, right: 15),
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: hex('#e4e4e4')))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 15,
                      height: 15,
                      margin: EdgeInsets.only(right: 7),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        gradient: new LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: colors),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        goToProcessDetail(data);
                      },
                      child: Text(
                        '${data['process']['name']}',
                        style: TextStyle(color: colors[1]),
                      ),
                    )
                  ],
                ),
                Container(
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 7),
                    decoration: BoxDecoration(
                        color: colors[1],
                        borderRadius: BorderRadius.circular(3)),
                    child: Row(
                      children: <Widget>[
                        Text(
                          '$text | ',
                          style: TextStyle(color: hex('#fff'), fontSize: 13),
                        ),
                        Text(
                          '延期${data['delayDays']}天',
                          style: TextStyle(color: hex('#fff'), fontSize: 13),
                        ),
                      ],
                    ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 20),
                  child: Row(
                    children: <Widget>[
                      Text('计划：'),
                      Text('$startDate - $endDate')
                    ],
                  ),
                ),
                Container(
                  margin:
                      EdgeInsets.only(top: actualStartDate.isEmpty ? 0 : 20),
                  child: actualStartDate.isEmpty
                      ? null
                      : Row(
                          children: <Widget>[
                            Text('实际：'),
                            Container(
                                child:
                                    Text('$actualStartDate - $actualEndDate'))
                          ],
                        ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
