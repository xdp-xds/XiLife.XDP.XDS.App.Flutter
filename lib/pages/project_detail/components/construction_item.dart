// import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:color_dart/color_dart.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/parse_date.dart';
import 'package:xdsapp/utils/global.dart';

class ConstructionItem extends StatefulWidget {
  final process;
  final Function(Map item) onTapProcessItem;
  ConstructionItem({this.process, this.onTapProcessItem});
  @override
  _ConstructionItemState createState() => _ConstructionItemState();
}

class _ConstructionItemState extends State<ConstructionItem> {
  String statusColor = '#111';

  /// 返回工序状态文字
  handleConstructionStatus() {
    final status = widget.process['status'];
    switch (status) {
      case PlanDetailsStatus.Unstart:
        {
          return '未开始';
        }
        break;
      case PlanDetailsStatus.Doing:
        {
          return '进行中';
        }
        break;

      case PlanDetailsStatus.Suspend:
        {
          return '已暂停';
        }
        break;
      case PlanDetailsStatus.Done:
        {
          return '已完成';
        }
        break;
      default:
        {
          return "";
        }
    }
  }

  //判断开始和结束时间是否在
  dateToString(start, end) {
    String startDate = dealtimeSlash(start);
    String endDate = dealtimeSlash(end);
    if (startDate == "" && endDate == "") {
      return "";
    } else {
      return "$startDate - $endDate";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          DecoratedBox(
              decoration: BoxDecoration(
                  border: new Border(
                      top: BorderSide(color: hex("#D8D8D8"), width: 0.5))),
              child: Container()),
          title(),
          constructionItemLine(widget.process['delayDays']),
        ],
      ),
    );
  }

  ///标题组件
  Padding title() {
    TextStyle contentStyle = TextStyle(
      color: hex("#111111"),
      fontSize: 15.0,
    );
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.star,
                size: 16,
              ),
              Container(
                margin: EdgeInsets.only(left: 5),
                child: InkWell(
                  onTap: () {
                    widget.onTapProcessItem(widget.process);
                  },
                  child: Text(
                    "${widget.process['process']['name']}",
                    style: new TextStyle(
                      color: hex(statusColor),
                      fontSize: 15.0,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 2, left: 5),
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 15,
                  color: hex('#d8d8d8'),
                ),
              ),
            ],
          ),
          Text(
            '${this.handleConstructionStatus()}',
            style: contentStyle,
          )
        ],
      ),
    );
  }

  /// 工序时间的Widget
  Widget constructionItemLineContent(text, style) {
    return Text(
      '$text',
      style: style,
    );
  }

  ///工序的内容
  constructionItemLine(delayDays) {
    // final isEdit = false;

    TextStyle labelStyle = TextStyle(
      color: hex("#333333"),
      fontSize: 15.0,
    );
    TextStyle contentStyle = TextStyle(
      color: hex("#111111"),
      fontSize: 15.0,
    );
    String actualDate = dateToString(
        widget.process['actualStartDate'], widget.process['actualEndDate']);
    String actual = '$actualDate';
    Widget actualWidget;
    actualWidget = constructionItemLineContent(actual, contentStyle);

    ///工序的行
    Padding rowItem(title, TextStyle labelStyle, Widget rightContent) {
      return Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
        child: Row(
          children: <Widget>[
            Text(
              title,
              style: labelStyle,
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                child: rightContent,
              ),
            )
          ],
        ),
      );
    }

    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 0, 10, 5),
      child: Column(
        children: <Widget>[
          rowItem(
              '计划:',
              labelStyle,
              Text(
                '${dealtimeSlash(widget.process['startDate'])} - ${dealtimeSlash(widget.process['endDate'])}',
                style: contentStyle,
              )),
          actualDate != ""
              ? rowItem('实际:', labelStyle, actualWidget)
              : Container(),
        ],
      ),
    );
  }
}
