import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/constant/index.dart';

class Abnormal extends StatefulWidget {
  final Map exceptions;
  Abnormal({Key key, this.planBasicInfo, this.exceptions}) : super(key: key);

  final Map planBasicInfo;

  @override
  _AbnormalState createState() => _AbnormalState();
}

class _AbnormalState extends State<Abnormal> {
  String planId;

  Map data;

  double opacity = 0;

  @override
  void initState() {
    super.initState();
    planId = widget.planBasicInfo['planId'];

    Future.delayed(Duration.zero, () async {
      getDelay();
    });
  }

  @override
  void deactivate() {
    super.deactivate();

    if (ModalRoute.of(context).isCurrent) {
      getDelay();
    }
  }

  getDelay() async {
    try {
      var res = await Request.getDelay(planId);
      setState(() {
        data = res;
        opacity = 1;
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hex('#f4f4f4'),
      child: Column(
        children: <Widget>[
          Container(
            child: Opacity(
              opacity: opacity,
              child: (data == null || data['total'] <= 0) &&
                      widget.exceptions["countOfFrozen"] <= 0
                  ? Container(
                      margin: EdgeInsets.only(top: 76),
                      child: Image.asset(
                        'lib/assets/images/not_abnormal.png',
                        height: 195,
                        fit: BoxFit.contain,
                      ),
                    )
                  : Column(
                      children: <Widget>[
                        widget.exceptions["countOfFrozen"] > 0
                            ? buildRow(
                                "停工",
                                widget.exceptions["countOfFrozen"],
                                widget.exceptions["countOfFrozen"],
                                "stop",
                              )
                            : Container(),
                        data != null && data['total'] > 0
                            ? buildRow(
                                "延期", data["total"], data['current'], "delay")
                            : Container(),
                      ],
                    ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildRow(text, count, current, type) {
    Color textColor = hex('111');
    Color bgColor = hex('#f4f4f4');

    Widget icon;
    String route;
    Map arguments = {"planId": planId};
    //行的类型
    if (type == "stop") {
      if (widget.exceptions["exceptionsOfFrozen"]["suspendingException"]
              ["status"] !=
          SuspendStatus.all) {
        textColor = hex('#E80202');
        bgColor = hex('#FDBFBF');
      }
      arguments = {"planId": planId, "exceptions": widget.exceptions};
      icon = iconzantingcopy(size: 16, color: textColor);
      route = "/project_pause_detail";
    }
    if (type == "delay") {
      if (current != 0) {
        textColor = hex('#FA6510');
        bgColor = hex('#FDDCBF');
      }
      icon = iconyanqi(size: 16, color: textColor);
      route = "/delay_detail";
    }
    return Container(
      child: InkWell(
        onTap: () => G.pushNamed(route, arguments: arguments),
        child: Container(
          margin: EdgeInsets.only(top: 25),
          color: hex('#fff'),
          height: 63,
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(right: 15),
                      width: 33,
                      height: 33,
                      color: bgColor,
                      child: icon),
                  Text(
                    '$text（$count）',
                    style: TextStyle(color: textColor, fontSize: 15),
                  )
                ],
              ),
              Transform.scale(
                  child: Icon(
                    Icons.arrow_forward_ios,
                    size: 8,
                  ),
                  scale: 2)
            ],
          ),
        ),
      ),
    );
  }
}
