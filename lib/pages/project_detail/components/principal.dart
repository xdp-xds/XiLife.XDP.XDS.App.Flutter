import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/common_ui/xds_leader_card/card_type.dart';
import 'package:xdsapp/common_ui/xds_leader_card/index.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class Principal extends StatefulWidget {
  Principal({
    Key key,
    this.planId,
  }) : super(key: key);

  final String planId;

  @override
  _PrincipalState createState() => _PrincipalState();
}

class _PrincipalState extends State<Principal> {
  /// loading
  Loading loading;

  bool collapse = false;

  /// ```
  /// [
  ///     /// 所有工站
  ///     workStations: [XdsLeaderCardData]
  ///     /// 所有工序
  ///     workProcess: {
  ///       workStationId: [XdsLeaderCardData],
  ///       workStationId: [XdsLeaderCardData],
  ///     }
  /// ]
  /// ```
  Map data = {};

  bool isEdit = false;

  @override
  void initState() {
    loading = XdsDialog.loading(context);
    Future.delayed(Duration.zero, () async {
      await getData();
    });

    super.initState();
  }

  /// 获取数据
  getData() async {
    loading.show();
    try {
      var res = await Request.getPrinciapl(widget.planId);
      List result = res['data'];
      // String s = json.encode(res['data']);
      // print(s);

      List<XdsLeaderCardData> workStations = [];
      Map<String, List<XdsLeaderCardData>> workProcessJson = {};

      result.forEach((val) {
        Map workStation = val['workStation'];
        String workStationId = workStation['id'];

        List workProcess = val['processes'];

        List<XdsLeaderCardData> procesListData = [];

        /// 工序
        workProcess.forEach((procesData) {
          procesListData.add(XdsLeaderCardData.fromJson({
            "workStation": {
              "id": procesData['process']['id'],
              "name": procesData['process']['name']
            },
            "workProcedure": null,
            "leader": {
              "defaultValue": procesData['planChargePerson'],
            },
            "remark": procesData['remark'],
            "verifyLeader": true,
            "verifyRemark": false,
            "validation": false,
            "isEdit": isEdit,
            "collapse": false
          }));
        });

        workProcessJson['$workStationId'] = procesListData;

        /// 工站
        workStations.add(XdsLeaderCardData.fromJson({
          "workStation": {"id": workStationId, "name": workStation['name']},
          "workProcedure": null,
          "leader": {
            "defaultValue": val['userId'] == null
                ? null
                : {
                    "id": val['userId'],
                    "name": val['chargePerson'],
                    "phone": val['phoneNo']
                  },
          },
          "remark": val['remark'],
          "verifyLeader": true,
          "verifyRemark": false,
          "validation": false,
          "isEdit": isEdit,
          "collapse": false
        }));
      });

      Map jsonData = {
        "workStations": workStations,
        "workProcess": workProcessJson,
      };

      // print(json.encode(jsonData));

      setState(() {
        data = jsonData;
      });

      loading.hide();
    } catch (e) {
      print(e);
      loading.hide();
    }
  }

  /// 展开收起组件
  List<Widget> buildCollapse() {
    List<XdsLeaderCardData> workStations = data['workStations'];
    Map<String, List<XdsLeaderCardData>> workProcess = data['workProcess'];
    // int workStationsLen = workStations.length;

    List<Widget> collapses = [];

    workStations.asMap().forEach((index, val) {
      List<XdsLeaderCardData> processData = workProcess[val.workStation.id];

      XdsLeaderCardData lastPrcess = processData.last;

      DefaultValue leaderDefalut = val.leader.defaultValue;

      /// 工站负责人
      String text;

      if (leaderDefalut == null || leaderDefalut.id == '0') {
        text = '';
      } else {
        text = '${leaderDefalut.name} / ${leaderDefalut.phone}';
      }

      collapses.add(Container(
        color: hex('#fff'),
        margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.only(top: 10),
        child: XdsCollapse(
          value: val.collapse,
          title: Container(
            height: 50,
            alignment: Alignment.center,
            child: buildTitle(
                data: val,
                fontWeight: val.collapse ? FontWeight.bold : FontWeight.normal,
                type: CardType.workStation),
          ),
          body: Container(
            child: Column(
              children: [
                Container(
                    padding: EdgeInsets.symmetric(vertical: 14, horizontal: 15),
                    decoration: BoxDecoration(
                      color: hex('#f4f4f4'),
                      borderRadius: BorderRadius.circular(3),
                    ),
                    child: Column(
                      children: <Widget>[
                        buildWorkStationLeader(text, leaderDefalut),
                        Container(
                          margin: EdgeInsets.only(
                              top: val.remark == null || val.remark.isEmpty
                                  ? 0
                                  : 10),
                          child: val.remark == null || val.remark.isEmpty
                              ? null
                              : Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Text(
                                        '备注：${val.remark}',
                                        style: TextStyle(
                                            color: hex('#333'),
                                            fontSize: 15,
                                            height: 1.75),
                                      ),
                                    ),
                                  ],
                                ),
                        ),
                      ],
                    )),

                /// 分割线
                Container(
                  margin: EdgeInsets.only(top: 20),
                  height: 1,
                  color: hex('#d8d8d8'),
                ),

                Column(
                  children: workProcess[val.workStation.id]
                      .map((XdsLeaderCardData val) {
                    return buildWorkProcedure(val,
                        bottomBorder:
                            lastPrcess.workStation.id == val.workStation.id
                                ? false
                                : true);
                  }).toList(),
                )
              ],
            ),
          ),
          onChange: (bool collapseVal) {
            val.collapse = collapseVal;
            setState(() {
              data['workStations'][index] = val;
            });
          },
        ),
      ));
    });

    return collapses;
  }

  /// 工站负责人
  buildWorkStationLeader(String text, DefaultValue leaderDefalut) {
    return Row(
      children: <Widget>[
        Text(
          '负责人：',
          style: TextStyle(color: hex('#333'), fontSize: 15),
        ),
        text.isEmpty
            ? Container()
            : Text(
                '$text',
                style: TextStyle(color: hex('#111'), fontSize: 15),
              ),
        text.isEmpty
            ? Container()
            : GestureDetector(
                child: Container(
                    margin: const EdgeInsets.only(left: 15, bottom: 4),
                    child: icondianhua(color: hex('#13BB87'), size: 18)),
                onTap: () async {
                  if (leaderDefalut.phone == null) return;
                  G.callPhone(num.parse(leaderDefalut.phone));
                },
              )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hex('#f4f4f4'),
      child: ListView(
        children: data.isEmpty ? [] : buildCollapse(),
      ),
    );
  }

  /// 工序
  Container buildWorkProcedure(
    XdsLeaderCardData data, {
    bool bottomBorder = false,
  }) {
    return Container(
      margin: EdgeInsets.only(top: 20),
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                buildTitle(data: data, type: CardType.workProcedure),
                Container(
                  margin: EdgeInsets.only(top: 1,left: 5),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    size: 15,
                    color: hex('#d8d8d8'),
                  ),
                ),
              ],
            ),
            onTap: () {
              G.pushNamed('/progress_detail', arguments: {
                "processId": data.workStation.id,
                "param": {
                  "planId": widget.planId,
                  "planDetailId": null,
                  "processName": data.workStation.name,
                  "userId": data.leader.defaultValue?.id
                }
              });
            },
          ),
          XdsLeaderCard(
            // onPressTitle: (XdsLeaderCardData xdsLeaderCardData, json) {
            //   print(xdsLeaderCardData);
              // G.pushNamed('/progress_detail', arguments: {
              //   "processId": processId,
              //   "param": {
              //     "planId": id,
              //     "planDetailId": planDetailId,
              //     "processName": processName,
              //     "userId": userId
              //   }
              // });
            // },
            data: data,
            isEdit: false,
            bottomBorder: bottomBorder,
            type: CardType.workProcedure,
            onLeaderPress: (xdsLeaderCardData, json) {},
            onChange: (xdsLeaderCardData, json) {},
          ),
        ],
      ),
    );
  }
}
