import 'package:flutter/material.dart';
import 'package:color_dart/HexColor.dart';
// import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';
import 'package:xdsapp/pages/project_detail/components/collapse_title.dart';
import 'package:xdsapp/pages/project_detail/components/construction_item.dart';
import 'package:xdsapp/utils/parse_date.dart';
// import 'package:xdsapp/constant/index.dart';

class ConstructionProcess extends StatelessWidget {
  final constructionSchedule;
  final planBasicInfo;
  final Function goToProcessDetail;
  ConstructionProcess(
      {Key key,
      this.constructionSchedule,
      this.planBasicInfo,
      this.goToProcessDetail})
      : super(key: key);
  handleStage() {
    List<Widget> stageList = [];
    this.constructionSchedule.forEach((item) {
      stageList.add(new ConstructionStage(
          stage: item,
          index: this.constructionSchedule.indexOf(item),
          onTapProcessItem: onTapProcessItem));
    });
    return stageList;
  }

  onTapProcessItem(processItem) {
    String processId = processItem["process"]["id"];
    String processName = processItem["process"]["name"];
    String planDetailId = processItem["id"];
    goToProcessDetail(processId, processName, planDetailId);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: hex("#F4F4F4"),
        child: ListView(
          padding: EdgeInsets.all(0),
          children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                  height: 25,
                  color: hex("#F4F4F4"),
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  // child: InkWell(
                  //   onTap: () {},
                  //   //已完成不显示编辑按钮
                  //   child: planBasicInfo['status'] != PlanStatusEnum.done
                  //       ? Row(
                  //           mainAxisAlignment: MainAxisAlignment.end,
                  //           children: <Widget>[
                  //             Container(
                  //               margin: EdgeInsets.only(right: 5),
                  //               child:
                  //                   iconxiugai(color: hex('#13BB87'), size: 15),
                  //             ),
                  //             Text(
                  //               '修改进度',
                  //               style: new TextStyle(
                  //                   color: hex('#13BB87'), fontSize: 15),
                  //             )
                  //           ],
                  //         )
                  //       : Container(),
                  // ),
                ),
                Column(
                  children: this.handleStage(),
                ),
              ],
            )
          ],
        ));
  }
}

class ConstructionStage extends StatefulWidget {
  final Map stage;
  final int index;
  final Function(Map item) onTapProcessItem;
  ConstructionStage({this.stage, this.index, this.onTapProcessItem});
  @override
  _ConstructionStageState createState() => _ConstructionStageState();
}

class _ConstructionStageState extends State<ConstructionStage> {
  bool isCollapse = false;
  int exceptions = 0;
  @override
  void initState() {
    super.initState();
  }

//循环生成工序
  handleProcess() {
    List<Widget> construction = [];
    widget.stage['planDetails'].forEach((item) {
      construction.add(new ConstructionItem(
        process: item,
        onTapProcessItem: widget.onTapProcessItem,
      ));
    });
    return construction;
  }

  //展开收起
  collapse(bool value) {
    this.setState(() {
      isCollapse = value;
    });
  }

  ///判断开始和结束时间是否在
  dateToString(start, end) {
    String startDate = dealtimeSlash(start);
    String endDate = dealtimeSlash(end);
    if (startDate == "" && endDate == "") {
      return "";
    } else {
      return "$startDate - $endDate";
    }
  }

  @override
  Widget build(BuildContext context) {
    String actualDate = dateToString(
        widget.stage['actualStartDate'], widget.stage['actualEndDate']);
    return Container(
        color: hex('#fff'),
        margin: EdgeInsets.only(bottom: 20),
        child: new XdsCollapse(
          padding: EdgeInsets.only(top: 20, left: 15, right: 15, bottom: 10),
          title: CollapseTitle(
            title: widget.stage['name'],
            iconIndex: widget.index,
            fontWeight: isCollapse ? FontWeight.bold : FontWeight.normal,
          ),
          value: isCollapse,
          onChange: this.collapse,
          body: Container(
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 5),
                  // height: 80,
                  color: hex('#F4F4F4'),
                  padding: EdgeInsets.symmetric(vertical: 14, horizontal: 20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(right: 5),
                              child: Text(
                                  '计划: ${dateToString(widget.stage['startDate'], widget.stage['endDate'])}'),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: actualDate != ""
                            ? Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.fromLTRB(0, 10, 5, 0),
                                    child: Text('实际: $actualDate'),
                                  ),
                                ],
                              )
                            : Container(),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  decoration: BoxDecoration(
                      border: Border(
                          bottom:
                              BorderSide(color: hex("#D8D8D8"), width: 0.5))),
                ),
                Column(
                  children: this.handleProcess(),
                )
              ],
            ),
          ),
        ));
  }
}
