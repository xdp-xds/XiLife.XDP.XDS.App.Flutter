import 'package:flutter/material.dart';
import 'package:color_dart/HexColor.dart';
import 'package:xdsapp/utils/iconfont.dart';

class CollapseTitle extends StatelessWidget {
  final String title;
  final Widget rightException;
  final Widget icon;
  final int iconIndex;
  final FontWeight fontWeight;
  CollapseTitle({
    this.title, 
    this.rightException, 
    this.icon, 
    this.iconIndex, 
    this.fontWeight = FontWeight.normal
  });

  Widget returnIcon() {
    switch (iconIndex) {
      case 0:
        return iconjizhuangshigongzhunbei(color: hex('#13BB87'), size: 21);
        break;
      case 1:
        return iconjizhuangshigong(color: hex('#13BB87'), size: 21);
        break;
      case 2:
        return Container(
          margin: EdgeInsets.only(right: 5),
          child: iconmiancengshigong(color: hex('#13BB87'), size: 15),
        );
        break;
      case 3:
        return iconanzhuang(color: hex('#13BB87'), size: 15);
        break;
      default:
        return iconanzhuang(color: hex('#13BB87'), size: 15);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        // padding: EdgeInsets.only(top: 20),
        child: Row(
          children: <Widget>[
            Container(
              width: 33,
              height: 33,
              color: hex('#BFFDEA'),
              margin: EdgeInsets.only(right: 10),
              alignment: Alignment.center,
              child: iconIndex != null ? returnIcon() : icon,
            ),
            Text('$title', style: TextStyle(
              fontWeight: fontWeight
            ),),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: rightException,
            )
          ],
        ),
      ),
    );
  }
}
