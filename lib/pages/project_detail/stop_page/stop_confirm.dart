import 'package:color_dart/HexColor.dart';
import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class StopConfirm extends StatefulWidget {
  StopConfirm({Key key, this.args}) : super(key: key);

  final Map args;

  @override
  _StopConfirmState createState() => _StopConfirmState();
}

class _StopConfirmState extends State<StopConfirm> {
  TextEditingController _remarkController;

  /// 备注
  String remark;

  /// 停工可选择最小时间
  DateTime minTime = DateTime.parse('${DateTime.now()}');

  /// 停工可选择最大时间
  DateTime maxTime = DateTime.parse('2050-12-31');

  /// 停工时间
  DateTime stopTime;

  String planId;
  Map data;

  /// 确认弹窗
  Confirm confirm;

  /// loading
  Loading loading;

  /// alert
  Alert alert;

  /// 返回页面是否弹窗
  bool isShowConfirm = false;

  @override
  void initState() {
    super.initState();

    /// 初始化弹窗 & loading & alert
    confirm = XdsDialog.confirm(context);
    loading = XdsDialog.loading(context);
    alert = XdsDialog.alert(context);
    planId = widget.args['planId'];

    /// 默认情况  停工时间 = 停工最小时间
    stopTime = minTime;

    Future.delayed(Duration.zero, () async {
      loading.show();
      try {
        var res = await Request.getProjectDetail(planId);
        setState(() {
          data = res['data'];
        });
        loading.hide();
      } catch (e) {
        loading.hide();
      }
    });

    _remarkController = TextEditingController(text: remark);
  }

  void _showDatePicker() {
    isShowConfirm = true;
    DatePicker.showDatePicker(context,
        pickerTheme: DateTimePickerTheme(
          showTitle: true,
          confirm: Text(
            '确认',
            style: TextStyle(color: hex('#1989fa')),
          ),
          cancel: Text(
            '取消',
            style: TextStyle(color: hex('#455a64')),
          ),
        ),
        minDateTime: minTime,
        maxDateTime: maxTime,
        initialDateTime: stopTime,
        onCancel: () {}, onConfirm: (DateTime dateTime, List<int> ints) {
      setState(() {
        stopTime = dateTime;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text('停工确认'),
        leadingOnPressed: () {
          isShowConfirm ? 
          confirm.show(
            title: '取消将不保存当前信息',
            subtitle: '取消将不保存当前信息',
            onOk: () {
              G.pop();
            },
          ) : G.pop();
        },
      ),
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              padding:
                  EdgeInsets.only(bottom: 84, left: 16, right: 16, top: 16),
              child: data == null
                  ? null
                  : Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text('项目名称：'),
                            Text('${data['planBasicInfo']['projectName']}')
                          ],
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 25),
                          child: Row(
                            children: <Widget>[
                              Text('停工时间：'),
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    _showDatePicker();
                                  },
                                  child: Container(
                                      height: 44,
                                      padding:
                                          EdgeInsets.symmetric(horizontal: 10),
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: hex('#979797')),
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                              '${stopTime.toIso8601String().split('T')[0].replaceAll('-', '/')}'),
                                          iconrili(color: hex('#13bb87'))
                                        ],
                                      )),
                                ),
                              ),
                            ],
                          ),
                        ),

                        // Container(
                        //   margin: EdgeInsets.only(top: 10),
                        //   child: Row(
                        //     children: <Widget>[
                        //       Opacity(
                        //         opacity: 0,
                        //         child: Text('停工时间：')
                        //       ),
                        //       Text('* 请选择今天及以后的停工时间', style: TextStyle(
                        //         color: hex('#F61E1E')
                        //       ),)
                        //     ],
                        //   ),
                        // ),

                        Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Row(
                            children: <Widget>[
                              Text('停工原因：'),
                            ],
                          ),
                        ),

                        Container(
                          margin: EdgeInsets.only(top: 10),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  decoration: BoxDecoration(
                                      border: Border.all(color: hex('#979797')),
                                      borderRadius: BorderRadius.circular(3)),
                                  child: TextField(
                                    controller: _remarkController,
                                    keyboardType: TextInputType.multiline,
                                    decoration: InputDecoration(
                                        enabledBorder: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        contentPadding: EdgeInsets.all(10),
                                        hintText: '请填写备注',
                                        counterText: ''),
                                    maxLength: 100,
                                    maxLines: 3,
                                    onChanged: (String val) {
                                      isShowConfirm = true;
                                      setState(() {
                                        remark = val;
                                      });
                                    },
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
            ),
            XdsBottomButton(
              showBorder: true,
              onCancel: () {
                isShowConfirm ? 
                confirm.show(
                  title: '取消将不保存当前信息',
                  subtitle: '取消将不保存当前信息',
                  onOk: () {
                    G.pop();
                  },
                ) : G.pop();
              },
              onConfirm: () {
                Map data = {
                  "planId": planId,
                  "suspendDate": stopTime.toIso8601String(),
                  "suspendRemark": remark
                };

                confirm.show(
                    title: '是否确认要停工该项目？',
                    subtitle: '确认之后不可撤回',
                    onOk: () async {
                      try {
                        var res = await Request.getSuspend(data);
                        if(res == null) {
                          return alert.error(
                            title: '',
                              desc: '停工操作失败，请稍后再试',
                              onClose: () {
                                G.pop();
                              //   G.pushNamed('/project_pause_detail', arguments: {
                              //     "planId": planId
                              //   });
                              }
                          );
                        }
                        alert.success(
                          title: res,
                          desc: '',
                          onClose: () {
                            // G.pushNamed('/project_pause_detail',
                            //     arguments: {"planId": planId});
                            G.pop();
                          }
                        );
                      } catch (e) {
                        
                      }
                    });
              },
            ),
          ],
        ),
      ),
    );
  }
}
