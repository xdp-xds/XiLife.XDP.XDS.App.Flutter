import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/pages/project_detail/components/project_base_detail.dart';
import 'package:xdsapp/pages/project_detail/components/construction_process.dart';
import 'package:color_dart/color_dart.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'components/abnormal.dart';
import 'components/principal.dart';

class ProjectDetail extends StatefulWidget {
  final Map args;
  ProjectDetail({Key key, this.args}) : super(key: key);
  @override
  _ProjectDetailState createState() => _ProjectDetailState();
}

class _ProjectDetailState extends State<ProjectDetail>
    with TickerProviderStateMixin {
  TabController _tabController;
  ScrollController _nestedScrollViewController = new ScrollController();
  String title = '项目详情';
  String id = '';
  Map data = {
    "planBasicInfo": {},
    "currentPlanDetails": [],
  };
  List<dynamic> constructionSchedule = [];
  double height = 240;
  // double padding = 0;

  bool susend;

  @override
  void initState() {
    super.initState();
    this.setState(() {
      if (widget.args != null) {
        id = widget.args['id'];
        print(id);
      }
    });
    _tabController = TabController(length: 3, vsync: this);
    this.getProjectDetail(id);
    this.getSchedule(id);
    _nestedScrollViewController.addListener(() {
      // 如果滑动到底部
      if (_nestedScrollViewController.position.pixels >= 200) {
        this.setState(() {
          title = data['planBasicInfo']['projectName'];
          // padding = 58;
        });
      } else {
        this.setState(() {
          title = '项目详情';
          // padding = 0;
        });
      }
    });
  }

  @override
  void deactivate() {
    super.deactivate();

    if (ModalRoute.of(context).isCurrent) {
      this.getProjectDetail(id);
      this.getSchedule(id);
    }
  }

  getProjectDetail(id) {
    height = 240;
    Request.getProjectDetail(id).then((res) {
      print(res);
      if (res['isSuccessStatusCode'] == true) {
        //计算内容行的高度
        int currentPlanDetailsLength = 0;
        res['data']['currentPlanDetails'] != null
            ? currentPlanDetailsLength =
                res['data']['currentPlanDetails'].length
            : currentPlanDetailsLength = 0;
        int titleHeight = 0;
        int processHeight = 30;
        int processContentHeight = 0;

        if (res['data']['currentPlanDetails'] != null) {
          res['data']['currentPlanDetails'].forEach((item) {
            if (item['processName'] != null &&
                item['processName'].length >= 3) {
              int multiple = (item['processName'].length / 3).floor();
              processContentHeight = processHeight * multiple + 10;
            }
            processContentHeight = processContentHeight + processHeight;
          });
        }
        //计算当前的边框
        if (currentPlanDetailsLength > 0) {
          titleHeight = 50;
        }
        setState(() {
          data = res['data'];
          int delayDays = res['data']['planBasicInfo']['delayDays'];
          int frozenDays = res['data']['planBasicInfo']["frozenDays"];

          Map exceptionsOfFrozen =
              res['data']['exceptions']['exceptionsOfFrozen'];
          if (exceptionsOfFrozen['suspendingException'] == null) {
            susend = true;
          } else {
            int suspendStatus =
                exceptionsOfFrozen['suspendingException']['status'];
            int projectStatus = res['data']['planBasicInfo']['status'];

            /// 项目经理显示
            if (G.user.workInfo.type == UserType.projectManager) {
              /// 不显示按钮
              if (suspendStatus == SuspendStatus.unSuspend) susend = null;

              /// 复工
              if (projectStatus == PlanStatusEnum.suspend)
                susend = false;

              /// 停工
              else if (projectStatus == PlanStatusEnum.doing &&
                  suspendStatus != SuspendStatus.unSuspend) susend = true;
            } else {
              susend = null;
            }
          }
          int delayDaysHeight = 0;
          int actualDateHeight = 0;
          //实际时间高度
          if (res['data']['planBasicInfo']['status'] == PlanStatusEnum.done) {
            actualDateHeight = 30;
          }
          //延期的高度 停工或是延期
          if (delayDays > 0 || frozenDays > 0) {
            delayDaysHeight = 47;
          }
          height = height +
              processContentHeight +
              titleHeight +
              delayDaysHeight +
              actualDateHeight +
              40;
        });
      }
    });
  }

  getSchedule(id) {
    Request().getSchedule(id).then((res) {
      if (res['isSuccessStatusCode'] == true) {
        setState(() {
          constructionSchedule = res["data"]["deliveryStages"];
        });
      }
    });
  }

  goToProcessDetail(processId, processName, planDetailId) {
    G.pushNamed('/progress_detail', arguments: {
      "processId": processId,
      "param": {
        "planId": id,
        "planDetailId": planDetailId,
        "processName": processName,
        "userId": ""
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text("$title"),
        actions: <Widget>[
          Container(
              child: G.user.workInfo.status == UserType.projectManager
                  ? titleButton(isSuspend: susend)
                  : null),
        ],
      ),
      body: NestedScrollView(
        physics: NeverScrollableScrollPhysics(),
        controller: _nestedScrollViewController,
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: hex('#fff'),
              leading: Container(),
              centerTitle: true, //标题是否居中, 标题是否居中显示，默认值根据不同的操作系统，显示方式不一样
              primary: false, //是否预留高度
              forceElevated: false,
              automaticallyImplyLeading: true,
              titleSpacing: NavigationToolbar.kMiddleSpacing,
              snap: false, //与floating结合使用
              expandedHeight: height, //展开高度
              floating: false, //是否随着滑动隐藏标题
              pinned: true, //是否固定在顶部
              // title: Text('$title'),
              flexibleSpace: FlexibleSpaceBar(
                  //可以展开区域，通常是一个FlexibleSpaceBar
                  centerTitle: true,
                  // title: Container(
                  //   child: Text("项目详情"),
                  // ),
                  background: Container(
                    child: data['planBasicInfo'].length > 0
                        ? BaseDetail(
                            data: data['planBasicInfo'],
                            currentPlanDetails: data['currentPlanDetails'],
                          )
                        : Container(),
                  )),
              bottom: TabAppBarWidget(tabController: _tabController),
            ),
          ];
        },
        body: Center(
            child: Column(
          children: <Widget>[
            // Container(
            //   color: hex("#F4F4F4"),
            //   height: padding,
            // ),
            Expanded(
              child: data['planBasicInfo'].length > 0
                  ? TabBarView(controller: _tabController, children: <Widget>[
                      Abnormal(
                          planBasicInfo: data['planBasicInfo'],
                          exceptions: data["exceptions"]),
                      ConstructionProcess(
                          constructionSchedule: constructionSchedule,
                          planBasicInfo: data['planBasicInfo'],
                          goToProcessDetail: goToProcessDetail),
                      Principal(planId: id)
                    ])
                  : Container(),
            ),
          ],
        )),
      ),
    );
  }

  /// 标题右侧停工复工
  Widget titleButton(
      {

      /// 是否是停��状态
      bool isSuspend}) {
    if (isSuspend == null) return Container();
    Color color = hex('#13BB87');
    String text = '停工';
    Icon icon = iconzantingcopy(color: color);
    String router = '/stop_confirm';

    /// 停工
    if (isSuspend) {
      color = hex('#13BB87');
      text = '停工';
      icon = iconzantingcopy(color: color);
      router = '/stop_confirm';
    } else {
      color = hex('#E80202');
      text = '复工';
      icon = iconfugong(color: color);
      router = '/continue_confirm';
    }
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.only(right: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(margin: EdgeInsets.only(right: 5), child: icon),
            Container(
              margin: EdgeInsets.only(top: 2),
              child: Text(
                '$text',
                style: TextStyle(
                    fontSize: 15, color: color, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ),
      onTap: () => G.pushNamed('$router', arguments: {
        "planId": id,
      }),
    );
  }
}

class TabAppBarWidget extends StatefulWidget implements PreferredSizeWidget {
  final double height;
  final double elevation; //阴影
  final TabController tabController;

  const TabAppBarWidget(
      {Key key, this.height: 0.0, this.elevation: 0.5, this.tabController})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new TabAppBarState();
  }

  @override
  Size get preferredSize => Size.fromHeight(height); //这里设置控件（appBar）的高度
}

class TabAppBarState extends State<TabAppBarWidget> {
  @override
  Widget build(BuildContext context) {
    return new PreferredSize(
        child: new Stack(
          children: <Widget>[
            new Offstage(
                offstage: false,
                child: Container(
                  // padding: const EdgeInsets.only(left: 30.0, top: 0.0),
                  color: hex('#fff'),
                  child: TabBar(
                    controller: widget.tabController,
                    labelColor: hex('#111'),
                    unselectedLabelColor: hex('#111'),
                    indicatorSize: TabBarIndicatorSize.label,
                    indicatorColor: hex('#13bb87'),
                    labelStyle: TextStyle(
                      fontSize: 15,
                      color: hex('#111'),
                      fontWeight: FontWeight.bold,
                    ),
                    unselectedLabelStyle: TextStyle(
                      color: hex('#111'),
                    ),
                    tabs: [
                      Tab(text: '异常信息'),
                      Tab(text: '施工进度'),
                      Tab(text: "负责人"),
                    ],
                  ),
                ))
          ],
        ),
        preferredSize: Size.fromHeight(widget.height));
  }
}
