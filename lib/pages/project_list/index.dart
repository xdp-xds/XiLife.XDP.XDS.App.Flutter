// import 'dart:isolate';

import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_button.dart';
import 'package:xdsapp/common_ui/xds_drawer/index.dart';
import 'package:xdsapp/pages/project_list/components/base_list.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class ProjectList extends StatefulWidget {
  @override
  _ProjectListState createState() => _ProjectListState();
}

class _ProjectListState extends State<ProjectList>
    with SingleTickerProviderStateMixin {
  static const unfinished = '在建中';
  static const finished = '已交付';
  static const pageSize = 10;
  // final String managerId = G.user.workInfo.id;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  TabController _tabController;
  int pageIndex = 1;
  String unfinishedOrder = 'asc';
  String finishedOrder = 'desc';
  bool isResetOK = false;
  bool isTapReset = false;
  Map currentPage = {
    'type': 1,
    'name': 'unfinished',
  };
  String workerId;
  bool isRefresh = false;
  List tabs = [unfinished, finished];

  Map typeList = {
    'unfinished': {
      'data': [],
      'count': 0,
    },
    'finished': {
      'data': [],
      'count': 0,
    },
  };
  List area = [];
  Map filter = {
    'planStatus': [],
    'projectDistrict': [],
    'isAbnormal': [],
  };
  List areaFilter = [];
  List statusFilter = [];
  List abnormalFilter = [];
  final List statusType = [
    {
      'text': '未开始',
      'statusCode': [2],
      'isActive': false,
    },
    {
      'text': '进行中',
      'statusCode': [3],
      'isActive': false,
    },
    {
      'text': '已停工',
      'statusCode': [5],
      'isActive': false,
    }
  ];
  final List abnormalType = [
    {
      'text': '已延期',
      // 'statusCode': [2],
      'isActive': false,
    },
    {
      'text': '未延期',
      // 'statusCode': [2],
      'isActive': false,
    }
  ];
  @override
  void initState() {
    // Todo: implement initState
    super.initState();
    // workerId = G.user.workInfo.id;
    _tabController = TabController(length: tabs.length, vsync: this)
      ..addListener(() {
        if ((_tabController.index + 1) != currentPage['type']) {
          currentPage['type'] = _tabController.index + 1;
          currentPage['name'] =
              currentPage['name'] == 'unfinished' ? 'finished' : 'unfinished';
        }
      });
    getArea();
    getProjectList(type: 1);
    Future.delayed(Duration(milliseconds: 300), () {
      getProjectList(type: 2);
    });
  }

  ///返回刷新
  // @override
  // void deactivate() {
  //   super.deactivate();
  //   if (ModalRoute.of(context).isCurrent) {
  //     resetList();
  //     getProjectList(type: currentPage['type']);
  //   }
  // }

  //获取数据列表
  getProjectList({int type}) {
    filter['planStatusType'] = type;
    String name = "unfinished";
    type == 1 ? name = 'unfinished' : name = 'finished';
    String order = type == 1 ? unfinishedOrder : finishedOrder;
    Request.getPlanList(pageIndex, pageSize, order, filter).then((res) {
      setState(() {
        typeList[name]['data'].addAll(res['data']);
        typeList[name]['count'] = res['count'];
        isResetOK = false;
        isTapReset = false;
      });
    });
  }

  changeOrder1() {
    setState(() {
      unfinishedOrder = unfinishedOrder == 'desc' ? 'asc' : 'desc';
      resetList();
      getProjectList(type: currentPage['type']);
    });
  }

  changeOrder2() {
    setState(() {
      finishedOrder = finishedOrder == 'desc' ? 'asc' : 'desc';
      resetList();
      getProjectList(type: currentPage['type']);
    });
  }

  // 是否有筛选条件
  isChoose() {
    int areaLength = areaFilter.length;
    int statusLength = statusFilter.length;
    int abnormalLength = abnormalFilter.length;
    if (areaLength == 0 && statusLength == 0 && abnormalLength == 0) {
      return false;
    } else {
      return true;
    }
  }

  // 获取筛选区域
  getArea() {
    Request.getMetaData().then((res) {
      setState(() {
        final newList = res['projectDistricts'].map((item) {
          return {
            'text': item['value'],
            'isActive': false,
            'statusCode': [item['id']],
          };
        });
        area
          ..clear()
          ..addAll(newList);
      });
    });
  }

  // 返回条数
  getNum(value) {
    String name = value == unfinished ? 'unfinished' : 'finished';
    int number = typeList[name]['count'];
    String string = number != 0 ? '($number)' : '';
    return value + string;
  }

  // 重置数据
  resetList() {
    pageIndex = 1;
    typeList[currentPage['name']]['data'] = [];
  }

  // 下拉刷新
  onRefresh() {
    resetList();
    getProjectList(type: currentPage['type']);
  }

  // 上拉加载
  onLoad() {
    pageIndex++;
    getProjectList(type: currentPage['type']);
  }

  // 点击跳转详情
  onItemTap(int index) async {
    Map currentItem = typeList[currentPage['name']]['data'][index];
    dynamic result = await G.pushNamed('/notice_detail',
        arguments: {'messageId': currentItem['id'], 'refreshType': 'list'});
    isRefresh = result['isRefresh'];
  }

  // 筛选条件卡片
  Widget chooseCard({String title, List options}) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 40, 20, 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(bottom: 30),
            child: Text(
              title,
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: hex('#111111'),
              ),
            ),
          ),
          Wrap(
            spacing: 22.5,
            runSpacing: 30,
            children: options
                .map((c) => chooseOption(
                      text: c['text'],
                      isClick: c['isActive'],
                      index: options.indexOf(c),
                      currentList: options,
                    ))
                .toList(),
          ),
        ],
      ),
    );
  }

  // 筛选选项
  Widget chooseOption(
      {String text, bool isClick, List currentList, int index}) {
    return GestureDetector(
      onTap: () {
        setState(() {
          currentList[index]['isActive'] = !currentList[index]['isActive'];
        });
      },
      child: Container(
        width: 85,
        height: 30,
        child: Center(
          child: Text(
            text,
            style: TextStyle(color: isClick ? hex('#13BB87') : hex('#111111')),
          ),
        ),
        decoration: BoxDecoration(
            color: isClick ? hex('#CEFFF0') : hex('#f4f4f4'),
            borderRadius: BorderRadius.circular(4),
            border: isClick
                ? Border.all(
                    color: hex('#13BB87'),
                  )
                : null),
      ),
    );
  }

// t头部按钮
  titleButton({String title, Icon icon, Function onTap}) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Row(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontSize: 15, color: hex('#666666')),
          ),
          Padding(
            padding: EdgeInsets.only(left: 4),
            child: icon,
          )
        ],
      ),
    );
  }

  // 列表头
  Widget topRow2() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 17),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          titleButton(
            title: '完成时间',
            icon: finishedOrder == 'desc'
                ? iconjihuashijian(color: hex('#666666'), size: 13)
                : iconjihuashijian1(color: hex('#666666'), size: 13),
            onTap: changeOrder2,
          ),
        ],
      ),
    );
  }

  Widget topRow1() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 17),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          titleButton(
            title: '计划时间',
            icon: unfinishedOrder == 'desc'
                ? iconjihuashijian(
                    color: hex('#666666'),
                    size: 13,
                  )
                : iconjihuashijian1(
                    color: hex('#666666'),
                    size: 13,
                  ),
            onTap: changeOrder1,
          ),
          titleButton(
            title: '筛选',
            icon: isChoose()
                ? iconshaixuan(
                    color: hex('##26C292'),
                    size: 13,
                  )
                : iconshaixuan1(
                    color: hex('#666666'),
                    size: 13,
                  ),
            onTap: () {
              _scaffoldKey.currentState.openEndDrawer();
            },
          ),
        ],
      ),
    );
  }

  // 重置数组
  resetStatus(List array) {
    array.forEach((item) => item['isActive'] = false);
  }

  // 重置所有
  resetAllStatus() {
    resetStatus(area);
    resetStatus(statusType);
    resetStatus(abnormalType);
  }

  // 添加筛选条件
  addFilter(current, target) {
    final array = current.where((item) => item['isActive'] == true);
    target
      ..clear()
      ..addAll(array);
  }

  // 添加所有筛选条件
  addAllFilter() {
    addFilter(area, areaFilter);
    addFilter(statusType, statusFilter);
    addFilter(abnormalType, abnormalFilter);
  }

  resetFilter() {
    isTapReset = true;
    setState(() {
      resetAllStatus();
      // addAllFilter();
    });
  }

  // 恢复筛选条件
  backFilter(current, target) {
    if (current.length != 0) {
      setState(() {
        current.map((item) {
          final currentItem =
              target.lastWhere((value) => value['text'] == item['text']);
          currentItem['isActive'] = true;
        });
      });
    }
  }

  // 恢复所有条件
  backAllFilter() {
    // backFilter(areaFilter, area);
  }

  // 格式化传参

  formatFilter(filter, array) {
    array.clear();
    filter.forEach((item) {
      if (filter == abnormalFilter) {
        switch (filter.length) {
          case 0:
            array.addAll([2, 2]);
            break;
          case 1:
            if (item['text'] == '已延期') {
              array.addAll([1, 2]);
            } else {
              array.addAll([2, 1]);
            }
            break;
          case 2:
            array.clear();
            array.addAll([1, 1]);
            break;
        }
      } else {
        item['statusCode'].forEach((value) {
          array.add(value);
        });
      }
    });
  }

  // 刷新查询条件
  refreshFilter() {
    formatFilter(statusFilter, filter['planStatus']);
    formatFilter(areaFilter, filter['projectDistrict']);
    formatFilter(abnormalFilter, filter['isAbnormal']);
  }

// 按钮组
  Widget buttonGroup() {
    return Container(
      padding: EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          XdsButton(
            width: 140,
            height: 44,
            title: '重置',
            enableColor: hex('#ffffff'),
            textColor: hex('##13BB87'),
            border: Border.all(color: hex('##13BB87')),
            onTap: resetFilter,
          ),
          XdsButton(
            width: 140,
            height: 44,
            title: '确认',
            onTap: () {
              Navigator.pop(context);
              resetList();
              addAllFilter();
              refreshFilter();
              getProjectList(type: currentPage['type']);
            },
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: XdsAppBar(
        title: Text('项目列表'),
        actions: <Widget>[Container()],
        bottom: TabBar(
            controller: _tabController,
            onTap: (i) {
              if ((i + 1) != currentPage['type']) {
                currentPage['type'] = i + 1;
                currentPage['name'] = currentPage['name'] == 'unfinished'
                    ? 'finished'
                    : 'unfinished';
              }
            },
            indicatorSize: TabBarIndicatorSize.label,
            indicatorColor: hex('#13bb87'),
            labelStyle: TextStyle(fontWeight: FontWeight.bold),
            unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
            tabs: tabs
                .map((e) => Tab(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 10, bottom: 10),
                        child: Text(getNum(e)),
                      ),
                    ))
                .toList()),
      ),
      endDrawer: XdsDrawer(
        onOpen: backAllFilter,
        width: 340.0,
        child: SafeArea(
          // constraints: BoxConstraints.expand(),
          child: Stack(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  chooseCard(title: '区域选择', options: area),
                  Container(
                    height: 10,
                    decoration: BoxDecoration(color: hex('#f4f4f4')),
                  ),
                  chooseCard(title: '状态选择', options: statusType),
                  Container(
                    height: 10,
                    decoration: BoxDecoration(color: hex('#f4f4f4')),
                  ),
                  chooseCard(title: '异常选择', options: abnormalType),
                ],
              ),
              Positioned(
                left: 0,
                right: 0,
                bottom: 0,
                child: buttonGroup(),
              )
            ],
          ),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(color: hex('#f4f4f4')),
        child: TabBarView(
          controller: _tabController,
          children: <Widget>[
            BaseList(
              dataList: typeList['unfinished']['data'],
              onRefresh: onRefresh,
              onLoad: onLoad,
              pageSize: pageSize,
              pageCount: typeList['unfinished']['count'],
              onItemTap: (index) {
                onItemTap(index);
              },
              currentPage: currentPage,
              topRow: topRow1(),
            ),
            BaseList(
              dataList: typeList['finished']['data'],
              onRefresh: onRefresh,
              onLoad: onLoad,
              pageSize: pageSize,
              pageCount: typeList['finished']['count'],
              onItemTap: (index) {
                onItemTap(index);
              },
              currentPage: currentPage,
              topRow: topRow2(),
            ),
          ],
        ),
      ),
    );
  }
}
