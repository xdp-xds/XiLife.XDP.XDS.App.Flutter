import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:xdsapp/common_ui/xds_list_row/index.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';

class BaseList extends StatefulWidget {
  final List dataList;
  final Function onRefresh;
  final Function onLoad;
  final int pageSize;
  final int pageCount;
  final Function onItemTap;
  final Map currentPage;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Widget topRow;

  /// 基础列表
  ///
  /// ``` dart
  /// @param dataList - 数据数组
  /// @param onRefresh - 下拉刷新事件
  /// @param onLoad - 上拉加载
  /// @param pageSize - 列表单页大小
  /// @param pageCount - 列表总数
  /// @param onItemTap - 列表项点击操作
  /// @param currentPage - 当前页
  /// ```

  BaseList({
    this.dataList,
    this.onRefresh,
    this.pageSize,
    this.pageCount,
    this.onLoad,
    this.onItemTap,
    this.currentPage,
    this.scaffoldKey,
    this.topRow,
  });

  @override
  _BaseListState createState() => _BaseListState();
}

class _BaseListState extends State<BaseList> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  // 项目列表子项
  Widget projectItem(project) {
    if (project['delayDays'] > 0 &&
        project['status'] != 5 &&
        project['status'] != 6) {
      return baseItem(Status.delay, project);
    } else {
      switch (project['status']) {
        case 2:
        case 3:
          return baseItem(Status.normal, project);
          break;
        case 6:
          return baseItem(Status.complate, project);
          break;
        case 5:
          return baseItem(Status.pause, project);
          break;
        default:
          return Container();
      }
    }
  }

  // 时间处理
  time(String time) {
    final str = time.split('T')[0];
    final newTime = str.split('-');
    return '${newTime[0]}/${newTime[1]}/${newTime[2]}';
  }

  // 基本子项
  baseItem(type, data) {
    return GestureDetector(
      onTap: () {
        G.pushNamed('/project_detail', arguments: {'id': data['id']});
      },
      child: XdsListRow(
        status: type,
        title: "${data['order']['projectName']}",
        bottomLeft: type != Status.complate
            ? '计划: ${time(data['startDate'])} - ${time(data['endDate'])}'
            : '交付: ${time(data['actualEndDate'])}',
        bottomRight:
            type == Status.complate ? '工期${data['constructionPeriod']}天' : '',
        topRight: topRightWidget(type, data),
      ),
    );
  }

  topRightWidget(type, data) {
    switch (type) {
      case Status.delay:
        return Text(
          '延期${data['delayDays']}天',
          style: TextStyle(
            fontSize: 15,
            color: hex('#FA6510'),
          ),
        );
        break;
      case Status.pause:
        return Text(
          '已停工',
          style: TextStyle(
            fontSize: 15,
            color: hex('#E80202'),
          ),
        );
        break;
      case Status.complate:
        return data['delayDays'] > 0
            ? Text(
                '延期${data['delayDays']}天',
                style: TextStyle(
                  fontSize: 15,
                  color: hex('#111111'),
                ),
              )
            : Container();
        break;
      default:
        return Container();
    }
  }

  // 异常数量
  Widget abnormal(num) {
    return Row(
      children: <Widget>[
        iconyichang(color: hex('#e80202'), size: 14),
        Padding(
          padding: EdgeInsets.only(left: 7, top: 2),
          child: Text(
            '$num',
            style: TextStyle(
              color: hex('#e80202'),
              fontSize: 15,
            ),
          ),
        ),
      ],
    );
  }

  // 下拉刷新
  void _onRefresh() async {
    await widget.onRefresh();
    if (mounted) {
      _refreshController.loadComplete();
    }
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  // 下拉刷新
  void _onLoading() async {
    // monitor network fetch
    await widget.onLoad();

    if (mounted) {
      // if failed,use loadFailed(),if no data return,use LoadNodata()
      if (widget.dataList.length == widget.pageCount)
        return _refreshController.loadNoData();
    }

    _refreshController.loadComplete();
  }

  ///是否开启上拉加载
  bool isLoad() {
    int length = widget.dataList.length;
    if (length < widget.pageSize) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: hex('#f4f4f4'),
      child: Column(
        children: <Widget>[
          widget.topRow,
          Expanded(
            child: XdsPullToRefresh(
              controller: _refreshController,
              onRefresh: _onRefresh,
              onLoading: _onLoading,
              enablePullUp: isLoad(),
              data: widget.dataList,
              child: ListView.separated(
                itemCount: widget.dataList.length,
                separatorBuilder: (c, i) {
                  return Divider(
                    height: 20.0,
                    color: hex('#f4f4f4'),
                  );
                },
                itemBuilder: (c, i) {
                  return projectItem(widget.dataList[i]);
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
