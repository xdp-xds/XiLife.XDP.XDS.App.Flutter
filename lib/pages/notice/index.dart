import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/pages/notice/components/list_base.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/constant/index.dart';

class NoticeList extends StatefulWidget {
  @override
  _NoticeListState createState() => _NoticeListState();
}

class _NoticeListState extends State<NoticeList>
    with SingleTickerProviderStateMixin {
  static const construction = '施工';
  static const risk = '风险';
  static const pageSize = 10;
  TabController _tabController;
  int pageIndex = 1;
  int currentIndex = 1;
  String workerId;
  bool isRefresh = false;
  int usertype = G.user.workInfo.type;

  List tabs = [
    risk,
    construction,
  ];
  Map typeList = {
    'construction': {'data': [], 'number': 0, 'taskCount': 0, "pageIndex": 1},
    'risk': {'data': [], 'number': 0, 'taskCount': 0, "pageIndex": 1},
  };
  @override
  void initState() {
    // Todo: implement initState
    super.initState();
    if (usertype == UserType.projectManager) {
      currentIndex = 0;
    }
    workerId = G.user.workInfo.id;
    _tabController = TabController(length: tabs.length, vsync: this)
      ..addListener(() {
        // currentType = _tabController.index + 1;
      });
    // getUserInfo();
    Future.delayed(Duration.zero, () async {
      await getUserInfo();
    });
  }

  @override
  void deactivate() {
    super.deactivate();
    if (isRefresh) {
      resetList();
      int currentType = returnCurrentType();
      getTaskCount(currentType);
      getMessageList(type: currentType);
    }
  }

  getUserInfo() {
    getMessageList(type: MessageCategoryEnum.Construction);
    getTaskCount(MessageCategoryEnum.Construction);
    //项目才查风险
    if (usertype == UserType.projectManager) {
      getMessageList(type: MessageCategoryEnum.Risk);
      getTaskCount(MessageCategoryEnum.Risk);
    }
  }

  //获取消息列表
  getMessageList({int type}) {
    String listType = returnListType(type);
    final data = {
      'workerId': workerId.toString(),
      'category': type,
      'pageIndex': typeList[listType]["pageIndex"],
      'pageSize': pageSize,
    };
    Request.getMessageList(data).then((res) {
      setState(() {
        if (isRefresh) {
          typeList[listType]['data'] = [];
          isRefresh = false;
        }
        typeList[listType]['data'].addAll(res['data']);
        typeList[listType]['number'] = res['count'];
      });
    });
  }

  // 获取未读消息数
  getTaskCount(type) {
    Request.getMesgCount({
      'workerId': workerId.toString(),
      'category': type,
    }).then((res) {
      setState(() {
        String listType = returnListType(type);
        typeList[listType]['taskCount'] = res;
      });
    });
  }

  // 返回未读通知条数
  getUnReadNum(value) {
    String type = value == construction ? 'construction' : 'risk';
    int number = typeList[type]['taskCount'];
    String string = number != 0 ? '($number)' : '';
    return value + string;
  }

  // 重置数据
  resetList() {
    // pageIndex = 1;
    int currentType = returnCurrentType();
    String listType = returnListType(currentType);
    setState(() {
      typeList[listType]["pageIndex"] = 1;
      isRefresh = true;
    });
  }

  // 下拉刷新
  onRefresh() {
    resetList();
    int currentType = returnCurrentType();
    getMessageList(type: currentType);
    getTaskCount(currentType);
  }

  // 上拉加载
  onLoad() {
    int currentType = returnCurrentType();
    String listType = returnListType(currentType);
    setState(() {
      typeList[listType]["pageIndex"]++;
    });
    getMessageList(type: currentType);
  }

  // 点击跳转详情
  onItemTap(int index) async {
    int currentType = returnCurrentType();
    String listType = returnListType(currentType);
    Map currentItem = typeList[listType]['data'][index];
    setState(() {
      //0未读
      if (currentItem['readStatus'] == 0 &&
          typeList[listType]['taskCount'] > 0) {
        currentItem['readStatus'] = 1;
        typeList[listType]['taskCount']--;
      }
    });
    dynamic result = await G.pushNamed('/notice_detail',
        arguments: {'messageId': currentItem['id'], 'refreshType': 'list'});
    isRefresh = result['isRefresh'];
  }

  String returnListType(type) {
    String listType = 'risk';
    if (type == MessageCategoryEnum.Construction) {
      listType = "construction";
    }
    return listType;
  }

  int returnCurrentType() {
    int currentType = MessageCategoryEnum.Construction;
    if (currentIndex == 0) {
      currentType = MessageCategoryEnum.Risk;
    }
    return currentType;
  }

  Widget tabview() {
    return TabBarView(
      controller: _tabController,
      children: <Widget>[
        ListBase(
          noticeList: typeList['risk']['data'],
          onRefresh: onRefresh,
          onLoad: onLoad,
          pageSize: pageSize,
          pageCount: typeList['risk']['number'],
          onItemTap: (index) {
            onItemTap(index);
          },
        ),
        ListBase(
          noticeList: typeList['construction']['data'],
          onRefresh: onRefresh,
          onLoad: onLoad,
          pageSize: pageSize,
          pageCount: typeList['construction']['number'],
          onItemTap: (index) {
            onItemTap(index);
          },
        ),
      ],
    );
  }

  titleText() {
    if (usertype != UserType.projectManager) {
      if (typeList["construction"]['taskCount'] > 0) {
        return "通知列表(${typeList["construction"]['taskCount']})";
      }
      return "通知列表";
    }
    return "通知列表";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: Text(titleText()),
        //项目经理才有风险
        bottom: usertype == UserType.projectManager
            ? TabBar(
                controller: _tabController,
                onTap: (i) {
                  currentIndex = i;
                },
                indicatorSize: TabBarIndicatorSize.label,
                indicatorColor: hex('#13bb87'),
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                unselectedLabelStyle: TextStyle(fontWeight: FontWeight.normal),
                tabs: tabs
                    .map((e) => Tab(
                          // text: getUnReadNum(e),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 10, bottom: 10),
                            child: Text(getUnReadNum(e)),
                          ),
                        ))
                    .toList())
            : null,
      ),
      body: Container(
          decoration: BoxDecoration(color: hex('#f4f4f4')),
          //项目经理才有风险
          child: usertype == UserType.projectManager
              ? tabview()
              : ListBase(
                  noticeList: typeList['construction']['data'],
                  onRefresh: onRefresh,
                  onLoad: onLoad,
                  pageSize: pageSize,
                  pageCount: typeList['construction']['number'],
                  onItemTap: (index) {
                    onItemTap(index);
                  },
                )),
    );
  }
}
