import 'dart:convert';

import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/utils/global.dart';
import "package:xdsapp/constant/index.dart";
import 'package:xdsapp/utils/parse_date.dart';

class NoticeDetail extends StatefulWidget {
  final Map args;
  NoticeDetail({Key key, this.args}) : super(key: key);

  _NoticeDetailState createState() => _NoticeDetailState();
}

class _NoticeDetailState extends State<NoticeDetail> {
  Map<String, dynamic> data;
  List contentList;

  String msgType;

  @override
  void initState() {
    super.initState();
    String id = widget.args['messageId'];
    Request.getNoticeDetail(id).then((res) {
      setState(() {
        data = res;
        msgType = data['messageType'].toString();
        contentList = res['content'].split('\n');
      });
    });
  }

  // 根据type去往不同的界面
  toWhere() {
    switch (msgType) {
      case '1':
        return {
          'path': '/task_progress_update',
          'arguments': {
            'planId': json.decode(data['params'])['planId'],
            "taskId": json.decode(data['params'])['taskId'],
          }
        };
        break;
      case '2':
        return {
          'path': '/task_assignment',
          'arguments': {
            'planId': json.decode(data['params'])['planId'],
            "deliveryStageId": json.decode(data['params'])['deliveryStageId'],
            "taskId": json.decode(data['params'])['taskId'],
          }
        };
        break;
      case '3':
        /// 不要问我为什么？params里面的参数一下是大驼峰，一下是小驼峰
        Map jsonData = json.decode(data['params'].toString().toLowerCase());
        print(jsonData);
        return {
          'path': '/task_assignment_process',
          'arguments': {
            'planId': jsonData['planid'],
            'workstationId': jsonData['workstationid'],
            "taskId": jsonData['taskid'],
          }
        };
        break;
      case '4':
        String route = '/task_progress_update';
        int processType = json.decode(data['params'])['ProcessType'];
        if (processType == ProcessTypeEnum.Send) {
          route = "/task_progress_update_material";
        }
        return {
          'path': route,
          'arguments': {
            'planId': json.decode(data['params'])['PlanId'],
            "taskId": json.decode(data['params'])['TaskId'],
          },
        };
        break;
      case '5':
      case '9':
        return {
          'path': '/project_detail',
          'arguments': {'id': json.decode(data['params'])['PlanId']},
        };
        break;
      case MessageType.planDelayRisk:
        return {
          'path': '/project_detail',
          'arguments': {'id': json.decode(data['params'])['planId']},
        };
        break;
    }
  }

  ///获取消息状态

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        title: data != null ? Text('${data['title']}') : Text(''),
        leadingOnPressed: () {
          G.pop({
            'isRefresh': widget.args['refreshType'] == 'list' ? false : true
          });
        },
      ),
      body: Container(
        color: hex('#F4F4F4'),
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  data != null ? Text('${contentList[0]}') : Text(''),
                  data != null
                      ? Text(
                          '${dealtimeSlash(data['createdTime'])}',
                          style: TextStyle(color: hex("#999")),
                        )
                      : Text('')
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 35),
              child: data != null
                  ? Text(
                      '${contentList[1]}',
                      textAlign: TextAlign.left,
                      style: TextStyle(fontSize: 14, height: 1.75),
                    )
                  : Text(''),
            ),
            msgType != '7'
                ? Container(
                    margin: EdgeInsets.only(top: 95),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        GestureDetector(
                          behavior: HitTestBehavior.opaque,
                          onTap: () {
                            Map router = toWhere();
                            if (router['path'] != '') {
                              G.pushNamed(router['path'],
                                  arguments: router['arguments']);
                            }
                          },
                          child: Flex(
                            direction: Axis.horizontal,
                            children: <Widget>[
                              Text(
                                '了解详情',
                                style: TextStyle(color: hex('#13bb87')),
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 8),
                                child: Icon(
                                  Icons.arrow_forward,
                                  size: 18,
                                  color: hex('#13bb87'),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
