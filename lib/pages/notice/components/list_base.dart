import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/index.dart';
import 'package:xdsapp/utils/parse_date.dart';

class ListBase extends StatefulWidget {
  final List noticeList;
  final Function onRefresh;
  final Function onLoad;
  final int pageSize;
  final int pageCount;
  final Function onItemTap;

  /// 通知基础列表
  ///
  /// ``` dart
  /// @param noticeList - 通知数据数组
  /// @param onRefresh - 下拉刷新事件
  /// @param onLoad - 上拉加载
  /// @param pageSize - 列表单页大小
  /// @param pageCount - 列表总数
  /// @param onItemTap - 列表项点击操作
  /// ```

  ListBase({
    this.noticeList,
    this.onRefresh,
    this.pageSize,
    this.pageCount,
    this.onLoad,
    this.onItemTap,
  });

  @override
  _ListBaseState createState() => _ListBaseState();
}

class _ListBaseState extends State<ListBase> {
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  Widget divider = Divider(
    color: hex('#e6e6e6'),
    height: 0.5,
  );
  // 红色圆点
  Widget redDot() {
    return Container(
      margin: EdgeInsets.only(left: 12),
      width: 6,
      height: 6,
      decoration: BoxDecoration(
        color: hex('#e44040'),
        borderRadius: BorderRadius.circular(6),
      ),
    );
  }

  // 通知时间
  Widget noticeTime(String time) {
    String nonTime = dealtimeSlash(time);
    return Text(
      nonTime,
      style: TextStyle(
        fontSize: 13,
        color: hex('#666666'),
      ),
    );
  }

  // 通知内容
  Widget noticeItem(Map notice, int index) {
    String content = notice['content'].replaceAll('\n', '');
    int contentIndex = content.indexOf(":");
    if (contentIndex >= 0) {
      content = content.substring(contentIndex + 1);
    }
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => widget.onItemTap(index),
      child: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(children: <Widget>[
                    Text(
                      notice['title'],
                      style: TextStyle(
                        color: hex('#111111'),
                        fontSize: 15,
                      ),
                    ),
                    notice['readStatus'] == 0 ? redDot() : Container(),
                  ]),
                  noticeTime(notice['createdTime'])
                ],
              ),
            ),
            SizedBox(
              // width: 231,
              // height: 20,
              child: Text(
                content,

                // overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: hex('#666666'),
                  fontSize: 14,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  // 下拉刷新
  void _onRefresh() async {
    await widget.onRefresh();
    if (mounted) {
      _refreshController.loadComplete();
    }
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  //上拉加载
  void _onLoading() async {
    // monitor network fetch
    await widget.onLoad();

    if (mounted) {
      // if failed,use loadFailed(),if no data return,use LoadNodata()
      if (widget.noticeList.length == widget.pageCount)
        return _refreshController.loadNoData();
    }

    _refreshController.loadComplete();
  }

  ///是否开启上拉加载
  bool isLoad() {
    int length = widget.noticeList.length;
    if (length < widget.pageSize) {
      return false;
    } else {
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return XdsPullToRefresh(
      controller: _refreshController,
      onRefresh: _onRefresh,
      onLoading: _onLoading,
      enablePullUp: isLoad(),
      data: widget.noticeList,
      child: ListView.separated(
        itemCount: widget.noticeList.length,
        itemBuilder: (c, i) {
          return noticeItem(widget.noticeList[i], i);
        },
        separatorBuilder: (c, i) {
          return divider;
        },
        padding: const EdgeInsets.symmetric(horizontal: 15),
      ),
    );
  }
}
