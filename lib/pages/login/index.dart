// import 'package:color_dart/color_dart.dart';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:xdsapp/api/api_config.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_button.dart';
import 'package:xdsapp/config.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:xdsapp/utils/showUpdate.dart';
import 'package:package_info/package_info.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 140, 20, 0),
          child: Column(
            children: <Widget>[
              Center(
                child: Image.asset(
                  './lib/assets/images/logo.png',
                  width: 200,
                ),
              ),
              // Text(ApiConfig.baseUrl),
              LoginForm(),
            ],
          ),
        ),
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  static const int timeEnd = 60;
  static const int phoneLength = 11;
  int timeCode = timeEnd;
  bool isGetSMS = true;
  bool isLogin = false;
  bool isSmsCode = false;
  String phone = '';
  String smsCode = '';
  Timer timer;
  String userId;
  TextEditingController _phoneController = new TextEditingController();
  TextEditingController _smsController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    clearToken();
    _phoneController.addListener(() {
      phone = _phoneController.text;
      if (phone.length == phoneLength) {
        setState(() {
          isSmsCode = true;
        });
      } else {
        setState(() {
          isSmsCode = false;
        });
      }
      changeLoginStatus();
    });
    _smsController.addListener(() {
      smsCode = _smsController.text;
      changeLoginStatus();
    });
    checkoutVersion();
  }

  checkoutVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    print(packageInfo.packageName);
    try {
      Request.getVersion({
        'package': packageInfo.packageName,
        'ver': packageInfo.version,
        'platform': G.isIOS ? 2 : 3,
      }).then((val) {
        print(val);
        var locals = packageInfo.version.split('.');
        var nets = val['versionNumber'] != null
            ? val['versionNumber'].toString().split('.')
            : [];
        var isShow = false;
        if (nets.length > locals.length) {
          isShow = true;
        } else {
          for (var i = 0; i < nets.length; i++) {
            if (int.parse(nets[i]) > int.parse(locals[i])) {
              isShow = true;
              break;
            }
          }
        }
        if (isShow) {
          ShowUpdate(
            context,
            remarks: val['remarks'],
            url: val['downloadUri'],
            isForce: val['forceUpdateStatus'],
            version: val['versionNumber'],
          );
        }
      });
    } catch (e) {
      print(e);
    }
  }

  // 清除token信息
  clearToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  // change登录按钮状态
  changeLoginStatus() {
    if (phone.length == phoneLength && smsCode.length == 6) {
      setState(() {
        isLogin = true;
      });
    } else {
      setState(() {
        isLogin = false;
      });
    }
  }

// 获取验证码
  _changeSmsStatus() async {
    if (isSmsCode && isGetSMS) {
      if (await _getSms()) {
        setState(() {
          isGetSMS = false;
        });
        timer = new Timer.periodic(const Duration(milliseconds: 1000), (timer) {
          setState(() {
            timeCode--;
          });
          if (timeCode == 0) {
            timer.cancel();
            isGetSMS = true;
            timeCode = timeEnd;
          }
        });
      }
    }
  }

  //发送验证码检测并发送请求
  _getSms() async {
    try {
      var res = await Request.getSmsCode({
        'mobile': phone,
      });

      if (res != null) {
        Fluttertoast.showToast(
          msg: "验证码发送成功",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 2,
          fontSize: 10.0,
        );
      }

      return true;
    } catch (e) {
      return false;
    }
  }

  login() {
    if (phone.length == phoneLength && smsCode.length == 6) {
      Request.login(
              {'mobile': phone, 'code': smsCode, 'clientId': Config.clientId})
          .then((res) async {
        //去掉定时器
        if (res != null) {
          timer.cancel();
          await G.user.setToken(
              refreshToken: res['refresh_token'],
              accessToken: res['access_token']);
          await G.user.parsingToken(res['access_token']);
          Fluttertoast.showToast(
              msg: "登录成功",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.CENTER,
              timeInSecForIos: 2,
              fontSize: 10.0);
          G.pushNamed('/');
        }
      });
    }
  }

  @override
  void dispose() {
    //去掉定时器
    if (timer != null) timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: 40),
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 36),
          decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Color.fromARGB(22, 0, 0, 0),
                    offset: Offset(0, 0),
                    blurRadius: 10,
                    spreadRadius: 1)
              ],
              borderRadius: BorderRadius.circular(4)),
          child: Column(
            children: <Widget>[
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '手机号',
                      style: TextStyle(
                          fontSize: 15, color: const Color(0xff111111)),
                    ),
                    TextField(
                      controller: _phoneController,
                      keyboardType: TextInputType.phone,
                      maxLength: phoneLength,
                      inputFormatters: [
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      decoration: InputDecoration(
                        counterText: '',
                        hintText: "请输入11位手机号码",
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 27),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '验证码',
                      style: TextStyle(
                          fontSize: 15, color: const Color(0xff111111)),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            controller: _smsController,
                            maxLength: 6,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              counterText: '',
                              hintText: "请输入验证码",
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 13),
                          child: GestureDetector(
                            child: Container(
                              width: 86,
                              height: 37,
                              child: Center(
                                child: isGetSMS
                                    ? Text(
                                        '获取验证码',
                                        style: TextStyle(
                                          color: isSmsCode
                                              ? Color(0xff111111)
                                              : Color(0xffbbbbbb),
                                        ),
                                      )
                                    : Text(
                                        '$timeCode' + 's',
                                        style: TextStyle(
                                          color: isSmsCode
                                              ? Color(0xff111111)
                                              : Color(0xffbbbbbb),
                                        ),
                                      ),
                              ),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: isSmsCode
                                          ? Color(0xff111111)
                                          : Color(0xffbbbbbb),
                                      width: 1),
                                  borderRadius: BorderRadius.circular(3)),
                            ),
                            onTap: _changeSmsStatus,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 21),
          child: XdsButton(
            isActive: isLogin,
            title: '登录',
            onTap: login,
          ),
        )
      ],
    );
  }
}
