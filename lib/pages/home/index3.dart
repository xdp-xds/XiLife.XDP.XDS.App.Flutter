import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/api/api_config.dart';
import 'package:flutter_picker/flutter_picker.dart';
import './date_picker_bottom_sheet.dart';

const PickerData = [
  {
    "a": [
      {
        "a1": [1, 2, 3, 4]
      },
      {
        "a2": [5, 6, 7, 8]
      },
      {
        "a3": [9, 10, 11, 12]
      }
    ]
  },
  {
    "b": [
      {
        "b1": [11, 22, 33, 44]
      },
      {
        "b2": [55, 66, 77, 88]
      },
      {
        "b3": [99, 1010, 1111, 1212]
      }
    ]
  },
  {
    "c": [
      {
        "c1": ["a", "b", "c"]
      },
      {
        "c2": ["aa", "bb", "cc"]
      },
      {
        "c3": ["aaa", "bbb", "ccc"]
      }
    ]
  }
];

class HomeTest extends StatefulWidget {
  @override
  _HomeTestState createState() => _HomeTestState();
}

class _HomeTestState extends State<HomeTest> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    super.initState();
    //初始化状态
  }

  showPicker(BuildContext context) {
    Picker picker = Picker(
        adapter: PickerDataAdapter<String>(pickerdata: PickerData),
        changeToFirst: true,
        textAlign: TextAlign.left,
        textStyle: const TextStyle(color: Colors.blue),
        selectedTextStyle: TextStyle(color: Colors.red),
        columnPadding: const EdgeInsets.all(8.0),
        onConfirm: (Picker picker, List value) {});
    picker.show(_scaffoldKey.currentState);
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 16.0);
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.fromLTRB(20, 140, 20, 0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[Text('HomeTest')],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('go to Login & 设置ios小红点99个'),
                    onPressed: () {
                      G.pushNamed('/login');
                    },
                  ),
                ],
              ),

              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('jpush.getRegistrationID()'),
                    onPressed: () {
                      G.jpush.getRegistrationID().then((id) {});
                    },
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('登出'),
                    onPressed: () async {
                      G.user.clear();
                      G.pushNamed('/login');
                    },
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('api地址'),
                    onPressed: () {},
                  ),
                  Text(ApiConfig.baseUrl)
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('设置别名'),
                    onPressed: () {
                      G.jpush.setAlias('18728321372').then((val) {});
                    },
                  ),
                ],
              ),
              // Image.asset(
              //   './lib/assets/images/LOGO.png',
              //   width: 200,
              // ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('通知列表'),
                    onPressed: () {
                      G.pushNamed('/notice');
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('获取user信息'),
                    onPressed: () async {
                      var accessToken =
                          'eyJhbGciOiJSUzI1NiIsImtpZCI6IkY1NDlENDE1OUREMkE4NTgzNzM2MDhBMjMyOTVDOUFEMUVDQTk1QjIiLCJ0eXAiOiJKV1QiLCJ4NXQiOiI5VW5VRlozU3FGZzNOZ2lpTXBYSnJSN0tsYkkifQ.eyJuYmYiOjE1NzEyODIyMDksImV4cCI6MTYwMjgxODIwOSwiaXNzIjoiYXBpLmxpeGlhbmd6aHUuY29tIiwiYXVkIjpbImFwaS5saXhpYW5nemh1LmNvbS9yZXNvdXJjZXMiLCJ4ZHAtYXV0aC1hcGkiLCJ4ZHAteGRzLWFwaSJdLCJjbGllbnRfaWQiOiJ4aWxpZmUueGRwLnhkcy5hcHAubG9naW4iLCJzdWIiOiIxODcyODMyMTM3MiIsImF1dGhfdGltZSI6MTU3MTI4MjIwOSwiaWRwIjoibG9jYWwiLCJNb2JpbGUiOiIxODcyODMyMTM3MiIsIlVzZXJOYW1lIjoiSVQt6IKW6LW36ZizIiwiVXNlcklkIjoiMTAwNSIsIlN0YWZmSWQiOiIxMDA1IiwiUm9sZSI6IlByb2plY3RBZG1pbiIsInNjb3BlIjpbIm9wZW5pZCIsInByb2ZpbGUiLCJ4ZHAtYXV0aC1hcGkiLCJ4ZHAteGRzLWFwaSIsIm9mZmxpbmVfYWNjZXNzIl0sImFtciI6WyJjdXN0b20iXX0.c4Tt2mhKZ-dny0gKwtPTBAg3fb9M0o4gu6E7FchXOIIqQ0PDvoS-sGzAR5qPLlY10rFs-Jw8HI2RsCBlLxz2NJzMkImD6gU_5y_M98iI2SsEukiZgdb2FetBLAdAeEwpTvTgDPYR7F8shatsHpANv6gxLuOniA3_mc1rZiMkGXkhFFDCgv_meGRiBAZiyvV1BOSc1zdKL4hvoTweqNlebnQ2ahw0yyRCNpFgiyTdgf1zCvqWx39yEoqA2Pdh_xF4rxPNowjqztzAa9bPcilqaxMmiDzbuY2XOl1RQeyTPG1rsGnOhKerQPrg2uL6usfsUuQy4Q5mGUjY10ltHmEWCQ';
                      G.user.setToken(
                        accessToken: accessToken,
                      );
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('项目列表'),
                    onPressed: () {
                      G.pushNamed('/project_list');
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('XdsListRow列表组件'),
                    onPressed: () {
                      G.pushNamed('/example_xds_list_row');
                    },
                  )
                ],
              ),

              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('XdsDialog弹窗'),
                    onPressed: () {
                      G.pushNamed('/example_xds_dialog');
                    },
                  )
                ],
              ),

              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('XdsCollapse折叠面板'),
                    onPressed: () {
                      G.pushNamed('/example_xds_collapse');
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('任务列表'),
                    onPressed: () {
                      G.pushNamed('/task_list');
                    },
                  )
                ],
              ),
              Row(
                children: <Widget>[
                  FlatButton(
                    color: hex('#ddd000'),
                    child: Text('项目基本信息'),
                    onPressed: () {
                      G.pushNamed('/example_xds_project_base_detail');
                    },
                  )
                ],
              ),

              RaisedButton(
                child: Text('Picker Show'),
                onPressed: () {
                  showPicker(context);
                },
              ),
              RaisedButton(
                color: Colors.blue,
                child: Text("DatePicker Bottom Sheet", style: textStyle),
                onPressed: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return DatePickerBottomSheet();
                  }));
                },
              ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //   children: <Widget>[ConstructionItem()],
              // )
            ],
          ),
        ),
      ),
    );
  }
}
