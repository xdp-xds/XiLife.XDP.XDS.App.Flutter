import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:xdsapp/api/api.dart';
import 'package:xdsapp/common_ui/xds_appbar/index.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/index.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/utils/showUpdate.dart';
import 'package:package_info/package_info.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with WidgetsBindingObserver {
  Map data = {};
  final phone = G.user.workInfo.phone;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  void _onRefresh() async {
    await getTodoList();

    if (mounted) {
      _refreshController.loadComplete();
    }
    // if failed,use refreshFailed()
    return _refreshController.refreshCompleted();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    Future.delayed(Duration.zero, () async {
      getTodoList();
    });
    if (phone != null) {
      G.jpush.setAlias(phone).then((map) {});
    }
    checkoutVersion();
  }

  signOut() async {
    G.jpush.deleteAlias().then((map) {});
    G.user.clear();
    G.pushNamed('/login');
  }

  @override
  void deactivate() {
    super.deactivate();
    if (ModalRoute.of(context).isCurrent) {
      getTodoList();
      // print('进来了！');
    }
    // print('进来了！');
  }

  getTodoList() async {
    try {
      var res = await Request.getTodoList(G.user.workInfo.id);
      setState(() {
        data = res['data'];
      });
    } catch (e) {}
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    //监听app后台进入
    if (state == AppLifecycleState.resumed) {
      getTodoList();
    }
  }

  checkoutVersion() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    print(packageInfo.packageName);
    try {
      Request.getVersion({
        'package': packageInfo.packageName,
        'ver': packageInfo.version,
        'platform': G.isIOS ? 2 : 3,
      }).then((val) {
        print(val);
        var locals = packageInfo.version.split('.');
        var nets = val['versionNumber'] != null
            ? val['versionNumber'].toString().split('.')
            : [];
        var isShow = false;
        if (nets.length > locals.length) {
          isShow = true;
        } else {
          for (var i = 0; i < nets.length; i++) {
            if (int.parse(nets[i]) > int.parse(locals[i])) {
              isShow = true;
              break;
            }
          }
        }
        if (isShow) {
          ShowUpdate(
            context,
            remarks: val['remarks'],
            url: val['downloadUri'],
            isForce: val['forceUpdateStatus'],
            version: val['versionNumber'],
          );
        }
      });
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: XdsAppBar(
        centerTitle: false,
        automaticallyImplyLeading: false,
        newLeading: false,
        title: Text(
          '你好，${G.user.userInfo.userName}',
          style: TextStyle(
              color: hex('#0d0d0d'), fontSize: 16, fontWeight: FontWeight.bold),
        ),
        actions: <Widget>[
          GestureDetector(
            onTap: () async {
              await G.pushNamed('/notice');
              getTodoList();
            },
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(right: 18),
              child: Stack(
                children: <Widget>[
                  /// 通过调整width height 控制红点位置
                  Container(
                    width: 40,
                    height: 40,
                    child: icontongzhi(),
                  ),
                  Positioned(
                    right: 10,
                    top: 10,
                    child: (data == null || data['countOfMsgs'] == 0)
                        ? Container()
                        : Container(
                            width: 5,
                            height: 5,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: hex('#ef2832')),
                          ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: XdsPullToRefresh(
        enablePullUp: false,
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: SafeArea(
          bottom: false,
          child: Stack(
            children: <Widget>[
              Container(
                color: hex('#fff'),
                margin: EdgeInsets.only(bottom: 84),
                padding: EdgeInsets.symmetric(horizontal: 18),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 20),
                      child: buildCard(
                          title: '全部任务',
                          allNumber: data['allTasks'],
                          number: data['countOfTasks'],
                          text: '待办任务',
                          imgSrc: 'lib/assets/images/task.png',
                          onPress: () async {
                            await G.pushNamed('/task_list');
                            getTodoList();
                          }),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30),
                      child: buildCard(
                          title: '全部项目',
                          allNumber: data['allPlans'],
                          number: data['counOfPlans'],
                          text: '在建项目',
                          imgSrc: 'lib/assets/images/project.png',
                          onPress: () async {
                            await G.pushNamed('/project_list');
                            getTodoList();
                          }),
                    ),
                  ],
                ),
              ),
              Positioned(
                bottom: 0,
                left: 0,
                child: GestureDetector(
                  onTap: () {
                    XdsDialog.confirm(context).show(onOk: signOut);
                  },
                  child: Container(
                    height: 84,
                    width: G.screenWidth,
                    alignment: Alignment.center,
                    child: Text(
                      '退出登录',
                      style: TextStyle(color: hex('#13bb87'), fontSize: 15),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildCard({
    String title,
    int allNumber,
    int number = 0,
    String imgSrc,
    String text,
    @required Function onPress,
  }) {
    return GestureDetector(
      onTap: () => onPress(),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  '$title($allNumber)',
                  style: TextStyle(color: hex('#666'), fontSize: 13),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 10),
              height: 89,
              padding: EdgeInsets.symmetric(vertical: 12),
              decoration: BoxDecoration(
                  color: hex('#fff'),
                  borderRadius: BorderRadius.circular(3),
                  boxShadow: [
                    BoxShadow(
                        color: hex('#696969').withOpacity(.5), blurRadius: 1),
                  ]),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 35),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      '$imgSrc',
                      fit: BoxFit.cover,
                      width: 101.5,
                    ),

                    /// 右边
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: 32,
                          height: 32,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: hex('#13bb87'),
                              borderRadius: BorderRadius.circular(40)),
                          child: Text(
                            '${number == null ? 0 : number}',
                            style: TextStyle(
                                fontSize: 19,
                                color: hex('#fff'),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          child: Text(
                            '$text',
                            style: TextStyle(
                                color: hex('#13bb87'),
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
