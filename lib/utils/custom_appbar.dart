// import 'package:flutter/material.dart';
// import 'package:xdsapp/utils/global.dart';

// class XdsAppBar extends AppBar {
//   XdsAppBar({
//     Key key,
//     Widget leading,
//     bool automaticallyImplyLeading = true,
//     Widget title,
//     List<Widget> actions,
//     Widget flexibleSpace,
//     PreferredSizeWidget bottom,
//     double elevation,
//     ShapeBorder shape,
//     Color backgroundColor,
//     Brightness brightness,
//     IconThemeData iconTheme,
//     IconThemeData actionsIconTheme,
//     TextTheme textTheme,
//     bool primary = true,
//     bool centerTitle = true,
//     double titleSpacing = NavigationToolbar.kMiddleSpacing,
//     double toolbarOpacity = 1.0,
//     double bottomOpacity = 1.0,
//     this.leadingOnPressed
//   }) : super(
//     key: key,
//     leading: leading,
//     automaticallyImplyLeading: automaticallyImplyLeading,
//     title: title,
//     actions: actions,
//     flexibleSpace: flexibleSpace,
//     bottom: bottom,
//     elevation: elevation,
//     shape: shape,
//     backgroundColor: backgroundColor,
//     brightness: brightness,
//     iconTheme: iconTheme,
//     actionsIconTheme: actionsIconTheme,
//     textTheme: textTheme,
//     primary: primary,
//     centerTitle: centerTitle,
//     titleSpacing: titleSpacing,
//     toolbarOpacity: toolbarOpacity,
//     bottomOpacity: bottomOpacity,
//   );

//   final VoidCallback leadingOnPressed;

//   @override
//   Widget get leading => leadingFn();

//   Widget leadingFn() {
//     return IconButton(
//       icon: const Icon(
//         Icons.arrow_back,
//         size: 18.0,
//       ),
//       onPressed: () => leadingOnPressed == null ? G.pop() : leadingOnPressed(),
//     );
//   }
// }