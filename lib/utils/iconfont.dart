import 'package:flutter/material.dart';

Icon iconmiancengshigong({double size = 16.0, Color color}) => Icon(
  IconData(0xe6ee, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconjizhuangshigong({double size = 16.0, Color color}) => Icon(
  IconData(0xe6ef, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon icondianhua({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f0, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconjizhuangshigongzhunbei({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f1, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconanzhuang({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f2, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconjiaofu({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f3, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconxiugai({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f5, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon icontianjia({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f6, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconrili({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f7, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconyichang({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f8, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconjihuashijian({double size = 16.0, Color color}) => Icon(
  IconData(0xe6f9, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconjihuashijian1({double size = 16.0, Color color}) => Icon(
  IconData(0xe6fa, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconshaixuan({double size = 16.0, Color color}) => Icon(
  IconData(0xe6fb, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconshaixuan1({double size = 16.0, Color color}) => Icon(
  IconData(0xe6fc, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconfanhui({double size = 16.0, Color color}) => Icon(
  IconData(0xe6fd, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconicontest({double size = 16.0, Color color}) => Icon(
  IconData(0xe6fe, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon icontongzhi({double size = 16.0, Color color}) => Icon(
  IconData(0xe6ff, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconsousuo({double size = 16.0, Color color}) => Icon(
  IconData(0xe701, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconyanqi({double size = 16.0, Color color}) => Icon(
  IconData(0xe702, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconzantingcopy({double size = 16.0, Color color}) => Icon(
  IconData(0xe703, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

Icon iconfugong({double size = 16.0, Color color}) => Icon(
  IconData(0xe706, fontFamily: 'iconfont'),
  size: size,
  color: color,
);

