dealData(date) {
  if (date == null) {
    return "";
  }
  if (date == '0001-01-01T00:00:00') {
    return "";
  }
  if (date.contains('T')) {
    return date.split('T')[0];
  }
  return "";
}

dealtime(String time) {
  if (time == null || time == '0001-01-01T00:00:00') {
    return "";
  }
  final str = time.split('T')[0];
  final newTime = str.split('-');
  return '${newTime[0]}年${newTime[1]}月${newTime[2]}日';
}

dealtimeSlash(String time) {
  if (time == null || time == '0001-01-01T00:00:00') {
    return "";
  }
  if (time.contains('T')) {
    final str = time.split('T')[0];
    final newTime = str.replaceAll('-', "/");
    return "$newTime";
  }
  if (time.contains(' ')) {
    final str = time.split(' ')[0];
    final newTime = str.replaceAll('-', "/");
    return "$newTime";
  }
}

dealtimeSlashHourAndMinute(String time) {
  if (time == null || time == '0001-01-01T00:00:00') {
    return "";
  }
  if (time.contains('T')) {
    final index = time.lastIndexOf(":");
    final str = time.substring(0, index);
    final newTime = str.replaceAll('-', "/").replaceAll("T", " ");
    return "$newTime";
  }
}

nowDate() {
  DateTime now = DateTime.now();
  String nowString = now.toString();
  if (nowString.contains(".")) {
    nowString = nowString.split(".")[0];
  }
  nowString = nowString.replaceAll("-", "/");

  return nowString;
}

dealMonthAndDay(String time) {
  if (time == null || time == '0001-01-01T00:00:00') {
    return "";
  }
  final str = time.split(' ')[0];
  final newTime = str.split('/');
  String month = "";
  String day = "";
  if (newTime[0] != null) {
    month = newTime[0];
  }
  if (newTime[1] != null) {
    day = newTime[1];
  }
  return '$month/$day';
}
