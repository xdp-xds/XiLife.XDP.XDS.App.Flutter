import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:jpush_flutter/jpush_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:xdsapp/utils/user.dart';

class G {
  ///接口数据集合
  static Set uriSet = Set();

  /// loading 状态
  static bool loadStatus = false;

  /// 不能返回的路由
  static final _notGoBackRoute = ['/', '/login'];

  /// 全局key
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  /// 是否是ios
  static final bool isIOS = Platform.isIOS;

  /// 当前屏幕宽度
  static final double screenWidth =
      MediaQuery.of(getCurrentContext()).size.width;

  /// 当前屏幕高度
  static final double screenHeight =
      MediaQuery.of(getCurrentContext()).size.height;

  /// jpush
  static final JPush jpush = JPush();

  /// 获取当前的state
  static NavigatorState getCurrentState() => navigatorKey.currentState;

  /// 获取当前的context
  static BuildContext getCurrentContext() => navigatorKey.currentContext;

  /// 跳转页面使用 G.pushNamed
  static Future pushNamed(String routeName, {Object arguments}) {
    if (_notGoBackRoute.indexOf(routeName) > -1) {
      return getCurrentState()
          .pushNamedAndRemoveUntil(routeName, (route) => route == null);
    } else {
      return getCurrentState().pushNamed(routeName, arguments: arguments);
    }
  }

  /// 返回页面
  static void pop([Object result]) => getCurrentState().pop(result);

  /// 获取User相关信息
  static final User user = User();

  /// 拨打电话
  static Future callPhone(num phone) async {
    try {
      if (await canLaunch('tel:$phone')) {
        await launch('tel:$phone');
      } else {
        return 'not launch $phone';
      }
    } catch (e) {
      return e;
    }
  }

  /// 处理时间
  static String handleDate(String date) {
    if (date == '0001-01-01T00:00:00') return "";
    return date.split('T')[0].replaceAll('-', '/');
  }
}
