import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xdsapp/api/api_urls.dart';
import 'package:xdsapp/config.dart';

import 'dart:convert';

import 'package:xdsapp/utils/parse_jwt.dart';

class UserInfo {
  static UserInfo _instance;

  String token;
  String accessToken = 'accessToken';
  String refreshToken = 'refreshToken';
  String workInfo = 'workInfo';

  static getInstance() {
    if (_instance == null) {
      _instance = new UserInfo();
    }
    return _instance;
  }

  /// 存储token
  setToken(token, {refreshToken}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(accessToken, token);
    UserInfo.getInstance().token = token;
    if (refreshToken != null) {
      await prefs.setString(refreshToken, refreshToken);
      prefs.getString(refreshToken);
    }
  }

  getMemeryToken() {
    return UserInfo.getInstance().token;
  }

  /// 获取token
  getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString(accessToken);
    return token;
  }

  /// 解析token
  parseToeken() async {
    var token = await getToken();
    if (token != null) {
      return parseJwt(token);
    }
    return null;
  }

  clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  getFileRefreshToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var temp = prefs.getString(refreshToken);
    if (temp != null) {
      return temp;
    } else {
      throw {'message': 'no refreshToken'};
    }
  }

  refreshTokenMethod() async {
    Future(getFileRefreshToken())
        .then((refreshToken) => refreshHttp(refreshToken));
  }

  refreshHttp(refreshToken) async {
    Response response;
    var body = {'refreshToken': refreshToken, 'clientId': Config.clientId};
    try {
      response = await Dio().post(ApiUrls.refreshUrl, data: body);
    } on DioError catch (e) {
      if (e.message.contains('401')) {
        throw e;
      }
    }
    setToken(response.data['access_token'],
        refreshToken: response.data['refresh_token']);
  }

  /// 存储工人信息
  setWorkInfo(info) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String infoString = json.encode(info);
    await prefs.setString(workInfo, infoString);
    return;
  }

  /// 获取工人信息
  getWorkInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map info = prefs.getString(workInfo) != null
        ? json.decode(prefs.getString(workInfo))
        : null;
    return info;
  }
}
