import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:xdsapp/api/api_config.dart';
import 'package:xdsapp/api/api_urls.dart';
import 'package:xdsapp/config.dart';
import 'package:xdsapp/jsonserialize/user/data.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/parse_jwt.dart';

class User {
  String _accessToken;
  String _refreshToken;

  /// 解析之后的token
  UserInfoData _userInfo;
  WorkInfoData _workInfo;

  /// accessToken
  get accessToken => _accessToken;

  /// refreshToken
  get refreshToken => _refreshToken;

  /// 解析accessToken返回的信息
  UserInfoData get userInfo => _userInfo;

  /// 调用getUserId返回的信息
  WorkInfoData get workInfo => _workInfo;

  static User _singleton;

  User.singleton();

  factory User() {
    if (_singleton == null) {
      _singleton = User.singleton();
    }

    return _singleton;
  }

  /// 初始化user
  init() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _accessToken = prefs.getString('access_token');
    _refreshToken = prefs.getString('refresh_token');
    await parsingToken(_accessToken);
  }

  /// 设置token
  setToken({String accessToken, String refreshToken}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (accessToken != null) {
      _accessToken = accessToken;
      // 保存token到本地存储
      await prefs.setString('access_token', accessToken);
    }
    if (refreshToken != null) {
      _refreshToken = refreshToken;
      await prefs.setString('refresh_token', refreshToken);
    }
  }

  ///根据accessToken解析数据
  parsingToken(String token) async {
    if (token != null) {
      // 解析token
      Map _parseAccressToken = parseJwt(_accessToken);

      /// role转换成数组
      var role = _parseAccressToken['Role'];
      if (role is String) _parseAccressToken['Role'] = [role];
      _userInfo = UserInfoData.fromJson(_parseAccressToken);
      // 通过userId请求 workInfo
      try {
        Map<String, dynamic> res = await _getWorkInfo();
        _workInfo = WorkInfoData.fromJson(res);
      } catch (e) {}
    }
  }

  /// 根据解析的accessToken获取workInfo信息
  _getWorkInfo() async {
    String userId = _userInfo.userId;
    try {
      Dio dio = Dio();
      var res = await dio.get(
          '${ApiConfig.baseUrl}${ApiUrls.getUserId}/$userId',
          options: Options(
              headers: {'Authorization': 'Bearer ' + G.user.accessToken}));
      // var res = await Request.getUserId(userId);
      return res.data;
    } catch (e) {
      print(e);
      return null;
    }
  }

  /// 刷新token
  Future refreshTokenMethod() async {
    Map<String, dynamic> body = {
      'refreshToken': _refreshToken,
      'clientId': Config.clientId
    };

    Dio dio = Dio();
    dio.options.baseUrl = Config.baseUrl;
    try {
      Response response = await dio.post('/${ApiUrls.refreshUrl}', data: body);
      await setToken(
        accessToken: response.data['access_token'],
        refreshToken: response.data['refresh_token'],
      );
      return response;
    } on DioError catch (_) {
      // if (e.message.contains('401')) {
      await Fluttertoast.showToast(
          msg: "您当前的登录已失效，请重新登录",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 2,
          fontSize: 10.0);
      return G.pushNamed('/login');
      // throw e;
      // }
    }
  }

  clear() async {
    _userInfo = UserInfoData.fromJson({});
    _workInfo = WorkInfoData.fromJson({});

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('access_token');
    prefs.remove('refresh_token');
  }
}
