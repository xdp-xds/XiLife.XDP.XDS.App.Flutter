import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ShowUpdate {
  static int platform = Platform.isIOS ? 2 : 3;
  static String downloadPath = '';
  static OverlayEntry overlayTips;
  static OverlayEntry overlayProgress;
  static ValueNotifier<int> _progress = ValueNotifier<int>(0);
  final context;
  final String remarks;
  final String url;
  final int isForce;
  final String version;
  ReceivePort _port = ReceivePort();
  ShowUpdate(
    this.context, {
    this.remarks = '',
    this.url = '',
    this.isForce = 0,
    this.version = '',
  }) {
    _findLocalPath();
    _bindBackgroundIsolate();
    FlutterDownloader.registerCallback(downloadCallback);
  }

  static String getFileName(String path) {
    int pos = path.lastIndexOf('/');
    String name = path.substring(pos + 1);
    return name;
  }

  _openAppStore() {
    _launchURL();
  }

  _launchURL() async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  _downloadFile(downloadUrl, savePath) async {
    // downloadUrl = "http://192.168.16.190/xds/app-release1.apk";
    await FlutterDownloader.enqueue(
      url: downloadUrl,
      savedDir: savePath,
      showNotification: true,
      fileName: getFileName(downloadUrl),
      openFileFromNotification: true,
    );
    closeTips();
    var middleWidget = Progress();
    _showProgress(middleWidget);
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    print(
        'Background Isolate Callback: task ($id) is in status ($status) and process ($progress)');
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port');
    send.send([id, status, progress]);
  }

  void _bindBackgroundIsolate() {
    bool isSuccess = IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    if (!isSuccess) {
      _unbindBackgroundIsolate();
      _bindBackgroundIsolate();
      return;
    }
    _port.listen((dynamic data) {
      print('UI Isolate Callback: $data');
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      download(id, status, progress, url);
    });
  }

  void _unbindBackgroundIsolate() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
  }

  static void download(taskId, status, progress, url) {
    // url = "http://192.168.16.190/xds/app-release1.apk";
    _progress.value = progress;
    if (status == DownloadTaskStatus.complete) {
      IsolateNameServer.removePortNameMapping('downloader_send_port');
      closeProgress();
      OpenFile.open(downloadPath + '/' + getFileName(url));
    } else if (status == DownloadTaskStatus.failed) {
      IsolateNameServer.removePortNameMapping('downloader_send_port');
      Fluttertoast.showToast(msg: '下载失败', gravity: ToastGravity.CENTER);
      closeProgress();
    }
  }

  _showProgress(myWiddet) {
    overlayProgress = OverlayEntry(builder: (context) {
      return myWiddet;
    });
    Overlay.of(context).insert(overlayProgress);
  }

  _findLocalPath() async {
    if (platform == 3) {
      Directory directory = await getExternalStorageDirectory();
      if (directory == null) {
        Fluttertoast.showToast(msg: '没拿到位置');
      }
      downloadPath = directory == null ? '' : directory.path;
    }
    showUpdateTips();
  }

  void showUpdateTips() {
    overlayTips = OverlayEntry(builder: (context) {
      return Container(
        color: Color.fromRGBO(0, 0, 0, .6),
        child: Padding(
          padding: EdgeInsets.all(54.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 360,
                decoration: BoxDecoration(
                  color: Color(0xFFFFFFFF),
                  borderRadius: BorderRadius.all(
                    Radius.circular(5.0),
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    // Positioned(
                    //   top: 0,
                    //   right: 0,
                    //   child: Container(
                    //     height: 130,
                    //     child: Image.asset(
                    //       'assets/bg_gengxin.png',
                    //       fit: BoxFit.fitHeight,
                    //     ),
                    //   ),
                    // ),
                    Positioned(
                      top: 26.0,
                      left: 0,
                      child: Container(
                        padding: EdgeInsets.fromLTRB(22.0, 0, 22.0, 0),
                        width: MediaQuery.of(context).size.width - 100.0,
                        child: createUpdateList(),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      bottom: 0,
                      child: Container(
                        height: 56.0,
                        width: MediaQuery.of(context).size.width - 100.0,
                        decoration: BoxDecoration(
                          border: Border(
                            top: BorderSide(
                              color: Color(0xFFEAEAEA),
                              width: 1.0,
                            ),
                          ),
                        ),
                        child: Flex(
                          direction: Axis.horizontal,
                          children: <Widget>[
                            isForce == 0
                                ? Expanded(
                                    flex: 1,
                                    child: GestureDetector(
                                      onTap: () {
                                        closeTips();
                                      },
                                      child: Container(
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          border: Border(
                                            right: BorderSide(
                                              color: Color(0xFFEAEAEA),
                                              width: 1.0,
                                            ),
                                          ),
                                        ),
                                        child: Text(
                                          '稍后再说',
                                          style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500,
                                            decoration: TextDecoration.none,
                                            color: Color(0xFF666666),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                : Container(),
                            Expanded(
                              flex: 1,
                              child: GestureDetector(
                                onTap: () {
                                  // Navigator.of(context).pop();
                                  platform == 2
                                      ? _openAppStore()
                                      : _downloadFile(url, downloadPath);
                                },
                                child: Container(
                                  alignment: Alignment.center,
                                  child: Text(
                                    '立即更新',
                                    style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      decoration: TextDecoration.none,
                                      color: Color(0xFFFA6314),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    });
    try {
      Overlay.of(context).insert(overlayTips);
    } catch (e) {}
  }

  Widget createUpdateList() {
    List<Widget> list = [
      Container(
        alignment: Alignment.centerLeft,
        margin: EdgeInsets.only(bottom: 70.0),
        child: RichText(
          text: TextSpan(
            text: '版本更新',
            style: TextStyle(
              fontSize: 26,
              decoration: TextDecoration.none,
              color: Color(0xFF333333),
            ),
            children: <TextSpan>[
              TextSpan(
                text: '(v$version)',
                style: TextStyle(
                  fontSize: 12,
                  decoration: TextDecoration.none,
                  color: Color(0xFF333333),
                ),
              )
            ],
          ),
        ),
      ),
    ];
    list.add(
      Container(
        alignment: Alignment.bottomLeft,
        child: Text(
          remarks,
          style: TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w500,
            decoration: TextDecoration.none,
            color: Color(0xFF333333),
          ),
        ),
      ),
    );
    return Column(
      children: list,
    );
  }

  void closeTips() {
    overlayTips.remove();
  }

  static void closeProgress() {
    overlayProgress.remove();
  }
}

class Progress extends StatefulWidget {
  @override
  _ProgressState createState() => _ProgressState();
}

class _ProgressState extends State<Progress> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: ShowUpdate._progress,
      builder: (BuildContext context, int value, Widget child) {
        return Container(
          color: Color.fromRGBO(0, 0, 0, .6),
          child: Padding(
            padding: EdgeInsets.all(50.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Container(
                  height: 137.0,
                  padding: EdgeInsets.fromLTRB(20.0, 0, 20.0, 0),
                  decoration: BoxDecoration(
                    color: Color(0xFFFFFFFF),
                    borderRadius: BorderRadius.all(
                      Radius.circular(5.0),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(top: 25.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '正在下载新版本XDS…',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none,
                            color: Color(0xFF333333),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 4.0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          '已完成：$value%',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            decoration: TextDecoration.none,
                            color: Color(0xFF151515),
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(top: 21.0),
                        width: (MediaQuery.of(context).size.width - 140.0),
                        height: 22.0,
                        child: Stack(
                          children: <Widget>[
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Container(
                                width:
                                    (MediaQuery.of(context).size.width - 140.0),
                                height: 22.0,
                                decoration: BoxDecoration(
                                  color: Color(0xFFD2D2D2),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(11.0),
                                  ),
                                ),
                              ),
                            ),
                            Positioned(
                              left: 0,
                              top: 0,
                              child: Container(
                                width: (MediaQuery.of(context).size.width -
                                        140.0) *
                                    value /
                                    100,
                                height: 22.0,
                                decoration: BoxDecoration(
                                  color: Colors.yellow,
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(11.0),
                                  ),
                                  gradient: const LinearGradient(colors: [
                                    Color(0xFFF5A014),
                                    Color(0xFFFA3714),
                                  ]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
