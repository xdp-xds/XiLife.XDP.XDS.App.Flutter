import 'package:jpush_flutter/jpush_flutter.dart';

import 'global.dart';
import "dart:convert";

///初始化光推送设置
Future<void> initializeJPush() async {
  // 添加初始化方法
  G.jpush.setup(
    appKey: "d246e2afca10ffb6c79af6f9",
    channel: "theChannel",
    production: false,
    debug: false,
  );
  // 申请推送权限，注意这个方法只会向用户弹出一次推送权限请求（如果用户不同意，之后只能用户到设置页面里面勾选相应权限），需要开发者选择合适的时机调用。
  G.jpush.applyPushAuthority(
      new NotificationSettingsIOS(sound: true, alert: true, badge: true));
  // 添加事件监听方法。
  G.jpush.addEventHandler(
    // 接收通知回调方法。
    onReceiveNotification: (Map<String, dynamic> message) async {},
    // 点击通知回调方法。
    onOpenNotification: (Map<String, dynamic> message) async {
      if (G.isIOS == true) {
        try {
          var extra = message['extras'];
          Map<String, dynamic> msgJson =
              json.decode(extra['extras']['MsgParameter']);
          G.pushNamed('/notice_detail', arguments: {
            "messageId": msgJson['MessageId'],
            'refreshType': 'push',
            'msgType': msgJson['MessageType'],
            'parameter': msgJson,
          });
        } catch (e) {}
      } else {
        var extra = message['extras']['cn.jpush.android.EXTRA'];
        if (extra != null) {
          var extraJson = json.decode(extra);
          Map<String, dynamic> msgParameter =
              json.decode(extraJson['extras']['MsgParameter']);
          G.pushNamed('/notice_detail', arguments: {
            "messageId": msgParameter['MessageId'],
            'refreshType': 'push',
            // 'msgType': msgParameter['MessageType'],
            // 'parameter': msgParameter,
          });
        }
      }
    },
    // 接收自定义消息回调方法。
    onReceiveMessage: (Map<String, dynamic> message) async {},
  );
}
