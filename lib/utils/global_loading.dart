import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_loading/index.dart';
import 'package:xdsapp/utils/global.dart';

class GlobalLoading {
  static void before(uri) {
    if (G.user.workInfo?.id != null) {
      G.uriSet.add(uri);
      // 已有弹窗，则不再显示弹窗, dict.length >= 2 保证了有一个执行弹窗即可，
      if (G.loadStatus == false && G.uriSet.length == 1) {
        G.loadStatus = true;
        WidgetsBinding.instance.addPostFrameCallback((_) {
          XdsLoading.show();
        });
      }
    }
  }

  static void complete(uri) {
    if (G.user.workInfo?.id != null) {
      G.uriSet.remove(uri);
      // 所有接口接口返回并有弹窗
      if (G.uriSet.isEmpty && G.loadStatus == true) {
        G.loadStatus = false; // 修改状态
        //完成后关闭loading窗
        XdsLoading.hide();
      }
    }
  }
}
