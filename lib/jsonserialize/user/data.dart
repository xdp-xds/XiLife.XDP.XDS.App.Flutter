import 'package:json_annotation/json_annotation.dart';

part 'data.g.dart';

@JsonSerializable()
class UserData {
  UserInfoData userInfo;
  WorkInfoData workInfo;

  UserData({
    this.userInfo,
    this.workInfo,
  });

  //反序列化
  factory UserData.fromJson(Map<String, dynamic> json) =>
      _$UserDataFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}

@JsonSerializable()
class UserInfoData {
  int nbf;
  int exp;
  String iss;
  List<String> aud;
  String clientId;
  String sub;
  int authTime;
  String idp;
  @JsonKey(name: 'Mobile')
  String mobile;
  @JsonKey(name: 'UserName')
  String userName;
  @JsonKey(name: 'UserId')
  String userId;
  @JsonKey(name: 'StaffId')
  String staffId;
  @JsonKey(name: 'Role')
  List<String> role;
  List<String> scope;
  List<String> amr;

  UserInfoData({
    this.nbf,
    this.exp,
    this.iss,
    this.aud,
    this.clientId,
    this.sub,
    this.authTime,
    this.idp,
    this.mobile,
    this.userName,
    this.userId,
    this.staffId,
    this.role,
    this.scope,
    this.amr,
  });

  //反序列化
  factory UserInfoData.fromJson(Map<String, dynamic> json) =>
      _$UserInfoDataFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$UserInfoDataToJson(this);
}

@JsonSerializable()
class WorkInfoData {
  int type;
  String name;
  String phone;
  dynamic userId;
  dynamic deviceId;
  int status;
  int source;
  String createdTime;
  List<dynamic> userDeliveryStageMappings;
  dynamic childRelations;
  String id;

  WorkInfoData({
    this.type,
    this.name,
    this.phone,
    this.userId,
    this.deviceId,
    this.status,
    this.source,
    this.createdTime,
    this.userDeliveryStageMappings,
    this.childRelations,
    this.id,
  });

  //反序列化
  factory WorkInfoData.fromJson(Map<String, dynamic> json) =>
      _$WorkInfoDataFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$WorkInfoDataToJson(this);
}
