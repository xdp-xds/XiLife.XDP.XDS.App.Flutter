// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) {
  return UserData(
      userInfo: json['userInfo'] == null
          ? null
          : UserInfoData.fromJson(json['userInfo'] as Map<String, dynamic>),
      workInfo: json['workInfo'] == null
          ? null
          : WorkInfoData.fromJson(json['workInfo'] as Map<String, dynamic>));
}

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'userInfo': instance.userInfo,
      'workInfo': instance.workInfo
    };

UserInfoData _$UserInfoDataFromJson(Map<String, dynamic> json) {
  return UserInfoData(
      nbf: json['nbf'] as int,
      exp: json['exp'] as int,
      iss: json['iss'] as String,
      aud: (json['aud'] as List)?.map((e) => e as String)?.toList(),
      clientId: json['clientId'] as String,
      sub: json['sub'] as String,
      authTime: json['authTime'] as int,
      idp: json['idp'] as String,
      mobile: json['Mobile'] as String,
      userName: json['UserName'] as String,
      userId: json['UserId'] as String,
      staffId: json['StaffId'] as String,
      role: (json['Role'] as List)?.map((e) => e as String)?.toList(),
      scope: (json['scope'] as List)?.map((e) => e as String)?.toList(),
      amr: (json['amr'] as List)?.map((e) => e as String)?.toList());
}

Map<String, dynamic> _$UserInfoDataToJson(UserInfoData instance) =>
    <String, dynamic>{
      'nbf': instance.nbf,
      'exp': instance.exp,
      'iss': instance.iss,
      'aud': instance.aud,
      'clientId': instance.clientId,
      'sub': instance.sub,
      'authTime': instance.authTime,
      'idp': instance.idp,
      'Mobile': instance.mobile,
      'UserName': instance.userName,
      'UserId': instance.userId,
      'StaffId': instance.staffId,
      'Role': instance.role,
      'scope': instance.scope,
      'amr': instance.amr
    };

WorkInfoData _$WorkInfoDataFromJson(Map<String, dynamic> json) {
  return WorkInfoData(
      type: json['type'] as int,
      name: json['name'] as String,
      phone: json['phone'] as String,
      userId: json['userId'],
      deviceId: json['deviceId'],
      status: json['status'] as int,
      source: json['source'] as int,
      createdTime: json['createdTime'] as String,
      userDeliveryStageMappings: json['userDeliveryStageMappings'] as List,
      childRelations: json['childRelations'],
      id: json['id'] as String);
}

Map<String, dynamic> _$WorkInfoDataToJson(WorkInfoData instance) =>
    <String, dynamic>{
      'type': instance.type,
      'name': instance.name,
      'phone': instance.phone,
      'userId': instance.userId,
      'deviceId': instance.deviceId,
      'status': instance.status,
      'source': instance.source,
      'createdTime': instance.createdTime,
      'userDeliveryStageMappings': instance.userDeliveryStageMappings,
      'childRelations': instance.childRelations,
      'id': instance.id
    };
