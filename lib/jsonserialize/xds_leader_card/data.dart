import 'package:json_annotation/json_annotation.dart';

part 'data.g.dart';

@JsonSerializable()
class XdsLeaderCardData {
  /// 如果页面是分配工序负责人  workStation代表工序
  /// 可以理解成数据结构，忽略参数名字反应的含义
  WorkStation workStation;
  String workProcedure;
  Leader leader;
  String remark;
  bool isEdit;
  /// 是否开启验证
  bool verifyLeader;
  bool verifyRemark;
  /// 验证结果
  bool validation;
  /// 工站展开收起
  bool collapse;

  XdsLeaderCardData({
    this.workStation,
    this.workProcedure,
    this.leader,
    this.remark,
    this.isEdit,
    this.verifyLeader,
    this.verifyRemark,
    this.validation,
    this.collapse
  });

  //反序列化
  factory XdsLeaderCardData.fromJson(Map<String, dynamic> json) =>
      _$XdsLeaderCardDataFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$XdsLeaderCardDataToJson(this);
}

@JsonSerializable()
class Leader {
  DefaultValue defaultValue;
  List<DefaultValue> list;

  Leader({
    this.defaultValue,
    this.list,
  });

  //反序列化
  factory Leader.fromJson(Map<String, dynamic> json) => _$LeaderFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$LeaderToJson(this);
}

@JsonSerializable()
class DefaultValue {
  String id;
  String name;
  String phone;

  DefaultValue({
    this.id,
    this.name,
    this.phone,
  });

  //反序列化
  factory DefaultValue.fromJson(Map<String, dynamic> json) =>
      _$DefaultValueFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$DefaultValueToJson(this);
}

@JsonSerializable()
class WorkStation {
  String id;
  String name;

  WorkStation({
    this.id,
    this.name,
  });

  //反序列化
  factory WorkStation.fromJson(Map<String, dynamic> json) =>
      _$WorkStationFromJson(json);
  //序列化
  Map<String, dynamic> toJson() => _$WorkStationToJson(this);
}
