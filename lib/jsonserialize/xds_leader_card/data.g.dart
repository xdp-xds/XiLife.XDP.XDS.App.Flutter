// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

XdsLeaderCardData _$XdsLeaderCardDataFromJson(Map<String, dynamic> json) {
  return XdsLeaderCardData(
      workStation: json['workStation'] == null
          ? null
          : WorkStation.fromJson(json['workStation'] as Map<String, dynamic>),
      workProcedure: json['workProcedure'] as String,
      leader: json['leader'] == null
          ? null
          : Leader.fromJson(json['leader'] as Map<String, dynamic>),
      remark: json['remark'] as String,
      isEdit: json['isEdit'] as bool,
      verifyLeader: json['verifyLeader'] as bool,
      verifyRemark: json['verifyRemark'] as bool,
      validation: json['validation'] as bool,
      collapse: json['collapse'] as bool);
}

Map<String, dynamic> _$XdsLeaderCardDataToJson(XdsLeaderCardData instance) =>
    <String, dynamic>{
      'workStation': instance.workStation,
      'workProcedure': instance.workProcedure,
      'leader': instance.leader,
      'remark': instance.remark,
      'isEdit': instance.isEdit,
      'verifyLeader': instance.verifyLeader,
      'verifyRemark': instance.verifyRemark,
      'validation': instance.validation,
      'collapse': instance.collapse
    };

Leader _$LeaderFromJson(Map<String, dynamic> json) {
  return Leader(
      defaultValue: json['defaultValue'] == null
          ? null
          : DefaultValue.fromJson(json['defaultValue'] as Map<String, dynamic>),
      list: (json['list'] as List)
          ?.map((e) => e == null
              ? null
              : DefaultValue.fromJson(e as Map<String, dynamic>))
          ?.toList());
}

Map<String, dynamic> _$LeaderToJson(Leader instance) => <String, dynamic>{
      'defaultValue': instance.defaultValue,
      'list': instance.list
    };

DefaultValue _$DefaultValueFromJson(Map<String, dynamic> json) {
  return DefaultValue(
      id: json['id'] as String,
      name: json['name'] as String,
      phone: json['phone'] as String);
}

Map<String, dynamic> _$DefaultValueToJson(DefaultValue instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'phone': instance.phone
    };

WorkStation _$WorkStationFromJson(Map<String, dynamic> json) {
  return WorkStation(id: json['id'] as String, name: json['name'] as String);
}

Map<String, dynamic> _$WorkStationToJson(WorkStation instance) =>
    <String, dynamic>{'id': instance.id, 'name': instance.name};
