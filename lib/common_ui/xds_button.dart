import 'package:flutter/material.dart';

void defaultFuncation() {}

class XdsButton extends StatelessWidget {
  final String title;
  final Function onTap;
  final bool isActive;
  final double width;
  final double height;
  final double textSize;
  final Color textColor;
  final Color enableColor;
  final Color disableColor;
  final Color gradientStartColor;
  final Color gradientEndColor;
  final BoxBorder border;
  final double borderRadius;

  /// button
  /// 
  /// ``` dart
  /// @param title - 按钮文字
  /// @param onTap - 点击事件
  /// @param isActive - 是否可以点击
  /// @param width - 宽度
  /// @param height - 高度
  /// @param textSize - 文字大小
  /// @param textColor - 文字颜色
  /// @param enableColor - 启用背景颜色
  /// @param disableColor - 禁用背景颜色
  /// @param gradientStartColor - 线性渐变开始颜色
  /// @param gradientEndColor - 线性渐变结束颜色
  /// @param border - 边框
  /// @param borderRadius - 圆角
  /// ```
  XdsButton({
    this.title,
    this.onTap,
    this.isActive = true,
    this.width,
    this.height = 44,
    this.textColor = Colors.white,
    this.textSize = 16,
    this.enableColor,
    this.disableColor = const Color(0xffcdcdcd),
    this.gradientStartColor = const Color(0xff2BD890),
    this.gradientEndColor = const Color(0xff09C6B7),
    this.border,
    this.borderRadius = 3
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: width,
        height: height,
        child: Center(
          child: Text(
            title,
            style: TextStyle(color: textColor, fontSize: textSize),
          ),
        ),
        decoration: BoxDecoration(
            color: isActive ? enableColor : disableColor,
            border: border,
            gradient: enableColor == null
                ? LinearGradient(
                    colors: isActive
                        ? [gradientStartColor, gradientEndColor]
                        : [disableColor, disableColor],
                  )
                : null,
            borderRadius: BorderRadius.circular(borderRadius)),
      ),
      onTap: onTap,
    );
  }
}
