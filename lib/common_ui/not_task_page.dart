import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';

class NotTaskPage extends StatelessWidget {
  const NotTaskPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        color: hex('#f4f4f4'),
        alignment: Alignment.center,
        padding: EdgeInsets.only(bottom: 88),
        child: Image.asset('lib/assets/images/not_task.png', 
          fit: BoxFit.cover,
          width: 285,
          height: 237,
        ),
      ),
    );
  }
}