import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class XdsLoading {
  static dynamic ctx;
  static OverlayEntry overlayEntry;

  static void show() {
    OverlayState overlayState = Overlay.of(ctx);
    overlayEntry = OverlayEntry(
      builder: (ctx) => CupertinoActivityIndicator(
        radius: 24,
      ),
    );
    overlayState.insert(overlayEntry);
  }

  static void hide() {
    overlayEntry.remove();
  }
}
