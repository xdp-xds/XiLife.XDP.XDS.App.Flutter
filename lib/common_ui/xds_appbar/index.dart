import 'package:flutter/material.dart';
import 'package:xdsapp/utils/global.dart';

class XdsAppBar extends AppBar {
  XdsAppBar(
      {Key key,
      bool automaticallyImplyLeading = true,
      Widget title,
      List<Widget> actions,
      Widget flexibleSpace,
      PreferredSizeWidget bottom,
      double elevation,
      ShapeBorder shape,
      Color backgroundColor,
      Brightness brightness,
      IconThemeData iconTheme,
      IconThemeData actionsIconTheme,
      TextTheme textTheme,
      bool primary = true,
      bool centerTitle = true,
      double titleSpacing = NavigationToolbar.kMiddleSpacing,
      double toolbarOpacity = 1.0,
      double bottomOpacity = 1.0,
      this.leadingOnPressed,
      this.newLeading})
      : super(
          key: key,
          automaticallyImplyLeading: automaticallyImplyLeading,
          title: title,
          actions: actions,
          flexibleSpace: flexibleSpace,
          bottom: bottom,
          elevation: elevation,
          shape: shape,
          backgroundColor: backgroundColor,
          brightness: brightness,
          iconTheme: iconTheme,
          actionsIconTheme: actionsIconTheme,
          textTheme: textTheme,
          primary: primary,
          centerTitle: centerTitle,
          titleSpacing: titleSpacing,
          toolbarOpacity: toolbarOpacity,
          bottomOpacity: bottomOpacity,
        );

  /// 点击leading回调
  final VoidCallback leadingOnPressed;

  /// 相当于原生appbar leading
  /// null || true => 显示m默认icon
  /// false => 不显示不占位置（相当于leading == null）
  /// Widget => 显示Widget
  final dynamic newLeading;

  /// 重构leading
  @override
  Widget get leading => leadingFn();

  Widget leadingFn() {
    if (newLeading is Widget || newLeading is bool || newLeading == null) {
      if (newLeading == null || newLeading == true) {
        return IconButton(
          icon: const Icon(
            Icons.arrow_back,
            size: 18.0,
          ),
          onPressed: () =>
              leadingOnPressed == null ? G.pop() : leadingOnPressed(),
        );
      }

      return newLeading == false ? null : newLeading;
    } else {
      throw ('newLeading is Widget, bool');
    }
  }
}
