/*
 * @Author: meetqy
 * @since: 2019-10-14 09:36:27
 * @lastTime: 2019-10-14 10:38:41
 * @LastEditors: meetqy
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:xdsapp/common_ui/xds_pull_to_refresh/index.dart';

class ExampleXdsPullToRefresh extends StatefulWidget {
  ExampleXdsPullToRefresh({Key key}) : super(key: key);

  _TestDemoState createState() => _TestDemoState();
}

class _TestDemoState extends State<ExampleXdsPullToRefresh> {
  static String text = "pull to refresh demo";
  List<String> items = [text, text,text,text,text,text,text,text];
  RefreshController _refreshController = RefreshController(initialRefresh: false);

  void _onRefresh() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));

    if(mounted) {
      _refreshController.loadComplete();
      setState(() {
        items = [text, text,text,text,text,text,text,text];
      });
    }
    // if failed,use refreshFailed()
    return _refreshController.refreshCompleted();
  }

  void _onLoading() async{
    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));
    if(mounted) {
      // if failed,use loadFailed(),if no data return,use LoadNodata()
      if(items.length >= 10) return _refreshController.loadNoData();
      setState(() {
        items = items + [text, text, text];
      });
    }

    
    _refreshController.loadComplete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('pull to refresh'),
      ),
      body: SafeArea(
        child: XdsPullToRefresh(
          controller: _refreshController,
          onRefresh: _onRefresh,
          onLoading: _onLoading,
          child: ListView.builder(
            itemBuilder: (c, i) => Card(child: Center(child: Text(items[i]))),
            itemExtent: 100.0,
            itemCount: items.length,
          ),
        ),
      ),
    );
  }
}

