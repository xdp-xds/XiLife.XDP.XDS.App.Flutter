/*
 * @Author: meetqy
 * @since: 2019-10-14 09:49:46
 * @lastTime: 2019-11-27 10:58:34
 * @LastEditors: meetqy
 */
import 'package:color_dart/HexColor.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class XdsPullToRefresh extends StatefulWidget {
  final RefreshController controller;
  final Function onRefresh;
  final Function onLoading;
  final Widget child;
  final bool enablePullDown;
  final bool enablePullUp;
  final List data;

  /// 下拉刷新，上拉加载
  ///
  /// ``` dart
  /// @param controller - 控制器
  /// @param onLoading - 上拉加载事件
  /// @param onRefresh - 下拉刷新事件
  /// @param child - 内容
  /// @param enablePullDown - 开启下拉刷新（默认开启）
  /// @param enablePullUp - 开启上拉加载（默认开启）
  /// ```
  XdsPullToRefresh(
      {Key key,
      this.controller,
      this.onLoading,
      this.onRefresh,
      this.child,
      this.enablePullDown = true,
      this.enablePullUp = true,
      this.data})
      : super(key: key);

  _PullToRefreshState createState() => _PullToRefreshState();
}

class _PullToRefreshState extends State<XdsPullToRefresh> {
  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      enablePullDown: widget.enablePullDown,
      enablePullUp: widget.enablePullUp,
      header: header(),
      footer: footer(),
      controller: widget.controller,
      onRefresh: widget.onRefresh,
      onLoading: widget.onLoading,
      child: widget.data != null && widget.data.length == 0
          ? Center(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 100),
                child: Image.asset(
                  'lib/assets/images/worker.png',
                  width: 285,
                  height: 240,
                ),
              ),
            )
          : widget.child,
    );
  }

  /// 顶部样式
  header() {
    return CustomHeader(
      builder: (BuildContext context, RefreshStatus mode) {
        return Container(
          height: 44.0,
          child: Center(child: CupertinoActivityIndicator()),
        );
      },
    );
  }

  /// 底部样式
  footer() {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget body;
        if (mode == LoadStatus.noMore) {
          body = Text(
            '--- 到底了 ---',
            style: TextStyle(color: hex('#999')),
          );
        } else {
          body = CupertinoActivityIndicator();
        }
        return Container(
          height: 44.0,
          child: Center(child: body),
        );
      },
    );
  }
}
