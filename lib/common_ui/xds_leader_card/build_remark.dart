import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';

class BuildRemark extends StatefulWidget {
  BuildRemark( this.data, {
    Key key,
    @required this.onRemarkChange,
    this.disabled = false
  }) : super(key: key);

  final XdsLeaderCardData data;
  final Function(String) onRemarkChange;
  final bool disabled;

  @override
  _BuildRemarkState createState() => _BuildRemarkState();
}

class _BuildRemarkState extends State<BuildRemark> {
  /// remark 为 null显示添加备注按钮
  /// ''显示填写备注输入框
  TextEditingController _controller;
  XdsLeaderCardData data;


  @override
  void initState() {
    super.initState();
    data = widget.data;

    _controller = TextEditingController(text: data.remark);
  }

 @override
  Widget build(BuildContext context) {
    return data.isEdit ? edit() : normal();
  }

  /// 验证备注是否为空
  bool verify(String remark) {
    if(data.validation) return false;

    if(data.verifyRemark) {
      return remark == null || remark.isEmpty ? true : false;
    } else {
      return false;
    }
  }

  /// 备注
  Container edit() {
    bool _verify = verify(data.remark);
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: Container(
              child: data.remark == null ? Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Icon(Icons.add_circle_outline, color: widget.disabled ? hex('#999') : hex('#13BB87'), size: 18,),
                  Container(
                    margin: EdgeInsets.only(left: 5),
                    child: Text('添加备注', 
                      style: TextStyle(color: widget.disabled ? hex('#999') : hex('#13BB87'), fontWeight: FontWeight.bold, fontSize: 14),
                    ),
                  )
                ],
              ) : null,
            ),
            onTap: () {
              if(widget.disabled) return;
              data.remark = '';
              widget.onRemarkChange(data.remark);
            },
          ),

          Container(
            child: data.remark != null ? Column(
              children: <Widget>[
                Container(
                  alignment: Alignment.topLeft,
                  margin: EdgeInsets.only(bottom: 10),
                  child: Text('备注: ', style: TextStyle(
                    color: hex('#333'),
                    fontSize: 15
                  ),),
                ),

                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: hex('${_verify ? '#F61E1E' : '#d8d8d8'}')),
                    borderRadius: BorderRadius.circular(4)
                  ),
                  child: TextField(
                    controller: _controller,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      contentPadding: EdgeInsets.all(10),
                      hintText: '请填写备注',
                      counterText: ''
                    ),
                    maxLength: 100,
                    maxLines: 3,
                    onChanged: (String val) {
                      data.remark = val == '' ? null : val;
                      widget.onRemarkChange(data.remark);
                    },
                  ),
                )
              ],
            ) : null,
          ),
        
          Container(
            child: _verify ? Container(
              margin: EdgeInsets.only(top: 10),
              alignment: Alignment.centerLeft,
              child: Text('* 备注不能为空' ,
                style: TextStyle(
                  color: hex('#F61E1E'),
                ),
              ),
            ) : null,
          ),
        ],
      ),
    );
  }

  /// 备注
  Container normal() {
    return data.remark == null || data.remark.isEmpty ? Container() : Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text('备注：${data.remark}', style: TextStyle(
              color: hex('#333'),
              fontSize: 15,
              height: 1.75
            ),),
          ),
        ],
      ),
    );
  }
}