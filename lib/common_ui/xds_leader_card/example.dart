import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_button.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';
import 'package:xdsapp/common_ui/xds_leader_card/card_type.dart';
import 'package:xdsapp/common_ui/xds_leader_card/index.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';
import 'package:xdsapp/utils/global.dart';

class ExampleXdsLeaderCard extends StatefulWidget {
  ExampleXdsLeaderCard({Key key}) : super(key: key);

  @override
  _ExampleXdsLeaderCardState createState() => _ExampleXdsLeaderCardState();
}

class _ExampleXdsLeaderCardState extends State<ExampleXdsLeaderCard> {
  XdsLeaderCardData xdsLeaderCardData = XdsLeaderCardData.fromJson(data);

  bool collapse = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('xds_leader_card'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            color: hex('#fff'),
            child: XdsCollapse(
              title: buildTitle(
                data: xdsLeaderCardData,
                type: CardType.workStation
              ),
              value: collapse,
              onChange: (bool val) {
                setState(() {
                  collapse = val;
                });
              },
              body: XdsLeaderCard(
                isEdit: true,
                data: xdsLeaderCardData,
                bottomBorder: false,
                onLeaderPress: (XdsLeaderCardData xdsLeaderCardData, json) {
                  G.pushNamed('/choose_worker', arguments: {
                    "id": G.user.workInfo.id,
                  }).then((data) {
                    Map<String, dynamic> person = {
                      "id": data['id'],
                      "name": data['name'],
                      "phone": data['phone'],
                    };
                    setState(() {
                      xdsLeaderCardData.leader.defaultValue =
                          DefaultValue.fromJson(person);
                    });
                  });
                },
                onChange: (data, json) {
                  setState(() {
                    xdsLeaderCardData = data;
                  });
                },
              ),
            ),
          ),

          XdsButton(
            title: '确认',
            onTap: () {
              DefaultValue defaultValue = xdsLeaderCardData.leader.defaultValue;
              if (defaultValue == null || defaultValue.id == '0') {
                setState(() {
                  xdsLeaderCardData.verifyLeader = true;
                });
              }
            },
          )
        ],
      ),
    );
  }
}

const Map<String, dynamic> data = {
  "workStation": {"id": "000001", "name": "施工准备"},
  "workProcedure":
      "1A 打拆图 ，1 成品保护 ， 2A 打拆反馈 ， 2 打拆 ，1A 打拆图 ，1 成品保护 ， 2A 打拆反馈 ， 2 打拆",
  "leader": {
    // "defaultValue": {
    //   "id": "0001",
    //   "name": "AAAA",
    //   "phone": "18728321372"
    // },

    "defaultValue": null,

    "list": [
      {"id": "0001", "name": "AAAA", "phone": "18728321372"},
      {"id": "0002", "name": "BBBB", "phone": "18728321372"}
    ]
  },
  "remark": "",
  "isEdit": false,
  "verifyLeader": false,
  "verifyRemark": false,
  "validation": true
};
