import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';
import 'package:xdsapp/utils/global.dart';
import 'package:xdsapp/utils/iconfont.dart';


/// 负责人
/// 
/// ``` dart
/// @params data 数据
/// @params onPickerChange Picker值发生变化的回调 返回当前选中的负责人
/// ```
class BuildLeader extends StatelessWidget {
  const BuildLeader(this.data,{
    @required this.onPickerChange,
    @required this.onPress,
    Key key,
  }) : super(key: key);

  /// 数据
  final XdsLeaderCardData data;

  /// Picker值发生变化的回调 返回当前选中的负责人
  final Function(DefaultValue) onPickerChange;
  
  /// 单击负责人
  final Function(bool) onPress;

  /// 默认defaultValue
  static DefaultValue notLeader = DefaultValue.fromJson({
    "id": "0",
    "name": '请选择',
    "phone": ''
  });

  /// 选中的索引，用于回显
  static int initValueIndex = 0;

  @override
  Widget build(BuildContext context) {
    bool isEdit = data.isEdit;

    return isEdit ? edit(context) : normal();
  }

  showPickerModal(BuildContext context) {
    List<DefaultValue> leaderData = data.leader.list;

    if(leaderData.isEmpty) return ;


    if(leaderData[0].id != '0') {
      leaderData.add(notLeader);
      leaderData = leaderData.reversed.toList();
      data.leader.list = leaderData;
    }

    if(data.leader.defaultValue != null) {
      for(int i =0,len = leaderData.length; i<len;i++) {
        DefaultValue val = leaderData[i];
        if(val.id == data.leader.defaultValue.id) {
          initValueIndex = i;
          break;
        }
      }
    }


    Picker(
      selecteds: [initValueIndex],
      height: 264,
      itemExtent: 44,
      cancelText: '取消',
      confirmText: '确认',
      cancelTextStyle: TextStyle(color: hex('#1989fa')),
      confirmTextStyle: TextStyle(color: hex('#1989fa')),
      adapter: PickerDataAdapter<String>(pickerdata: 
        leaderData.map((val) {
          if(val.id == '0') {
            return '${val.name}';
          }
          
          return '${val.name} / ${val.phone}';
        }).toList()
      ),
      changeToFirst: false,
      hideHeader: false,
      onConfirm: (Picker picker, List value) {
        DefaultValue _defaultValue = data.leader.list[value[0]];
        onPickerChange(_defaultValue);
      }
    ).showModal(context);
  }

  /// 验证负责人是否为空
  /// 负责人为空 true
  bool verify(DefaultValue leaderDefalut) {
    if(data.verifyLeader || leaderDefalut?.id == '0') {
      return leaderDefalut == null ? true : false;
    } else {
      return false;
    }
  }

  /// 负责人
  Container edit(BuildContext context) {
    DefaultValue leaderDefalut = data.leader.defaultValue;

    String text;

    if(leaderDefalut == null || leaderDefalut.id == '0') {
      text = '请选择';
    } else {
      text = '${leaderDefalut.name} / ${leaderDefalut.phone}';
    }

    if(text.length > 15) text = text.substring(0, 15) + '...';

    bool _verify = verify(leaderDefalut);

    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Text('负责人：', style: TextStyle(
                color: hex('#333'),
                fontSize: 15
              ),),

              Expanded(
                child: Container(
                  height: 44,
                  decoration: BoxDecoration(
                    border: Border.all(color: hex('${_verify ? '#F61E1E' : '#d8d8d8'}')),
                    borderRadius: BorderRadius.circular(6)
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // 选择框
                      Expanded(
                        child: GestureDetector(
                          child: Container(
                            color: rgba(0,0,0,0),
                            padding: const EdgeInsets.only(left: 10, right: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('$text', style: TextStyle(
                                  color: hex('#111'),
                                  fontSize: 15
                                ),),

                                Transform.rotate(
                                  angle: -3.14*.5,
                                  child: Icon(Icons.expand_more, color: hex('#999'), size: 28),
                                )
                              ],
                            ),
                          ),
                          onTap: () => onPress(_verify),
                        ),
                      ),

                      // 打电话
                      GestureDetector(
                        child: Container(
                          width: 60,
                          height: 44,
                          decoration: BoxDecoration(
                            border: Border(left: BorderSide(color: hex('#d8d8d8')))
                          ),
                          
                          child: icondianhua(color: hex('#13BB87'), size: 18)
                        ),
                        onTap: () {
                          if(leaderDefalut != null && leaderDefalut.phone != null) {
                            launch('tel:${leaderDefalut.phone}');
                          }
                        }
                      ),
                    ],
                  ),
                ),
              ),
                  
            ],
          ),

          /// 负责人判断为空
          Opacity(
            opacity: _verify ? 1 : 0,
            child: Container(
              margin: const EdgeInsets.only(top: 10),
              child: Row(
                children: <Widget>[
                  Opacity(
                    opacity: 0,
                    child: Text('负责人：', style: TextStyle(
                      color: hex('#333'),
                      fontSize: 15
                    ),),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text('* 负责人不能为空' ,
                      style: TextStyle(
                        color: hex('#F61E1E'),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  /// 正常展示
  Container normal() {
    DefaultValue leaderDefalut = data.leader.defaultValue;

    String text = leaderDefalut == null ? '' : '${leaderDefalut.name} / ${leaderDefalut.phone}';

    if(text.length > 17) text = text.substring(0, 17) + '...';
    return Container(
      child: Row(
        children: <Widget>[
          Text('负责人：', style: TextStyle(
            color: hex('#333'),
            fontSize: 15
          ),),

          text.isEmpty ? Container() : Text('$text', style: TextStyle(
            color: hex('#111'),
            fontSize: 15
          ),),

          text.isEmpty ? Container() : GestureDetector(
            child: Container(
              margin: const EdgeInsets.only(left: 15, bottom: 4),
              child: icondianhua(color: hex('#13BB87'), size: 18)
            ),
            onTap: () async{
              if(leaderDefalut.phone == null) return;
              await G.callPhone(num.parse(leaderDefalut.phone));
            },
          )
        ],
      ),
    );
  }
}