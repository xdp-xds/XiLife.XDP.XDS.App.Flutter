import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_leader_card/build_leader.dart';
import 'package:xdsapp/common_ui/xds_leader_card/build_remark.dart';
import 'package:xdsapp/common_ui/xds_leader_card/card_type.dart';
import 'package:xdsapp/jsonserialize/xds_leader_card/data.dart';

typedef XdsLeaderCardCallback = void Function(
    XdsLeaderCardData data, Map json);

class XdsLeaderCard extends StatelessWidget {

  /// 负责人
  /// 
  /// ```dart 
  /// @params data 数据
  /// @params bottomBorder 底部border
  /// @params isEdit 是否为修改状态 该值如果不为null，可以覆盖data中的isEdit
  /// @params type 渲染页面类型 默认：工站
  /// @params onLeaderPress 点击负责人回调
  /// @params onChange 数据改变时回调 
  /// args1: XdsLeaderCardData Object类型
  /// args2: data map类型
  /// ```
  /// 
  /// 备注：
  /// 取消下拉选择负责人，修改为跳转页面
  /// 修改之后通过修改 data.leader.defaultValue 显示不同的负责人，其他参数不变
  XdsLeaderCard({
    Key key,
    @required this.data, 
    @required this.onChange,
    @required this.onLeaderPress,
    this.isEdit,
    this.type = CardType.workStation,
    this.bottomBorder = true,
    this.disabledAddRemark = false,
    this.onPressTitle
  }) : super(key: key){
    if(isEdit != null) data.isEdit = isEdit;
  }
  /// 数据
  /// bool data.verifyRemark 是否开启备注验证
  /// bool data.verifyLeader 是否开启负责人验证
  final XdsLeaderCardData data;


  /// 底部border =》 控制top border
  final bool bottomBorder;
  
  /// 数据改变时回调
  /// args1: XdsLeaderCardData Object类型
  /// args2: data map类型
  final XdsLeaderCardCallback onChange;
  
  /// 是否为修改状态 该值如果不为null，可以覆盖data中的isEdit
  final bool isEdit;

  /// 点击负责人
  final XdsLeaderCardCallback onLeaderPress;

  /// 渲染页面类型
  final CardType type;

  /// 是否禁用添加备注按钮
  final bool disabledAddRemark;

  /// 点击标题
  final XdsLeaderCardCallback onPressTitle;

  @override
  Widget build(BuildContext context) { 
    return Column(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(bottom: 20),
          child: Column(
            children: <Widget>[
              // 工序
              data.workProcedure == null || data.workProcedure.isEmpty ? Container(
                margin: EdgeInsets.only(bottom: 20),
              ) : Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(bottom: 20),
                child: Text('${data.workProcedure.replaceAll(RegExp("\\s|\\n"), '')}',style: TextStyle(
                  color: hex('#999'),
                  fontSize: 14
                ),)
              ),

              // 负责人
              BuildLeader(data,
                onPress: (bool verify) {
                  if(data.verifyLeader) {
                    data.validation = verify;
                  }
                  onLeaderPress(data, data.toJson());
                },
                onPickerChange: (DefaultValue defaultValue) {
                  data.leader.defaultValue = defaultValue;
                  onChange(data, data.toJson());
                },
              ),
              
              // 备注
              BuildRemark(data, 
                disabled: disabledAddRemark,
                onRemarkChange: (String remark) {
                  data.remark = remark;
                  if(data.verifyRemark) {
                    data.validation = remark == null || remark.isEmpty ? false : true;
                  }
                  onChange(data, data.toJson());
                },
              ),
            ],
          ),
        ),
        bottomBorder ? border() : Container()
      ],
    );
  }

  /// border
  border() {
    switch(type) {
      case CardType.workProcedure: 
        return Container(
          height: 1,
          child: Image.asset('./lib/assets/images/dashed.png'),
        );
      case CardType.workStation: 
        return Container(height: 1, color: hex('#d8d8d8'),);
      default: 
        return Container(); 
    }
  }
}

  /// 左侧图标
  Widget _buildTitleLeft(CardType type) {
    switch(type) {
      case CardType.workProcedure: 
        return Icon(Icons.star, size: 16,);
      case CardType.workStation: 
        return Icon(Icons.fiber_manual_record, size: 20, color: hex('#13bb87'),);
      default: 
        return null; 
    }
  }

  /// 标题
  Row buildTitle({
    XdsLeaderCardData data,
    CardType type,
    FontWeight fontWeight = FontWeight.normal
  }) {
    String desc = '';
    // FontWeight fontWeight = FontWeight.bold;
    double marginRight = 5;
    if(type == CardType.workProcedure) {
      desc = '工序：';
      fontWeight = FontWeight.normal;
      marginRight = 0;
    }
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(right: marginRight),
          child: _buildTitleLeft(type)
        ),

        Text('$desc${data.workStation.name}', style: TextStyle(
          color: hex('#111'),
          fontSize: 15,
          fontWeight: fontWeight
        ),)
      ],
    );
  }