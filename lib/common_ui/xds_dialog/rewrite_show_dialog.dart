/*
 * @Author: meetqy
 * @since: 2019-10-18 10:43:08
 * @lastTime: 2019-10-18 10:43:08
 * @LastEditors: meetqy
 */
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class RewriteShowDialog {
  final BuildContext context;

  RewriteShowDialog(this.context);

  /// 重写showDialog
  /// 系统自带的背景背景透明不能修改
  _showDialog({
    Widget Function(BuildContext) builder,
    Widget child,
    bool barrierDismissible,
    Color barrierColor
  }) {
    showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext buildContext, Animation<double> animation, Animation<double> secondaryAnimation) {
        final Widget pageChild = child ?? Builder(builder: builder);
        return SafeArea(
          child: Builder(
            builder: (BuildContext context) {
              return pageChild;
            }
          ),
        );
      },
      barrierDismissible: barrierDismissible,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: barrierColor,
      transitionDuration: const Duration(milliseconds: 150),
      transitionBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
        return FadeTransition(
          opacity: CurvedAnimation(
            parent: animation,
            curve: Curves.easeOut,
          ),
          child: child,
        );
      },
    );
  }

  /// 初始化
  void init({
    Widget child,
    bool barrierDismissible = false,
    Color barrierColor = Colors.black54,
    double elevation = 0
  }) {
    _showDialog(
      barrierDismissible: barrierDismissible,
      barrierColor: barrierColor,
      builder: (BuildContext context) {
        /// AnimatedPadding代替dialog 
        /// dialog没有提供修改padding的属性，本身也是调用的AnimatedPadding
        return AnimatedPadding(
          padding: EdgeInsets.symmetric(horizontal: 32, vertical: 24),
          duration: Duration(milliseconds: 100),
          curve: Curves.decelerate,
          child: MediaQuery.removeViewInsets(
            removeLeft: true,
            removeTop: true,
            removeRight: true,
            removeBottom: true,
            context: context,
            child: Center(
              child: Material(
                color: Colors.transparent,
                elevation: elevation,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(2.0))),
                type: MaterialType.card,
                child: child
              ),
            ),
          ),
        );
      }
    );
  }
}