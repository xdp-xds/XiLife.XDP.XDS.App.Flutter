import 'package:color_dart/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_dialog/rewrite_show_dialog.dart';
import 'package:xdsapp/utils/global.dart';

class Confirm extends RewriteShowDialog {
  Confirm(BuildContext context) : super(context);

  /// 显示
  /// ``` dart
  /// @param icon - 图标
  /// @param title - 标题
  /// @param subtitle - 副标题
  /// @param onOk - 确认按钮回调
  /// @param title - 标题
  /// @param subtitle - 副标题
  /// @param onCancel - 取消按钮回调
  /// ```
  void show({
    Widget icon,
    String title = '确认退出当前帐号？',
    String subtitle = '退出将重新登录',
    Function onOk,
    Function onCancel,
  }) {
    init(
      child: content(
        icon: icon ??
            Image.asset(
              './lib/assets/images/tip.png',
              width: 55,
            ),
        onOk: onOk,
        title: title,
        subtitle: subtitle,
        onCancel: onCancel,
      ),
    );
  }

  // 中间内容
  /// ``` dart
  /// @param icon - 图标
  /// @param title - 标题
  /// @param subtitle - 副标题
  /// @param onOk - 确认按钮回调
  /// @param onCancel - 取消按钮回调
  /// ```
  content({
    Widget icon,
    String title,
    String subtitle,
    Function onOk,
    Function onCancel,
  }) {
    return Center(
      child: Container(
        width: 330,
        padding: EdgeInsets.only(top: 45),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          color: hex('#fff'),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            icon,
            Padding(
              padding: EdgeInsets.only(top: 15),
              child: Text(
                title,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: hex('#111'),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Text(
                subtitle,
                style: TextStyle(
                  color: hex('#666'),
                  fontSize: 13,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 40),
              child: Container(
                height: 55,
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                      color: hex("#e4e4e4"),
                      width: 0.5,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: FlatButton(
                        child: Text(
                          '取  消',
                          style: TextStyle(
                            fontSize: 16,
                            color: hex('#111'),
                          ),
                        ),
                        onPressed: () {
                          G.pop();
                          if (onCancel != null) {
                            onCancel();
                          }
                        },
                      ),
                    ),
                    Container(
                      width: 0.5,
                      height: 40,
                      color: hex("#e4e4e4"),
                    ),
                    Expanded(
                      child: FlatButton(
                        child: Text(
                          '确  认',
                          style: TextStyle(
                            fontSize: 16,
                            color: hex('#111'),
                          ),
                        ),
                        onPressed: () {
                          G.pop();
                          onOk();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
