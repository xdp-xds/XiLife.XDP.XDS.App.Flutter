/*
 * @Author: meetqy
 * @since: 2019-10-18 16:44:52
 * @lastTime: 2019-10-18 17:14:16
 * @LastEditors: meetqy
 */
import 'package:color_dart/color_dart.dart';
import 'package:flutter/cupertino.dart';
import 'package:xdsapp/common_ui/xds_dialog/rewrite_show_dialog.dart';
import 'package:xdsapp/utils/global.dart';

class Loading extends RewriteShowDialog {
  Loading(BuildContext context):super(context);

  /// 显示
  void show() {
    init(
      elevation: 0,
      barrierColor: rgba(0,0,0,0),
      barrierDismissible: false,
      child: CupertinoActivityIndicator(radius: 24,),
    );
  }

  /// 隐藏
  void hide() => G.pop();
}