/*
 * @Author: meetqy
 * @since: 2019-10-18 15:59:07
 * @lastTime: 2019-11-15 10:07:16
 * @LastEditors: meetqy
 */
import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_dialog/rewrite_show_dialog.dart';
import 'package:xdsapp/utils/global.dart';


class Alert extends RewriteShowDialog {
  Alert(BuildContext context): super(context);

  /// 成功弹窗
  /// 参数说明请查看: normal
  success({
    String desc = '负责人保存成功后可继续查看',
    String title = '保存成功',
    Function onClose,
  }) {
    init(
      child: normal(
        title: Text('$title', style: TextStyle(
          color: hex('#111'),
          fontSize: 16,
          fontWeight: FontWeight.bold
        ),),
        icon: Container(
          width: 56,
          height: 56,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Image.asset('./lib/assets/images/success.png',)
        ),
        body: Text('$desc', style: TextStyle(
          fontSize: 14,
          color: hex('#999'),
        ),),
        onClose: onClose,
      )
    );
  }

  /// 失败弹窗
  /// 参数说明请查看: normal
  error({
    String desc = '检查网络环境，推出重新保存',
    Function onClose,
    String title = '保存失败'
  }) {
    init(
      child: normal(
        title: Text('$title', style: TextStyle(
          color: hex('#111'),
          fontSize: 16,
          fontWeight: FontWeight.bold
        ),),
        icon: Container(
          width: 56,
          height: 56,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Image.asset('./lib/assets/images/error.png',)
        ),
        body: Text('$desc', style: TextStyle(
          fontSize: 14,
          color: hex('#999'),
        ),),
        onClose: onClose,
      )
    );
  }

  
  /// 弹窗
  /// 
  /// ``` dart
  /// @param icon - icon
  /// @param title - 标题
  /// @param body - 内容
  /// @param onClose - 关闭按钮回调
  /// ```
  /// 
  normal({
    Widget icon,
    Widget title,
    Widget body,   
    Function onClose 
  }) {
    return  WillPopScope(
      onWillPop: () async{
        G.pop();
        if(onClose != null) return onClose();
        else return false;
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 310,
            padding: const EdgeInsets.symmetric(vertical: 34, horizontal: 15),
            decoration: BoxDecoration(
              color: hex('#fff'),
              borderRadius: BorderRadius.circular(6)
            ),
            child: Column(
              children: <Widget>[
                Container(child: icon,),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 14),
                  child: title,
                ),
                Container(child: body,),
              ],
            ),
          ),

          /// 底部关闭按钮
          Container(
            margin: const EdgeInsets.only(top: 35),
            child: GestureDetector(
              child: Icon(Icons.close, color: hex('#fff'),size: 30,),
              onTap: () {
                G.pop();
                if(onClose != null) onClose();
              },
            ),
          )
        ],
      ),
    );
  }
}