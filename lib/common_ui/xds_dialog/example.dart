/*
 * @Author: meetqy
 * @since: 2019-10-18 10:47:41
 * @lastTime: 2019-11-15 10:12:11
 * @LastEditors: meetqy
 */
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_button.dart';
import 'package:xdsapp/common_ui/xds_dialog/index.dart';

class ExampleXdsDialog extends StatelessWidget {
  const ExampleXdsDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('example_xds_dialog'),
        ),
        body: Column(
          children: <Widget>[
            buildContainer(context, title: '保存成功', onTap: () {
              XdsDialog.alert(context).success(onClose: () {});
            }),
            buildContainer(context, title: '保存失败', onTap: () {
              XdsDialog.alert(context).error(onClose: () {});
            }),
            buildContainer(context, title: '修改描述', onTap: () {
              XdsDialog.alert(context)
                  .success(desc: '修改保存失败描述，修改保存失败描述，修改保存失败描述\n修改保存失败描述');
            }),
            buildContainer(context, title: 'loading 1秒后自动隐藏', onTap: () {
              var loading = XdsDialog.loading(context);
              loading.show();
              Future.delayed(Duration(seconds: 1), () {
                loading.hide();
              });
            }),
          ],
        ));
  }

  Container buildContainer(BuildContext context,
      {String title, @required Function onTap}) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: XdsButton(
        width: 300,
        title: title,
        onTap: () => onTap(),
      ),
    );
  }
}
