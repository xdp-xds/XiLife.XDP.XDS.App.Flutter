/*
 * @Author: meetqy
 * @since: 2019-10-18 17:04:54
 * @lastTime: 2019-10-18 17:04:54
 * @LastEditors: meetqy
 */
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_dialog/alert.dart';
import 'package:xdsapp/common_ui/xds_dialog/confirm.dart';
import 'package:xdsapp/common_ui/xds_dialog/loading.dart';

class XdsDialog {
  /// 初始化alert
  static Alert alert(BuildContext context) => Alert(context);

  /// 初始化loading
  static Loading loading(BuildContext context) => Loading(context);

  /// 初始化confirm
  static Confirm confirm(BuildContext context) => Confirm(context);
}
