import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/constant/index.dart';
import 'package:xdsapp/utils/parse_date.dart';
import 'package:xdsapp/utils/iconfont.dart';
import 'package:xdsapp/utils/global.dart';

class BaseDetail extends StatelessWidget {
  final Map data;
  final List currentPlanDetails;
  final Function onTapProjectName;
  BaseDetail({
    Key key,
    this.data,
    this.currentPlanDetails,
    this.onTapProjectName,
  }) : super(key: key);

  ///当前工序
  getStatusColor(status) {
    final int delayDays = data["delayDays"];
    switch (status) {
      case PlanStatusEnum.unstart:
        {
          if (delayDays > 0) {
            return "#FA6510";
          }
          return "#999999";
        }
        break;
      case PlanStatusEnum.doing:
        {
          if (delayDays > 0) {
            return "#FA6510";
          }
          return "#13BB87";
        }
        break;
      case PlanStatusEnum.suspend:
        {
          return "#E80202";
        }
        break;
      case PlanStatusEnum.done:
        {
          return "#999999";
        }
        break;
      default:
        {
          return "#999999";
        }
    }
  }

  ///判断开始和结束时间是否在
  dateToString(start, end) {
    String startDate = dealtimeSlash(start);
    String endDate = dealtimeSlash(end);
    if (startDate == "" && endDate == "") {
      return "";
    } else {
      return "$startDate - $endDate";
    }
  }

  handelProcess(width) {
    List<Widget> processList = [];
    if (this.currentPlanDetails != null && this.currentPlanDetails.length > 0) {
      this.currentPlanDetails.forEach((item) {
        String text = "";
        if (item['processName'] != null) {
          item['processName'].forEach((processName) {
            int index = item['processName'].indexOf(processName);
            if (index != item['processName'].length - 1 &&
                item['processName'].length - 1 != index) {
              text = '$text$processName, ';
            } else {
              text = '$text$processName';
            }
          });
        }
        Widget line = Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('${item['deliveryStageName']} - '),
            Container(
              width: width - 220,
              child: Text(text),
            )
          ],
        );

        processList.add(line);
      });
      return processList;
    }
    return processList;
  }

  Widget currentProcess(width) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      padding: EdgeInsets.only(top: 11, left: 10),
      child: Center(
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 10),
                  child: Text(
                    '当前工序',
                    style: TextStyle(fontSize: 13, color: hex('#333333')),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 11),
                  child: Column(
                    children: this.handelProcess(width),
                  ),
                ),
              ],
            ),
            Positioned(
              bottom: 0,
              right: 10,
              child: Image.asset(
                'lib/assets/images/current_process.png',
                width: 75,
                height: 47.5,
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        color: hex('#c4fad2'),
        borderRadius: BorderRadius.circular(4),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;
    String actualString =
        dateToString(data['actualStartDate'], data['actualEndDate']);
    return Padding(
      padding: const EdgeInsets.fromLTRB(15, 20, 15, 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          InkWell(
              onTap: () {
                if (this.onTapProjectName != null) {
                  this.onTapProjectName();
                }
              },
              child: Row(
                children: <Widget>[
                  Text(
                    '${data['projectName']}',
                    style: TextStyle(
                      color: hex('#111111'),
                      fontSize: 20,
                    ),
                  ),
                  this.onTapProjectName != null
                      ? Container(
                          margin: EdgeInsets.fromLTRB(5, 8, 0, 0),
                          child: RotatedBox(
                            quarterTurns: 3, //旋转90度(1/4圈)
                            child: iconicontest(color: hex('#111111'), size: 8),
                          ),
                        )
                      : Container()
                ],
              )),
          Container(
            margin: const EdgeInsets.only(top: 14.5, bottom: 25),
            child: Text(
              '${data['address']}',
              style: TextStyle(
                fontSize: 13,
              ),
            ),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  width: .5,
                  color: hex('#333333'),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text('项目经理：${data['projectManagerName']}'),
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: InkWell(
                      onTap: () {
                        G.callPhone(int.parse(data['projectManagerPhoneNo']));
                      },
                      child: Container(
                        padding: EdgeInsets.only(bottom: 2),
                        width: 30,
                        height: 30,
                        child: icondianhua(color: hex('#13BB87'), size: 15),
                      ),
                    ),
                  )
                ],
              ),
              Container(
                padding:
                    const EdgeInsets.symmetric(vertical: 4, horizontal: 6.5),
                child: Text(
                  '${data['statusDesc']}',
                  style: TextStyle(
                    fontSize: 14,
                    color: hex('#fff'),
                  ),
                ),
                decoration: BoxDecoration(
                    color: hex(this.getStatusColor(data['status'])),
                    borderRadius: BorderRadius.circular(4)),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 7, bottom: 10),
                child: Text(
                    '计划：${dateToString(data['startDate'], data['endDate'])}'),
              ),
            ],
          ),
          actualString != ""
              ? Row(
                  children: <Widget>[
                    Text(
                      '实际：',
                    ),
                    Text(
                      '${dateToString(data['actualStartDate'], data['actualEndDate'])}',
                      style: TextStyle(color: hex("#111")),
                    ),
                  ],
                )
              : Container(),
          this.currentPlanDetails != null && this.currentPlanDetails.length > 0
              ? currentProcess(width)
              : Container(),
        ],
      ),
    );
  }
}
