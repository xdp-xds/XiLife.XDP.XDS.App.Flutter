import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_project_base_detail/index.dart';

class ExampleXdsProjectBaseDetail extends StatefulWidget {
  ExampleXdsProjectBaseDetail({Key key}) : super(key: key);

  @override
  _ExampleXdsProjectBaseDetailState createState() =>
      _ExampleXdsProjectBaseDetailState();
}

class _ExampleXdsProjectBaseDetailState
    extends State<ExampleXdsProjectBaseDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('xds_project_base_detail'),
      ),
      body: SafeArea(
        child: Container(
          color: hex('#f4f4f4'),
          child: BaseDetail(
            data: {
              "planId": "596c9279-ae73-48fc-99cd-3eaddb31931d",
              "projectName": "靖戎城金融城9栋1单元0001 ",
              "status": 2,
              "statusDesc": "未开始",
              "address": "四川省成都市高新区玉林街道靖戎城金融城9栋1单元0001",
              "projectManagerName": "陈经理",
              "projectManagerPhoneNo": 18780199916,
              "startDate": "2019-10-27T00:00:00",
              "endDate": "2020-01-09T23:59:59",
              "actualStartDate": "0001-01-01T00:00:00",
              "actualEndDate": "0001-01-01T00:00:00",
              "delayDays": 0
            },
            currentPlanDetails: [
              {
                "processName": ["工序1", "工序2"],
                "deliveryStageName": "基装"
              },
              {
                "processName": ["工序1", "工序2"],
                "deliveryStageName": "基装1"
              }
            ],
          ),
        ),
      ),
    );
  }
}
