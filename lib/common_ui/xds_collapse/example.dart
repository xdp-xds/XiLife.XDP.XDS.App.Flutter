/*
 * @Author: meetqy
 * @since: 2019-10-22 09:59:58
 * @lastTime: 2019-10-30 13:57:37
 * @LastEditors: meetqy
 */
import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_collapse/index.dart';

class ExampleXdsCollapse extends StatefulWidget {
  ExampleXdsCollapse({Key key}) : super(key: key);

  @override
  _ExampleXdsCollapseState createState() => _ExampleXdsCollapseState();
}

class _ExampleXdsCollapseState extends State<ExampleXdsCollapse> {
  bool status1 = true;
  bool status2 = true;
  bool status3 = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('xds_collapse'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            XdsCollapse(
              title: Text('折叠面板'),
              body: Column(
                children: <Widget>[
                  Container(
                      height: 50,
                      color: hex('#ff0'),
                      child: Row(
                        children: <Widget>[Text('第1行')],
                      )),
                  Container(
                      height: 50,
                      color: hex('#ff0'),
                      child: Row(
                        children: <Widget>[Text('第2行')],
                      )),
                  Container(
                      height: 50,
                      color: hex('#ff0'),
                      child: Row(
                        children: <Widget>[Text('第3行')],
                      )),
                  Container(
                      height: 50,
                      color: hex('#ff0'),
                      child: Row(
                        children: <Widget>[Text('第4行')],
                      )),
                  Container(
                      height: 50,
                      color: hex('#ff0'),
                      child: Row(
                        children: <Widget>[Text('第5行')],
                      )),
                ],
              ),
              value: status1,
              onChange: (bool value) {
                setState(() {
                  status1 = value;
                });
              },
            ),
            XdsCollapse(
              title: Text('折叠面板-padding为0'),
              padding: EdgeInsets.zero,
              body: Container(
                child: Column(
                  children: <Widget>[
                    Container(
                        height: 50,
                        color: hex('#ff0'),
                        child: Row(
                          children: <Widget>[Text('第1行')],
                        )),
                    Container(
                        height: 50,
                        color: hex('#ff0'),
                        child: Row(
                          children: <Widget>[Text('第2行')],
                        )),
                    Container(
                        height: 50,
                        color: hex('#ff0'),
                        child: Row(
                          children: <Widget>[Text('第3行')],
                        )),
                    Container(
                        height: 50,
                        color: hex('#ff0'),
                        child: Row(
                          children: <Widget>[Text('第4行')],
                        )),
                    Container(
                        height: 50,
                        color: hex('#ff0'),
                        child: Row(
                          children: <Widget>[Text('第5行')],
                        )),
                  ],
                ),
              ),
              value: status2,
              onChange: (bool value) {
                setState(() {
                  status2 = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
