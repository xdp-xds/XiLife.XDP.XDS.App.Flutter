/*
 * @Author: meetqy
 * @since: 2019-10-22 09:59:53
 * @lastTime: 2019-11-28 10:02:41
 * @LastEditors: meetqy
 */
import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';

import 'package:xdsapp/common_ui/xds_collapse/animate_rotate.dart';
import 'package:xdsapp/utils/iconfont.dart';

const sleep =  Duration(milliseconds: 150);

class XdsCollapse extends StatelessWidget {

  /// 折叠面板
  /// 
  /// ``` dart 
  /// @params title - 标题
  /// @params value - 是否收起 true:收起 false:展开 默认值:true
  /// @params body - 内容
  /// @params onChange - 回调 返回当前value
  /// @params padding - 内边距
  /// @params icon - 右侧展开收起icon
  /// ```
  const XdsCollapse({
    Key key,
    this.title,
    this.value = true,
    this.body,
    this.onChange,
    this.padding,
    this.icon
  }) : super(key: key);

  final Widget title;
  final bool value;
  final Widget body;
  final Function(bool) onChange;
  final EdgeInsetsGeometry padding;
  final Widget icon;
  
  @override
  Widget build(BuildContext context) {

    return Container(
      padding: padding == null ? EdgeInsets.symmetric(horizontal: 15) : padding,
      child: Column(
        children: <Widget>[
          // title
          GestureDetector(
            child: Container(
              color: rgba(0,0,0,0),
              padding: EdgeInsets.only(bottom: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,  
                children: <Widget>[
                  Container(
                    child: title,
                  ),

                  AnimatedRotate(
                    duration: sleep,
                    child: Container(
                      width: 40,
                      child: icon == null ? iconicontest(size: 8) : icon,
                    ),
                    rotate: value ? 0.5 : 1,
                  )
                ],
              ),
            ),
            onTap: () => onChange == null ? (){} : onChange(!value),
          ),

          _Body(child: body,value: value,),
        ],
      ),
    );
  }
}

/// 内容区域  
/// 为了获取当前内容的高度 实现展开收起动画，所以拆成状态组件
class _Body extends StatefulWidget {
  _Body({
    this.child,
    this.value,
    Key key,
  }) : super(key: key);

  final Widget child;
  final bool value;

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<_Body> with TickerProviderStateMixin {
  Widget build(BuildContext context) {
    return Container(
      child: AnimatedSize(
        vsync: this,
        duration: sleep,
        child: Container(
          constraints: BoxConstraints(
            maxHeight: !widget.value ? 0 : double.infinity,
          ),
          child: widget.child,       
        )
      ),
    );
  }
}