import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_button.dart';
import 'package:xdsapp/utils/global.dart';

class XdsBottomButton extends StatelessWidget {
  const XdsBottomButton({
    Key key,
    this.onCancel,
    this.onConfirm,
    this.showBorder = false
  }) : super(key: key);

  /// 取消回调
  final Function onCancel;
  
  /// 确认回调
  final Function onConfirm;

  final bool showBorder;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      bottom: 0,
      left: 0,
      child: Container(
        height: 84,
        padding: EdgeInsets.symmetric(horizontal: 15),
        width: G.screenWidth,
        decoration: BoxDecoration(
          color: hex('#fff'),
          border: Border(top: BorderSide(
            color: showBorder ? hex('#d8d8d8') : Colors.transparent
          ))
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            XdsButton(
              width: 160,
              height: 44,
              title: '取消',
              textColor: rgba(19,187,135,1),
              border: Border.all(color: rgba(19,187,135,1)),
              gradientStartColor: Colors.transparent,
              gradientEndColor: Colors.transparent,
              onTap: () => onCancel == null ? () {} : onCancel(),
            ),

            XdsButton(
              width: 160,
              height: 44,
              title: '确认',
              onTap: () => onConfirm == null ? (){} : onConfirm(),
            ),
          ],
        ),
      ),
    );
  }
}