import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_bottom_button/index.dart';

class ExampleXdsBottomButton extends StatefulWidget {
  ExampleXdsBottomButton({Key key}) : super(key: key);

  @override
  _ExampleXdsBottomButtonState createState() => _ExampleXdsBottomButtonState();
}

class _ExampleXdsBottomButtonState extends State<ExampleXdsBottomButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('xds_bottom_button'),
      ),
      body: SafeArea(
        child: Container(
          color: hex('#f4f4f4'),
          child: Stack(
            children: <Widget>[
              XdsBottomButton()
            ],
          ),
        ),
      ),
    );
  }
}