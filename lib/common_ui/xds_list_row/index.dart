import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';

/// 状态枚举
enum Status {
  /// 正常
  normal,

  /// 有风险
  warning,

  /// 延期
  delay,

  /// 完成
  complate,

  ///暂停
  pause
}

class XdsListRow extends StatelessWidget {
  final Status status;
  final String title;
  final Widget topRight;
  final String bottomLeft;
  final String bottomRight;

  /// 首字背景
  static List<Color> _firtWordColor = [];

  /// 标题颜色
  static Color _titleColor;

  /// 列表组件
  ///
  /// ``` dart
  /// @params status - 状态
  /// @params title - 标题
  /// @params topRight - 上右
  /// @params bottomLeft - 下左文本
  /// @params bottomRight - 下右文本
  /// ```
  const XdsListRow({
    Key key,
    this.status = Status.normal,
    this.topRight,
    this.bottomLeft = '',
    this.bottomRight = '',
    @required this.title,
  }) : super(key: key);

  /// 判断标题是否大于10字
  String checkTitle() {
    return title.length > 15 ? title.substring(0, 15) + '...' : title;
  }

  @override
  Widget build(BuildContext context) {
    _initColor();
    return Container(
      key: key,
      height: 101,
      color: hex('#fff'),
      child: Column(
        children: <Widget>[
          Container(
            height: 63,
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                // left
                Row(
                  children: <Widget>[
                    firstWord(),
                    Container(
                        margin: EdgeInsets.only(left: 7),
                        child: Text(
                          checkTitle(),
                          style: TextStyle(color: _titleColor),
                        )),
                  ],
                ),

                // right
                Row(
                  children: <Widget>[Container(child: topRight)],
                )
              ],
            ),
          ),

          // border
          Container(
            height: 1,
            color: hex('#e4e4e4'),
          ),

          // 底部
          Expanded(
            child: Container(
              alignment: Alignment.center,
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '$bottomLeft',
                    style: TextStyle(fontSize: 14, color: hex('#666')),
                  ),
                  Text(
                    '$bottomRight',
                    style: TextStyle(fontSize: 14, color: hex('#666')),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  /// 初始化颜色
  _initColor() {
    switch (status) {
      case Status.normal:
        {
          _firtWordColor = [
            hex('#2BD890'),
            hex('#09C6B7'),
          ];
          _titleColor = hex('#111');
          break;
        }
      case Status.warning:
        {
          _firtWordColor = [
            hex('#F69C69'),
            hex('#FA6510'),
          ];
          _titleColor = hex('#FA6510');
          break;
        }
      case Status.delay:
        {
          _firtWordColor = [
            hex('#F69C69'),
            hex('#FA6510'),
          ];
          _titleColor = hex('#FA6510');
          break;
        }
      case Status.complate:
        {
          _firtWordColor = [
            hex('#ADACBA'),
            hex('#808080'),
          ];
          _titleColor = hex('#111');
          break;
        }
      case Status.pause:
        {
          _firtWordColor = [
            hex('#F06464'),
            hex('#EB1A1A'),
          ];
          _titleColor = hex('#E80202');
          break;
        }
    }
  }

  Container firstWord() {
    String text = title[0];
    return Container(
      width: 25,
      height: 25,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        gradient: new LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: _firtWordColor),
      ),
      child: Text(
        '$text',
        style: TextStyle(color: hex('#fff')),
      ),
    );
  }
}
