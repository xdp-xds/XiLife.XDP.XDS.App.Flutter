import 'package:color_dart/color_dart.dart';
import 'package:flutter/material.dart';
import 'package:xdsapp/common_ui/xds_list_row/index.dart';

class ExampleXdsListRow extends StatelessWidget {
  const ExampleXdsListRow({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('xds_list_row'),
      ),

      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            color: hex('#F4F4F4'),
            child: Column(
              children: <Widget>[

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    title: '未完成 - 正常',
                    status: Status.normal,
                    bottomLeft: '当前：2 打折',
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    status: Status.delay,
                    title: '未完成 - 延期',
                    topRight: Text('xxxxx'),
                    bottomLeft: '当前：2 打折',
                    bottomRight: '延期:2天',
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    status: Status.warning,
                    title: '有风险',
                    bottomLeft: '当前：2 打折',
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    status: Status.complate,
                    title: '已完成',
                    bottomLeft: '当前：2 打折',
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    status: Status.complate,
                    title: '已完成 - 延期',
                    bottomLeft: '当前：2 打折',
                    bottomRight: '延期:2天',
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(vertical: 15),
                  child: XdsListRow(
                    status: Status.complate,
                    title: '已完成 - 任务列表',
                    bottomLeft: '完成时间：2019年10月02日',
                    bottomRight: '延期:2天',
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}