import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class XdsDrawer extends StatefulWidget {
  final double elevation;
  final Widget child;
  final String semanticLabel;
  final Function onOpen;

  ///new start
  final double width;

  ///new end
  const XdsDrawer({
    Key key,
    this.elevation = 16.0,
    this.child,
    this.semanticLabel,
    this.onOpen,

    ///new start
    this.width = 304.0,

    ///new end
  }) :

        ///new start

        ///new end

        super(key: key);
  @override
  _XdsDrawerState createState() => _XdsDrawerState();
}

class _XdsDrawerState extends State<XdsDrawer> {
  @override
  void initState() {
    // Todo: implement initState
    super.initState();
    widget.onOpen();
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterialLocalizations(context));
    String label = widget.semanticLabel;
    switch (defaultTargetPlatform) {
      case TargetPlatform.iOS:
        label = widget.semanticLabel;
        break;
      case TargetPlatform.android:
      case TargetPlatform.fuchsia:
        label = widget.semanticLabel ??
            MaterialLocalizations.of(context)?.drawerLabel;
    }

    ///new start
    final double _width = widget.width;

    ///new end
    return Semantics(
      scopesRoute: true,
      namesRoute: true,
      explicitChildNodes: true,
      label: label,
      child: ConstrainedBox(
        ///edit start
        constraints: BoxConstraints.expand(width: _width),

        ///edit end
        child: Material(
          elevation: widget.elevation,
          child: widget.child,
        ),
      ),
    );
  }
}
