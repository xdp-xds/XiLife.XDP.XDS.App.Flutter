_api_key="9e77916be140c2f02304a49c1afba599"
_uKey="2372b7435c1eef061882afcb2b19c79b"
_dir="/app/"
_config_dir="lib/config.dart";

# ######### 脚本样式 #############
__TITLE_LEFT_COLOR="\033[36;1m==== "
__TITLE_RIGHT_COLOR=" ====\033[0m"

__OPTION_LEFT_COLOR="\033[33;1m"
__OPTION_RIGHT_COLOR="\033[0m"

__LINE_BREAK_LEFT="\033[32;1m"
__LINE_BREAK_RIGHT="\033[0m"

# 红底白字
__ERROR_MESSAGE_LEFT="\033[41m ! ! ! "
__ERROR_MESSAGE_RIGHT=" ! ! ! \033[0m"

# 等待用户输入时间
__WAIT_ELECT_TIME=0.2

# 选择项输入方法 接收3个参数：1、选项标题 2、选项数组 3、选项数组的长度(0~256)
function READ_USER_INPUT() {
  title=$1
  options=$2
  maxValue=$3
  echo "${__TITLE_LEFT_COLOR}${title}${__TITLE_RIGHT_COLOR}"
  for option in ${options[*]}; do
    echo "${__OPTION_LEFT_COLOR}${option}${__OPTION_RIGHT_COLOR}"
  done
  read
  __INPUT=$REPLY
  expr $__INPUT "+" 10 &> /dev/null
  if [[ $? -eq 0 ]]; then
    if [[ $__INPUT -gt 0 && $__INPUT -le $maxValue ]]; then
      return $__INPUT
    else
      echo "${__ERROR_MESSAGE_LEFT}输入越界了，请重新输入${__ERROR_MESSAGE_RIGHT}"
      READ_USER_INPUT $title "${options[*]}" $maxValue
    fi
  else
    echo "${__ERROR_MESSAGE_LEFT}输入有误，请输入0~256之间的数字序号${__ERROR_MESSAGE_RIGHT}"
    READ_USER_INPUT $title "${options[*]}" $maxValue
  fi
}

# 打印信息
function printMessage() {
  pMessage=$1
  echo "${__LINE_BREAK_LEFT}${pMessage}${__LINE_BREAK_RIGHT}"
}

# 选择打包环境
select_packing_env=("1.delopment" "2.development_manualtest" "3.manualtest" "4.uat")
READ_USER_INPUT "请选择打包环境: " "${select_packing_env[*]}" ${#select_packing_env[*]}

# 选择结果
select_packing_env_options=$?

if [[ $select_packing_env_options -eq 1 ]]; then
  env_host="http://192.168.16.190:5000";
  env_appEnv="delopment";
elif [[ $select_packing_env_options -eq 2 ]]; then
  env_host="http://192.168.16.175:5000";
  env_appEnv="development_manualtest";
elif [[ $select_packing_env_options -eq 3 ]]; then
  env_host="http://192.168.16.170:5000";
  env_appEnv="manualtest";
elif [[ $select_packing_env_options -eq 4 ]]; then
  env_host="https://uat-api.xdp.xi-life.cn";
  env_appEnv="uat";
fi

# 清空文件内容
: > $_config_dir;

echo "class Config {
  /// 平台id
  static final String clientId = 'xilife.xdp.xds.app.login';

  /// 路由守卫开关
  static final bool routeDefend = true;

  /// 设置代理
  static final bool proxy = false;

  /// app环境
  static final String appEnv = '$env_appEnv';

  /// baseUrl
  static final String baseUrl = '$env_host';

  /// 请求链接超时
  static final int connectTimeout = 500000;

  /// 请求接收超时
  static final int receiveTimeout = 500000;
}" >> $_config_dir;

printMessage "config.dart创建成功😄";

echo "开始打包ipa";

flutter clean
flutter build ios --release #--no-codesign


if [ -d build/ios/iphoneos/Runner.app ]
    then

    mkdir app/Payload

    cp -r build/ios/iphoneos/Runner.app app/Payload
    
    mkdir app

    cd app
    filename=ios-$(date "+%Y%m%d%H%M").ipa
    _dir=$_dir$filename
    zip -r -m $filename Payload
    cd ..

    printMessage "打包成功😄"

    open app

else
    echo "遇到报错了😭, 打开Xcode查找错误原因"
    say "打包失败"
    exit;
fi

echo "开始上传"

curl -F "file=@`pwd`$_dir" \
-F "uKey=$_uKey" \
-F "_api_key=$_api_key" \
"http://www.pgyer.com/apiv1/app/upload"

printMessage "上传成功 🚀🚀🚀"

##==================================ipa==================================

echo "📅  Finished. Elapsed time: ${SECONDS}s"

open app

